<?php if ( comments_open() ) { ?>

    <section class="comments-list-box" id="comments-sect">
        <?php
            wp_update_comment_count_now( get_the_ID() );

            $count = get_comments_number();
            if($count){
        ?>
            <h3><span class="comments-list-box__marked-text"><?=$count?></span> comment<?php if($count > 1):?>s<?php endif;?></h3>
            <ul class="comments-list">
                <?php        
                    wp_list_comments( [
                        'walker'            => new Panda_Walker_Comment,
                        'style'             => 'ul',
                        'type'              => 'comment',
                        'reply_text'        => 'Reply',
                        'avatar_size'       => 80,
                        'reverse_top_level' => null,
                        'reverse_children'  => '',
                        'format'            => 'html5', // или xhtml, если HTML5 не поддерживается темой
                        'short_ping'        => false,    // С версии 3.6,
                        'echo'              => true,     // true или false
                    ] ); 
                    ?>
            </ul>
            <?php 
                $pagination = paginate_comments_links( [
                    'echo'    => false,
                    'add_fragment' => '#comments-sect',
                    'type' => 'array',
                    'prev_text' => '<svg width="21" height="20" viewBox="0 0 21 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M20 10.6C20.3314 10.6 20.6 10.3314 20.6 10C20.6 9.66863 20.3314 9.4 20 9.4L20 10.6ZM0.290018 9.57573C0.0557041 9.81005 0.0557041 10.1899 0.290018 10.4243L4.10839 14.2426C4.34271 14.477 4.72261 14.477 4.95692 14.2426C5.19124 14.0083 5.19124 13.6284 4.95692 13.3941L1.56281 10L4.95692 6.60588C5.19124 6.37157 5.19124 5.99167 4.95692 5.75736C4.72261 5.52304 4.34271 5.52304 4.1084 5.75736L0.290018 9.57573ZM20 9.4L0.714283 9.4L0.714283 10.6L20 10.6L20 9.4Z" fill="#146AFF"/>
                                </svg>',
                    'next_text' => '<svg width="21" height="20" viewBox="0 0 21 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 10.6C0.668629 10.6 0.4 10.3314 0.4 10C0.4 9.66863 0.668629 9.4 1 9.4L1 10.6ZM20.71 9.57573C20.9443 9.81005 20.9443 10.1899 20.71 10.4243L16.8916 14.2426C16.6573 14.477 16.2774 14.477 16.0431 14.2426C15.8088 14.0083 15.8088 13.6284 16.0431 13.3941L19.4372 10L16.0431 6.60588C15.8088 6.37157 15.8088 5.99167 16.0431 5.75736C16.2774 5.52304 16.6573 5.52304 16.8916 5.75736L20.71 9.57573ZM1 9.4L20.2857 9.4L20.2857 10.6L1 10.6L1 9.4Z" fill="#146AFF"/>
                                </svg>',
                ] );

                if($pagination){
            ?>
                <div class="comment-box__pagination-box pagination-box">
                        <?php 
                        $start_list = 0;
                        $end_list = 0;
                        foreach ($pagination as $i => $item) {

                            if( stripos( $item, 'prev ') !== false ){
                                $item = str_replace('prev ', ' prev pagination-box__arrow pagination-box__back-arrow ', $item);
                                echo $item;
                                ?>
                                    <ul class="pagination-box__pages-list">
                                <?php
                                $start_list = 1;
                                continue;
                            }elseif($i == 0){
                                ?>
                                <a class=" prev pagination-box__arrow pagination-box__back-arrow page-numbers pagination-box__arrow--hidden" href="#"><svg width="21" height="20" viewBox="0 0 21 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M20 10.6C20.3314 10.6 20.6 10.3314 20.6 10C20.6 9.66863 20.3314 9.4 20 9.4L20 10.6ZM0.290018 9.57573C0.0557041 9.81005 0.0557041 10.1899 0.290018 10.4243L4.10839 14.2426C4.34271 14.477 4.72261 14.477 4.95692 14.2426C5.19124 14.0083 5.19124 13.6284 4.95692 13.3941L1.56281 10L4.95692 6.60588C5.19124 6.37157 5.19124 5.99167 4.95692 5.75736C4.72261 5.52304 4.34271 5.52304 4.1084 5.75736L0.290018 9.57573ZM20 9.4L0.714283 9.4L0.714283 10.6L20 10.6L20 9.4Z" fill="#146AFF"/>
                                    </svg></a>
                                <?php
                            }

                            if( stripos( $item, 'next ') !== false ){
                                $item = str_replace('next ', ' next pagination-box__arrow pagination-box__next-arrow ', $item);
                                echo '</ul>'.$item;
                                $end_list = 1;
                                continue;
                            }

                            if(!$start_list && $i == 0){
                                ?>
                                    <ul class="pagination-box__pages-list">
                                <?php
                            }

                                $is_active = 0;
                                if( stripos( $item, ' current') !== false ){
                                    $is_active = 1;
                                }

                            ?>
                            <li class="pagination-box__page-instance <?=($is_active)?'pagination-box__page-instance--active':''?>">
                                <?=$item?>
                            </li>
                        <?php } ?>
                        <?php 
                            if(!$end_list){
                                ?>
                                    </ul>
                                    <a class=" next pagination-box__arrow pagination-box__next-arrow page-numbers pagination-box__arrow--hidden" href="#"><svg width="21" height="20" viewBox="0 0 21 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 10.6C0.668629 10.6 0.4 10.3314 0.4 10C0.4 9.66863 0.668629 9.4 1 9.4L1 10.6ZM20.71 9.57573C20.9443 9.81005 20.9443 10.1899 20.71 10.4243L16.8916 14.2426C16.6573 14.477 16.2774 14.477 16.0431 14.2426C15.8088 14.0083 15.8088 13.6284 16.0431 13.3941L19.4372 10L16.0431 6.60588C15.8088 6.37157 15.8088 5.99167 16.0431 5.75736C16.2774 5.52304 16.6573 5.52304 16.8916 5.75736L20.71 9.57573ZM1 9.4L20.2857 9.4L20.2857 10.6L1 10.6L1 9.4Z" fill="#146AFF"/>
                                    </svg></a>
                                <?php
                            }
                        ?>
                </div>
            <?php 
                }
            ?>
                        
        <?php } ?>            
    </section>
<?php } ?>