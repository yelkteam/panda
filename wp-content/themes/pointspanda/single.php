<?php get_header();?>

<?php while ( have_posts() ) : the_post(); ?>
    <?php
        $text = get_the_title();
        $url = get_the_permalink();

        $topdiscl = carbon_get_theme_option('topdiscl');
        $blog_discl = carbon_get_theme_option('disclosure_post');
        $share_text = carbon_get_theme_option('share_text');

        $before_comments_text = carbon_get_theme_option('after_auth_text');

        $cardblock_title = carbon_get_the_post_meta('cardblock_title');
        if( empty($cardblock_title) ) $cardblock_title = carbon_get_theme_option('post_card_cardblock_title');

        $cardblock_stitle = carbon_get_the_post_meta('cardblock_stitle');
        if( empty($cardblock_stitle) ) $cardblock_stitle = carbon_get_theme_option('post_card_cardblock_stitle');

        $cardblock_image = carbon_get_the_post_meta('cardblock_image');
        if( empty($cardblock_image) ) $cardblock_image = carbon_get_theme_option('post_card_cardblock_image');

        $cardblock_image_url = carbon_get_the_post_meta('post_card_cardblock_iurl');
        if( empty($cardblock_image_url) ) $cardblock_image_url = carbon_get_theme_option('post_card_cardblock_iurl');

        $cardblock_btext = carbon_get_the_post_meta('cardblock_btext');
        if( empty($cardblock_btext) ) $cardblock_btext = carbon_get_theme_option('post_card_cardblock_btext');

        $cardblock_burl = carbon_get_the_post_meta('cardblock_burl');
        if( empty($cardblock_burl) ) $cardblock_burl = carbon_get_theme_option('post_card_cardblock_burl');

        $cardblock_show = carbon_get_the_post_meta('cardblock_show');

        $authorName = get_the_author();
        $authorLink = get_the_author_posts_link();
        $authorAvatar = get_avatar( get_the_author_email());

    ?>
    <main class="blog-post-main-content">
        <div class="wrapper">
            <section class="breadcrumbs-box">
                <?php panda_breadcrumbs(); ?>
            </section>
            <section class="post-title-big-poster">
                <div class="post-title-big-poster__img-box">
                    <?=get_the_post_thumbnail( null, 'blog_top' )?>
                </div>

				<?php
					$primary_cat_id = get_post_meta( get_the_ID(), 'rank_math_primary_category', true );

					if ( $primary_cat_id ) {
					    $product_cat = get_term( $primary_cat_id, 'category' );
					}
				?>
				<object>
					<a href="<?=get_category_link($product_cat->term_id)?>" class="blog-card__img-tag"><?=$product_cat->name;?></a>
				</object>
                <h1 class="post-title-big-poster__title"><?=get_the_title()?></h1>
                <div class="post-title-big-poster__info">
                    <div class="post-title-big-poster__updated-date"><?=__('Last updated: ','panda')?><?=get_the_modified_date('F d,Y')?></div>
                    <div class="post-title-big-poster__date"><?=__('Originally Published: ','panda')?><?=get_the_date('F d,Y')?></div>
                    <div class="post-title-big-poster__author"><?=sprintf(__('by %s','panda'), get_the_author())?></div>
                </div>
            </section>
            <?php if($topdiscl){?>
              <div class="ad-top-box">
                <div class="ad-top-box__icon"></div>
                <?=apply_filters('the_content',$topdiscl)?>
              </div>
            <?php } ?>
            <section class="article-content-box">
                <section class="article-content">
                    <?php if($blog_discl){ ?>
                        <div class="advertiser-disclosure-text-box">
                            <?=apply_filters('the_content',$blog_discl)?>
                        </div>
                    <?php } ?>

                    <?php get_template_part('template-parts/share','horizontal'); ?>

                    <?php the_content(); ?>

                    <section class="post-tags-box">
                        <?php
                            $cats = wp_get_object_terms( get_the_ID(), 'category');
                            if($cats){
                        ?>
                            <div class="post-tags-box__info"><?=__('Posted in ','panda')?>
                                <?php foreach($cats as $cat){ ?>
                                    <a href="<?=get_category_link($cat->term_id)?>" class="post-tags-box__link"><?=$cat->name?></a>
                                <?php } ?>
                            </div>
                        <?php }  ?>
                        <?php
                            $tags = wp_get_object_terms( get_the_ID(), 'post_tag');
                            if($tags){
                        ?>
                            <div class="post-tags-box__tag-list">
                                <div class="post-tags-box__title"><?=__('Tagged:','panda')?></div>
                                <div class="post-tags-box__tags">
                                    <?php foreach($tags as $tag){ ?>
                                        <span class="post-tags-box__tag"><?=$tag->name?></span>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php }  ?>
                    </section>
                    <?php get_template_part('template-parts/share','horizontal'); ?>
                    <section class="author-box">
                        <div class="post-author-card">
                            <div class="post-author-card__avatar-box">
                                <?php echo $authorAvatar; ?>
                            </div>
                            <div class="post-author-card__text-box">
                            <h4><?=$authorLink?></h4>
                                <p><?php the_author_meta('description');?></p>
                                <?php
                                    $twitter = get_the_author_meta( 'twitter' );
                                    $facebook = get_the_author_meta( 'facebook' );
                                    $linkedin = get_the_author_meta( 'linkedin' );
                                    $instagram = get_the_author_meta( 'inst_url' );
                                    $reddit = get_the_author_meta( 'reddit_url' );
                                ?>
                                <ul class="dense-social-list post-author-card__social-list">
                                    <?php if($linkedin):?>
                                    <li class="dense-social-list__item dense-social-list__item-linked-in">
                                        <a href="<?=$linkedin;?>">
                                            <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M3.17811 14V4.55422H0.177061V14H3.17811ZM1.67797 3.26379C2.72449 3.26379 3.3759 2.53846 3.3759 1.63205C3.3564 0.705193 2.72453 0 1.69783 0C0.671294 0 0 0.705207 0 1.63205C0 2.53851 0.651247 3.26379 1.65838 3.26379H1.67788H1.67797ZM4.83918 14H7.84023V8.72502C7.84023 8.44271 7.85973 8.16069 7.93898 7.95888C8.15593 7.39483 8.64972 6.81064 9.47874 6.81064C10.5647 6.81064 10.9991 7.67685 10.9991 8.94665V13.9999H14V8.58381C14 5.68244 12.5194 4.33246 10.5449 4.33246C8.92599 4.33246 8.21516 5.27917 7.82027 5.92397H7.8403V4.55403H4.83925C4.87863 5.44037 4.83925 13.9998 4.83925 13.9998L4.83918 14Z" fill="#000"></path>
                                            </svg>
                                        </a>
                                    </li>
                                    <?php endif;?>
                                    <?php if($facebook):?>
                                    <li class="dense-social-list__item dense-social-list__item-facebook">
                                        <a href="<?=$facebook;?>" rel="nofollow" target="_blank" >
                                            <svg width="9" height="16" viewBox="0 0 9 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M8.0075 8.9995L8.452 6.104H5.6735V4.225C5.6735 3.433 6.0615 2.6605 7.306 2.6605H8.569V0.1955C8.569 0.1955 7.423 0 6.327 0C4.039 0 2.5435 1.387 2.5435 3.8975V6.1045H0V9H2.5435V16H5.6735V9L8.0075 8.9995Z" fill="#000"></path>
                                            </svg>
                                        </a>
                                    </li>
                                    <?php endif;?>
                                    <?php if($twitter):?>
                                    <li class="dense-social-list__item dense-social-list__item-twitter">
                                        <a href="<?=$twitter;?>" rel="nofollow" target="_blank" >
                                            <svg width="16" height="13" viewBox="0 0 16 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M16 1.54405C15.4116 1.79787 14.7713 1.98054 14.1118 2.05168C14.7965 1.64488 15.3093 1.00219 15.5539 0.244209C14.9113 0.62645 14.2074 0.894484 13.4734 1.03642C13.1666 0.708435 12.7955 0.447146 12.3833 0.268836C11.9711 0.090526 11.5266 -0.000984081 11.0775 7.98058e-06C9.26043 7.98058e-06 7.79906 1.47291 7.79906 3.28038C7.79906 3.5342 7.82983 3.78801 7.87982 4.03222C5.159 3.88993 2.73236 2.59008 1.1191 0.599936C0.825142 1.10202 0.671099 1.6737 0.672996 2.25551C0.672996 3.39383 1.25177 4.39756 2.13436 4.98787C1.61424 4.96739 1.1063 4.82442 0.651845 4.57061V4.61099C0.651845 6.20503 1.77863 7.52603 3.28038 7.82984C2.99841 7.90308 2.70834 7.94055 2.41702 7.94136C2.20358 7.94136 2.00168 7.92021 1.79786 7.89137C2.2132 9.19121 3.42267 10.1353 4.86288 10.1661C3.73609 11.0487 2.32472 11.5678 0.792213 11.5678C0.517246 11.5678 0.26343 11.5582 0 11.5275C1.45367 12.46 3.17846 12.9984 5.03593 12.9984C11.066 12.9984 14.3656 8.00289 14.3656 3.66688C14.3656 3.52458 14.3656 3.38229 14.356 3.24C14.9944 2.77275 15.5539 2.19397 16 1.54405Z" fill="#000"></path>
                                            </svg>
                                        </a>
                                    </li>
                                    <?php endif;?>
                                    <?php if($instagram):?>
                                    <li class="dense-social-list__item dense-social-list__item-instagram">
                                        <a href="<?=$instagram;?>" rel="nofollow" target="_blank" >
                                            <svg id="Bold" enable-background="new 0 0 24 24" viewBox="0 0 24 24" width="17" xmlns="http://www.w3.org/2000/svg"><path d="m12.004 5.838c-3.403 0-6.158 2.758-6.158 6.158 0 3.403 2.758 6.158 6.158 6.158 3.403 0 6.158-2.758 6.158-6.158 0-3.403-2.758-6.158-6.158-6.158zm0 10.155c-2.209 0-3.997-1.789-3.997-3.997s1.789-3.997 3.997-3.997 3.997 1.789 3.997 3.997c.001 2.208-1.788 3.997-3.997 3.997z"/><path d="m16.948.076c-2.208-.103-7.677-.098-9.887 0-1.942.091-3.655.56-5.036 1.941-2.308 2.308-2.013 5.418-2.013 9.979 0 4.668-.26 7.706 2.013 9.979 2.317 2.316 5.472 2.013 9.979 2.013 4.624 0 6.22.003 7.855-.63 2.223-.863 3.901-2.85 4.065-6.419.104-2.209.098-7.677 0-9.887-.198-4.213-2.459-6.768-6.976-6.976zm3.495 20.372c-1.513 1.513-3.612 1.378-8.468 1.378-5 0-7.005.074-8.468-1.393-1.685-1.677-1.38-4.37-1.38-8.453 0-5.525-.567-9.504 4.978-9.788 1.274-.045 1.649-.06 4.856-.06l.045.03c5.329 0 9.51-.558 9.761 4.986.057 1.265.07 1.645.07 4.847-.001 4.942.093 6.959-1.394 8.453z"/><circle cx="18.406" cy="5.595" r="1.439"/></svg>
                                        </a>
                                    </li>
                                    <?php endif;?>
                                    <?php if($reddit):?>
                                        <li class="dense-social-list__item dense-social-list__item-reddit">
                                        <a href="<?=$reddit;?>" rel="nofollow" target="_blank" >
                                            <svg height="16" viewBox="0 0 24 24" width="16" xmlns="http://www.w3.org/2000/svg"><path d="m17.631 2.782c.034 1.32.952 2.631 2.685 2.631 1.621 0 2.691-1.216 2.691-2.706 0-1.345-.925-2.707-2.692-2.707-.958 0-1.836.428-2.335 1.321l-4.297-1.289c-.414-.123-.835.123-.941.527l-1.923 7.31c-2.236.16-4.278.777-5.951 1.709-2.77-2.595-6.557 1.633-4.091 4.562-1.774 5.542 4.594 9.86 11.223 9.86 6.681 0 12.974-4.39 11.223-9.86 2.458-2.919-1.301-7.154-4.095-4.564-1.87-1.04-4.206-1.678-6.75-1.736l1.617-6.148zm2.684-1.282c.879 0 1.191.65 1.191 1.208 0 .667-.409 1.206-1.191 1.206-.88 0-1.191-.65-1.191-1.208 0-.674.412-1.206 1.191-1.206zm-16.784 8.955c-.793.607-1.455 1.297-1.957 2.052-.38-1.412.839-2.632 1.957-2.052zm8.469 12.045c-8.943 0-13.396-7.182-6.982-11.298.422-.194 2.664-1.876 6.982-1.876 3.756 0 5.474 1.184 6.98 1.875 6.39 4.099 2.018 11.299-6.98 11.299zm10.426-9.992c-.502-.756-1.164-1.446-1.958-2.053 1.141-.589 2.335.654 1.958 2.053z"/><path d="m10.305 14.935c0-1.321-.998-2.206-2.217-2.206-1.076 0-2.219.773-2.219 2.206 0 1.32.996 2.206 2.217 2.206 1.076 0 2.219-.773 2.219-2.206zm-2.936 0c0-.655.55-.706.719-.706.403 0 .717.229.717.706 0 .655-.55.706-.719.706-.168 0-.717-.051-.717-.706z"/><path d="m15.914 12.729c-1.076 0-2.219.773-2.219 2.206 0 1.32.996 2.206 2.217 2.206 1.076 0 2.219-.773 2.219-2.206 0-1.321-.998-2.206-2.217-2.206zm-.002 2.912c-.168 0-.717-.051-.717-.706s.55-.706.719-.706c.403 0 .717.229.717.706 0 .655-.55.706-.719.706z"/><path d="m15.297 18.42c-1.106 1.517-5.454 1.536-6.593.001-.56-.812-1.786.023-1.241.842 1.591 2.402 7.483 2.403 9.074 0 .536-.803-.667-1.64-1.24-.843z"/></svg>
                                        </a>
                                    </li>
                                    <?php endif;?>
                                </ul>
                            </div>
                        </div>
                    </section>
                    <?php do_shortcode('[crp limit="4"]'); ?>
                </section>
                <aside>
                    <div class="blog-post-sidebar-social-box">
                        <span class="follow_us-title"><?=$share_text;?></span>
                        <?=do_shortcode(' [aps-counter]')?>
                    </div>

                  <div class="aside-collapsed-subscribe-box collapsed-subscribe-box" id="mc_embed_signup">
                    <div class="collapsed-subscribe-box__wrapper">
                      <div class="collapsed-subscribe-box__img-section"></div>
                      <div class="collapsed-subscribe-box__main-section">
                          <?php
                          $mailchimp_description = carbon_get_theme_option( 'mailchimp_description_sidebar' );
                          echo $mailchimp_description;
                          ?>
                        <button class="collapsed-subscribe-box__toggle-btn">
                          <svg width="12" height="14" viewBox="0 0 12 14" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M5.46967 13.5303C5.76256 13.8232 6.23744 13.8232 6.53033 13.5303L11.3033 8.75736C11.5962 8.46447 11.5962 7.98959 11.3033 7.6967C11.0104 7.40381 10.5355 7.40381 10.2426 7.6967L6 11.9393L1.75736 7.6967C1.46447 7.40381 0.989593 7.40381 0.696699 7.6967C0.403806 7.98959 0.403806 8.46447 0.6967 8.75736L5.46967 13.5303ZM5.25 3.27835e-08L5.25 13L6.75 13L6.75 -3.27835e-08L5.25 3.27835e-08Z" fill="white"/></svg>
                        </button>
                      </div>
                      <div class="collapsed-subscribe-box__form-section">
                        <div class="collapsed-subscribe-box__separator">
                          <svg width="29" height="17" viewBox="0 0 29 17" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M5.18995 10.1192L0 5.94154L29 0L11.7002 15.2716L7.05651 11.0476L21.5338 2.55301L5.18995 10.1192Z" fill="url(#plane_paint2_linear)"/><path d="M5.18994 16.5715V10.1194L21.5337 2.55322L7.0565 11.0478L9.36497 13.1458L5.18994 16.5715Z" fill="url(#plane_paint3_linear)"/><defs><linearGradient id="plane_paint2_linear" x1="27.7319" y1="0.637445" x2="-3.6231" y2="19.3888" gradientUnits="userSpaceOnUse"><stop stop-color="white"/><stop offset="1" stop-color="white" stop-opacity="0"/></linearGradient><linearGradient id="plane_paint3_linear" x1="4.47517" y1="18.1633" x2="10.0759" y2="10.4653" gradientUnits="userSpaceOnUse"><stop stop-color="white"/><stop offset="1" stop-color="white" stop-opacity="0"/></linearGradient></defs></svg>
                        </div>
                        <?php get_template_part('template-parts/subscribe-form'); ?>
                      </div>
                    </div>
                  </div>
                  <?php
                    $ccitems = carbon_get_theme_option( 'top_ccdeals' );
                    if($ccitems){
                        $cctitle = carbon_get_theme_option( 'top_ccdealstitle' );
                        ?>
                        <section class="credit-cards-sidebar">
                            <div class="credit-cards-sidebar__header">
                                <h4><?=$cctitle?></h4>
                                <?php if(isset($cards_page[0]['id'])){ ?>
                                    <a href="<?=get_the_permalink($cards_page[0]['id'])?>"><?=__('See all','panda')?></a>
                                <?php } ?>
                            </div>
                            <ul class="credit-card-box-vertical-list">
                                <?php
                                    foreach($ccitems as $i => $item){
                                        $clink = carbon_get_post_meta($item['id'], 'card_url');
                                        if ( empty($clink) ) {
                                            $clink = '#';
                                        }
                                    ?>
                                    <li class="credit-card-box__item">
                                        <a class="credit-card-box__card-link" href="<?=$clink?>" rel="noopener noreferrer">
                                            <div class="credit-card-box__img-box">
                                                <?php
                                                    if(has_post_thumbnail($item['id'])){
                                                        echo get_the_post_thumbnail($item['id'],'small_card');
                                                    }else{
                                                        echo wp_get_attachment_image($placeholder,'small_card');
                                                    }
                                                ?>
                                            </div>
                                            <div class="credit-card-box__card-info">
                                                <?php if($i == 0){ ?>
                                                    <div class="credit-card-box__subtitle"><?=__('Best Total Value','panda')?></div>
                                                <?php } ?>
                                                <div class="credit-card-box__title"><?=get_the_title($item['id'])?></div>
                                                <div class="credit-card-box__info">
                                                <?php
                                                    $current = carbon_get_post_meta($item['id'],'current_offer');
                                                    if($current){
                                                        ?>
                                                        <span class="credit-card-box__mark-text"><?=__('Current offer:','panda')?></span> <?=$current?>
                                                        <?php
                                                    }else{
                                                        $in_bonus = carbon_get_post_meta($item['id'],'text_offer');
                                                        echo $in_bonus;
                                                    }
                                                ?>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                            <div id="creditCardsSlider" class="credit-card-box__header">
                                <h2><?=$cctitle?></h2>
                                <div class="credit-cards-slider">
                                    <?php
                                        foreach($ccitems as $i => $item){
                                            $clink = carbon_get_post_meta($item['id'], 'card_url');
                                            if ( empty($clink) ) {
                                                $clink = '#';
                                            }
                                        ?>
                                        <div class="slide credit-cards-slider__item credit-card-box__item">
                                            <a class="credit-card-box__card-link" href="<?=$clink?>" rel="noopener noreferrer">
                                                <div class="credit-card-box__img-box">
                                                <?php
                                                    if(has_post_thumbnail($item['id'])){
                                                        echo get_the_post_thumbnail($item['id'],'small_card');
                                                    }else{
                                                        echo wp_get_attachment_image($placeholder,'small_card');
                                                    }
                                                ?>
                                                </div>
                                                <div class="credit-card-box__card-info">
                                                    <?php if($i == 0){ ?>
                                                        <div class="credit-card-box__subtitle"><?=__('Best Total Value','panda')?></div>
                                                    <?php } ?>
                                                    <div class="credit-card-box__title"><?=get_the_title($item['id'])?></div>
                                                    <div class="credit-card-box__info">
                                                        <?php
                                                            $current = carbon_get_post_meta($item['id'],'current_offer');
                                                            if($current){
                                                                ?>
                                                                <span class="credit-card-box__mark-text"><?=__('Current offer:','panda')?></span> <?=$current?>
                                                                <?php
                                                            }else{
                                                                $in_bonus = carbon_get_post_meta($item['id'],'text_offer');
                                                                echo $in_bonus;
                                                            }
                                                        ?>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    <?php } ?>
                                    <div id="creditCardsSliderControls" class="credit-cards-slider__controls"></div>
                                </div>
                                <?php if(isset($cards_page[0]['id'])){ ?>
                                    <a class="credit-card-box__link--transparent" href="<?=get_the_permalink($cards_page[0]['id'])?>">
                                        <span><?=__('See all','panda')?></span>
                                        <svg width="42" height="10" viewBox="0 0 42 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M1 4.4C0.668629 4.4 0.4 4.66863 0.4 5C0.4 5.33137 0.668629 5.6 1 5.6V4.4ZM41.4243 5.42426C41.6586 5.18995 41.6586 4.81005 41.4243 4.57574L37.6059 0.757359C37.3716 0.523045 36.9917 0.523045 36.7574 0.757359C36.523 0.991674 36.523 1.37157 36.7574 1.60589L40.1515 5L36.7574 8.39411C36.523 8.62843 36.523 9.00833 36.7574 9.24264C36.9917 9.47696 37.3716 9.47696 37.6059 9.24264L41.4243 5.42426ZM1 5.6H41V4.4H1V5.6Z" fill="#146AFF"/>
                                        </svg>
                                    </a>
                                <?php } ?>
                            </div>
                        </section>
                    <?php } ?>
                </aside>
            </section>
            <?php if( $cardblock_show != 1 ) { ?>
                <section class="consultation-box">
                <?php if($cardblock_title){ ?>
                            <div class="consultation-box__title"><?=$cardblock_title?></div>
                        <?php } ?>
                    <div class="consultation-box__wrapper">
                        <div class="consultation-box__info-box">
                            <?php
                                if($cardblock_stitle){
                                    echo apply_filters('the_content', $cardblock_stitle);
                                }
                            ?>
                            <?php if($cardblock_btext){ ?>
                                <a class="green-link-btn" href="<?=$cardblock_burl?>"><?=$cardblock_btext?></a>
                            <?php } ?>
                        </div>
                        <?php if( !empty($cardblock_image_url) ): ?>
                            <a href="<?= $cardblock_image_url ?>" class="consultation-box__img-box">
                                <?=wp_get_attachment_image($cardblock_image,'large')?>
                            </a>
                        <?php else: ?>
                            <span class="consultation-box__img-box">
                                <?=wp_get_attachment_image($cardblock_image,'large')?>
                            </span>
                        <?php endif; ?>
                    </div>
                </section>
            <?php } ?>
            <section class="comment-box">
                <?php if($before_comments_text){ ?>
                    <div class="comment-warning-box">
                        <?=apply_filters('the_content',$before_comments_text)?>
                    </div>
                <?php } ?>
                <div class="comment-form-box">
                    <?php
                        $post_id = get_the_ID();
                        $current_user = wp_get_current_user();

                        $user_identity = '';
                        if($current_user){
                            $user_identity = $current_user->data->display_name;
                        }

                        $args = array(
                                        'fields' => array(
                                                            'author' => '<div class="comment-form__field-wrapper comment-form__name-field-wrapper">
                                                                            <input type="text" class="comment-form__field comment-form__name-field" value="" id="author" name="author" placeholder="Your Name">
                                                                            <p class="comment-form__validation-message"></p>
                                                                        </div>',
                                                            'email'  => '<div class="comment-form__field-wrapper comment-form__email-field-wrapper">
                                                                            <input type="text" class="comment-form__field comment-form__email-field" id="email" name="email" value="" placeholder="Your E-mail">
                                                                            <p class="comment-form__validation-message"></p>
                                                                        </div>',
                                                        ),
                                        'comment_field' => '<div class="comment-form__field-wrapper comment-form__message-field-wrapper">
                                                                <textarea id="messageTextarea" class="comment-form__field comment-form__message-field" placeholder="Your Message" id="comment" name="comment"></textarea>
                                                                <p class="comment-form__validation-message"></p>
                                                            </div>',
                                        'class_submit' => 'comment-form__btn green-link-btn',
                                        'id_submit' => 'formBtn',
                                        'label_submit' => 'Send',
                                        'logged_in_as' => '<div class="comment-form__header"><p class="logged-in-as">' . sprintf( __( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>' ), admin_url( 'profile.php' ), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ) . '</p></div>',
                                        'must_log_in' =>'<div class="comment-form__header"><p class="must-log-in">' .  sprintf( __( 'You must be <a href="%s">logged in</a> to post a comment.' ), wp_login_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ) . '</p></div>',
                                        'title_reply_before' => '<div class="comment-form__header"><h4>',
                                        'title_reply_after' => '</h4></div>',
                                        'comment_notes_before' => '',
                                    );

                        comment_form($args);

                    ?>
                </div>
                <noindex>
                    <?php comments_template(); ?>
                </noindex>
            </section>

            <section class="blog-post-ad-box">
                <div id='div-gpt-ad-6598859-2'>
                    <script> googletag.cmd.push(function() { googletag.display('div-gpt-ad-6598859-2'); }); </script>
                </div>
            </section>

        </div>
    </main>

<?php endwhile; ?>
<?php get_footer();?>
