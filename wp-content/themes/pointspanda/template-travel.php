<?php
/*
* Template Name: Travel
*/
get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
<?php

	$postMeta = get_post_meta(get_the_ID());

	$video_text = (isset($postMeta['_video_text'][0]) && $postMeta['_video_text'][0])?$postMeta['_video_text'][0]:'';
	$video_frame = (isset($postMeta['_video_frame'][0]) && $postMeta['_video_frame'][0])?$postMeta['_video_frame'][0]:'';
	$note = (isset($postMeta['_note'][0]) && $postMeta['_note'][0])?$postMeta['_note'][0]:'';
	
	$first_step_text = (isset($postMeta['_first_step_text'][0]) && $postMeta['_first_step_text'][0])?$postMeta['_first_step_text'][0]:'';
	$first_step_image = (isset($postMeta['_first_step_image'][0]) && $postMeta['_first_step_image'][0])?$postMeta['_first_step_image'][0]:'';
	$first_step_image = wp_get_attachment_image($first_step_image, 'full');

	$second_step_text = (isset($postMeta['_second_step_text'][0]) && $postMeta['_second_step_text'][0])?$postMeta['_second_step_text'][0]:'';
	$second_step_image = (isset($postMeta['_second_step_image'][0]) && $postMeta['_second_step_image'][0])?$postMeta['_second_step_image'][0]:'';
	$second_step_image = wp_get_attachment_image($second_step_image, 'full');

	$third_step_text = (isset($postMeta['_third_step_text'][0]) && $postMeta['_third_step_text'][0])?$postMeta['_third_step_text'][0]:'';
	$third_step_image = (isset($postMeta['_third_step_image'][0]) && $postMeta['_third_step_image'][0])?$postMeta['_third_step_image'][0]:'';
	$third_step_image = wp_get_attachment_image($third_step_image, 'full');

	$fourth_step_text = (isset($postMeta['_fourth_step_text'][0]) && $postMeta['_fourth_step_text'][0])?$postMeta['_fourth_step_text'][0]:'';
	$fourth_step_image = (isset($postMeta['_fourth_step_image'][0]) && $postMeta['_fourth_step_image'][0])?$postMeta['_fourth_step_image'][0]:'';
	$fourth_step_image = wp_get_attachment_image($fourth_step_image, 'full');

	$fifth_step_text = (isset($postMeta['_fifth_step_text'][0]) && $postMeta['_fifth_step_text'][0])?$postMeta['_fifth_step_text'][0]:'';
	$fifth_step_image = (isset($postMeta['_fifth_step_image'][0]) && $postMeta['_fifth_step_image'][0])?$postMeta['_fifth_step_image'][0]:'';
	$fifth_step_image = wp_get_attachment_image($fifth_step_image, 'full');

	$first_benefit = (isset($postMeta['_first_benefit'][0]) && $postMeta['_first_benefit'][0])?$postMeta['_first_benefit'][0]:'';
	$second_benefit = (isset($postMeta['_second_benefit'][0]) && $postMeta['_second_benefit'][0])?$postMeta['_second_benefit'][0]:'';
	$third_benefit = (isset($postMeta['_third_benefit'][0]) && $postMeta['_third_benefit'][0])?$postMeta['_third_benefit'][0]:'';

	$pagetoptext = (isset($postMeta['_pagetoptext'][0]) && $postMeta['_pagetoptext'][0])?$postMeta['_pagetoptext'][0]:'';
	$three_cols_block = (isset($postMeta['_tcols_items'][0]) && $postMeta['_tcols_items'][0])?$postMeta['_tcols_items'][0]:'';

	$teammeet_title = (isset($postMeta['_teammeet_title'][0]) && $postMeta['_teammeet_title'][0])?$postMeta['_teammeet_title'][0]:'';
	$teammeet_img = (isset($postMeta['_teammeet_img'][0]) && $postMeta['_teammeet_img'][0])?$postMeta['_teammeet_img'][0]:'';
	$teammeet_description = (isset($postMeta['_teammeet_description'][0]) && $postMeta['_teammeet_description'][0])?$postMeta['_teammeet_description'][0]:'';
	$teammeet_position = (isset($postMeta['_teammeet_position'][0]) && $postMeta['_teammeet_position'][0])?$postMeta['_teammeet_position'][0]:'';

	$sheduleblock_description = (isset($postMeta['_sheduleblock_description'][0]) && $postMeta['_sheduleblock_description'][0])?$postMeta['_sheduleblock_description'][0]:'';
	$sheduleblock_btext = (isset($postMeta['_sheduleblock_btext'][0]) && $postMeta['_sheduleblock_btext'][0])?$postMeta['_sheduleblock_btext'][0]:'';
	$sheduleblock_burl = (isset($postMeta['_sheduleblock_burl'][0]) && $postMeta['_sheduleblock_burl'][0])?$postMeta['_sheduleblock_burl'][0]:'';

	$testimonial_title = (isset($postMeta['_testimonial_title'][0]) && $postMeta['_testimonial_title'][0])?$postMeta['_testimonial_title'][0]:'';
	$testimonial_items = (isset($postMeta['_testimonial_items'][0]) && $postMeta['_testimonial_items'][0])?$postMeta['_testimonial_items'][0]:'';

	$faq_title = (isset($postMeta['_faq_title'][0]) && $postMeta['_faq_title'][0])?$postMeta['_faq_title'][0]:'';
	$faq_items = carbon_get_the_post_meta('faq_items');

?>
	<main class="concierge-page-main-content">
		<div class="wrapper">
			<section class="breadcrumbs-box">
				<?php panda_breadcrumbs(); ?>
			</section>

			<section class="concierge-page-main-content__header">
				<div class="team-people-card team-people-card--text-left">
					<div class="team-people-card__avatar-box">
					<div class="team-people-card__img-box">
						<?=get_the_post_thumbnail(null,'full')?>
					</div>
					</div>
					<div class="team-people-card__info-box">
						<?=apply_filters('the_content',$pagetoptext)?>
					</div>
				</div>
			</section>
			<div class="concierge-video-box">
				<div class="concierge-video-box__video-col">
					<?=$video_frame;?>
					<div class="quote-box concierge-page-main-content__quote-box">
						<?=$note;?>
					</div>
				</div>
				<div class="concierge-video-box__text-col">
					<?=apply_filters('the_content',$video_text)?>
				</div>
			</div>
			<?php if($first_step_text){ ?>
		    <div class="concierge-text-illustration-box concierge-text-illustration-box--first-contour">
		      <div class="concierge-text-illustration-box__img-box">
		        <?=$first_step_image;?>
		      </div>
		      <div class="concierge-text-illustration-box__text-box">
		        <?=apply_filters('the_content',$first_step_text);?>
		      </div>
		    </div>
			<?php } ?>
			<?php if($second_step_text){ ?>
		    <div class="concierge-text-illustration-box concierge-text-illustration-box--larger concierge-text-illustration-box--reverse">
		      <div class="concierge-text-illustration-box__img-box">
		        <?=$second_step_image;?>
		      </div>
		      <div class="concierge-text-illustration-box__text-box">
		        <?=apply_filters('the_content',$second_step_text);?>
		      </div>
		    </div>
			<?php } ?>
			<?php if($third_step_text){ ?>
		    <div class="concierge-text-illustration-box concierge-text-illustration-box--second-contour">
		      <div class="concierge-text-illustration-box__img-box">
		        <?=$third_step_image;?>
		      </div>
		      <div class="concierge-text-illustration-box__text-box">
		        <?=apply_filters('the_content',$third_step_text);?>
		      </div>
		    </div>
			<?php } ?>
			<?php if($fourth_step_text){ ?>
		    <div class="concierge-text-illustration-box concierge-text-illustration-box--reverse concierge-text-illustration-box--larger concierge-text-illustration-box--third-contour">
		      <div class="concierge-text-illustration-box__img-box">
		        <?=$fourth_step_image;?>
		      </div>
		      <div class="concierge-text-illustration-box__text-box">
		        <?=apply_filters('the_content',$fourth_step_text);?>
		      </div>
			</div>
			<?php } ?>
			<section class="concierge-pricing-list-box">
				<ul class="concierge-pricing-list">
					<li class="concierge-pricing-list__item">
						<?=apply_filters('the_content',$first_benefit);?>
					</li>
					<li class="concierge-pricing-list__item">
						<?=apply_filters('the_content',$second_benefit);?>
					</li>
					<li class="concierge-pricing-list__item">
						<?=apply_filters('the_content',$third_benefit);?>
					</li>
				</ul>
			</section>
			<?php if($fifth_step_text){ ?>
		    <div class="concierge-text-illustration-box">
		      <div class="concierge-text-illustration-box__img-box">
		        <?=$fifth_step_image;?>
		      </div>
		      <div class="concierge-text-illustration-box__text-box">
		        <?=apply_filters('the_content',$fifth_step_text);?>
		      </div>
		    </div>
			<?php } ?>
		    <?php if($three_cols_block){ ?>
				<section class="concierge-list-box">
					<ul class="concierge-list">
						<?php foreach($three_cols_block as $item){ ?>
							<li class="concierge-list__item">
								<div class="concierge-list__img-box">
									<?=wp_get_attachment_image($item['img'],'medium')?>
								</div>
								<?=apply_filters('the_content',$item['description'])?>
							</li>
						<?php } ?>
					</ul>
				</section>
			<?php } ?>

			<?php

				$compare_items = carbon_get_the_post_meta('compare_litems');
				$compare_okitems = carbon_get_the_post_meta('compare_lokitems');
			?>

				<section class="consulting-concierge-list-box">
					<div class="two-columns-marker-list">
							<div class="two-columns-marker-list__column two-columns-marker-list__column--first">
								<div class="two-columns-marker-list__column-header">
									<div class="two-columns-marker-list__text-box">
									<?php if(isset($postMeta['_compare_ltop'][0]) && $postMeta['_compare_ltop'][0]){ ?>
											<div class="two-columns-marker-list__subtitle"><?=$postMeta['_compare_ltop'][0]?></div>
										<?php } ?>
										<?php if(isset($postMeta['_compare_ltitle'][0]) && $postMeta['_compare_ltitle'][0]){ ?>
											<h3><?=$postMeta['_compare_ltitle'][0]?></h3>
										<?php } ?>
									</div>
									<?php if(isset($postMeta['_compare_lprice'][0]) && $postMeta['_compare_lprice'][0]){ ?>
										<div class="two-columns-marker-list__price"><?=$postMeta['_compare_lprice'][0]?>
											<?php if(isset($postMeta['_compare_lpriceperiod'][0]) && $postMeta['_compare_lpriceperiod'][0]){ ?>
												<span class="two-columns-marker-list__price-subtitle">/ <?=$postMeta['_compare_lpriceperiod'][0]?></span>
											<?php } ?>
										</div>
									<?php } ?>
								</div>
								<?php if($compare_items || $compare_okitems){ ?>
									<ul>
										<?php foreach($compare_items as $item){ ?>
											<li><?=$item['item']?></li>
										<?php } ?>
										<?php foreach($compare_okitems as $item){ ?>
											<li class="green-marker"><?=$item['item']?></li>
										<?php } ?>
									</ul>
								<?php } ?>
								<div class="pay-buttons-list">
									<?php
										if(isset($postMeta['_compare_lpaypal'][0]) && $postMeta['_compare_lpaypal'][0]){
											echo do_shortcode($postMeta['_compare_lpaypal'][0]);
										}
									?>
                                  <span class="pay-buttons-list__middle-text">or</span>
								  <?php
										if(isset($postMeta['_compare_lstripe'][0]) && $postMeta['_compare_lstripe'][0]){
											echo do_shortcode($postMeta['_compare_lstripe'][0]);
										}
									?>
                                </div>
								<?php if(isset($postMeta['_compare_lbottom'][0]) && $postMeta['_compare_lbottom'][0]){ ?>
									<div class="two-columns-marker-list__footnote">
										<?=$postMeta['_compare_lbottom'][0]?>
									</div>
								<?php } ?>
							</div>
					</div>
				</section>

			<section class="panda-team-box">
				<div class="panda-team-box__people-list">
					<div class="team-people-card team-people-card--revert">
					<div class="team-people-card__avatar-box">
						<div class="team-people-card__img-box">
							<?=wp_get_attachment_image($teammeet_img,'medium_large')?>
						</div>
						<?php if($teammeet_position){ ?>
							<div class="team-people-card__tag"><?=$teammeet_position?></div>
						<?php } ?>
					</div>
					<div class="team-people-card__info-box">
						<div class="panda-team-box__header">
						<h2><?=$teammeet_title?></h2>
						</div>
						<?=apply_filters('the_content', $teammeet_description )?>
					</div>
					</div>
				</div>
			</section>
			<div class="schedule-action-box">
				<div class="schedule-action-box__text-box">
					<?=apply_filters('the_content', $sheduleblock_description )?>
				</div>
				<div class="schedule-action-box__img-box">
					<img src="<?=get_template_directory_uri()?>/img/schedule-illustration.png" alt="">
				</div>
				<?php if($sheduleblock_btext){ ?>
					<div class="schedule-action-box__btn-box">
						<a class="schedule-action-box__btn green-link-btn" href="<?=$sheduleblock_burl?>" target="_blank"><?=$sheduleblock_btext?></a>
					</div>
				<?php } ?>
			</div>
	
		    <?php if($testimonial_items){ ?>
				<section class="testimonials-slider-box base-arrows-slider-box">
					<div class="base-arrows-slider-box__header testimonials-slider-box__header">
						<?php if($testimonial_title){ ?>
							<h2><?=$testimonial_title?></h2>
						<?php } ?>
						<div class="testimonials-slider-box__arrows base-arrows-slider-box__arrows">
						<button class="base-arrows-slider-box__prev-arrow">
							<svg width="8" height="16" viewBox="0 0 10 19" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M8.29289 0.909592C8.68342 0.519068 9.31658 0.519068 9.70711 0.909592C10.0976 1.30012 10.0976 1.93328 9.70711 2.32381L8.29289 0.909592ZM1 9.6167L0.292893 10.3238C-0.0976315 9.93328 -0.0976315 9.30012 0.292893 8.90959L1 9.6167ZM9.70711 16.9096C10.0976 17.3001 10.0976 17.9333 9.70711 18.3238C9.31658 18.7143 8.68342 18.7143 8.29289 18.3238L9.70711 16.9096ZM9.70711 2.32381L1.70711 10.3238L0.292893 8.90959L8.29289 0.909592L9.70711 2.32381ZM1.70711 8.90959L9.70711 16.9096L8.29289 18.3238L0.292893 10.3238L1.70711 8.90959Z" fill="#146AFF"/>
							</svg>
						</button>
						<button class="base-arrows-slider-box__next-arrow">
							<svg width="8" height="16" viewBox="0 0 10 19" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M1.70654 1.25473C1.3157 0.864517 0.682536 0.865026 0.292326 1.25586C-0.097885 1.6467 -0.0973765 2.27987 0.293461 2.67008L1.70654 1.25473ZM9 9.94956L9.70654 10.6572C9.89442 10.4697 10 10.2151 10 9.94956C10 9.68407 9.89442 9.42947 9.70654 9.24189L9 9.94956ZM0.293461 17.229C-0.0973765 17.6193 -0.097885 18.2524 0.292326 18.6433C0.682536 19.0341 1.3157 19.0346 1.70654 18.6444L0.293461 17.229ZM0.293461 2.67008L8.29346 10.6572L9.70654 9.24189L1.70654 1.25473L0.293461 2.67008ZM8.29346 9.24189L0.293461 17.229L1.70654 18.6444L9.70654 10.6572L8.29346 9.24189Z" fill="#146AFF"/>
							</svg>
						</button>
						</div>
					</div>
					<div class="swiper-container review-videos-slider" id="reviewVideosSlider">
						<div class="swiper-wrapper">
							<?php foreach($testimonial_items as $item){ ?>
								<div class="swiper-slide">
									<iframe src="<?=$item['video']?>" width="640" height="360" frameborder="0" allow="fullscreen" allowfullscreen></iframe>
								</div>
							<?php } ?>
						</div>
						<div class="base-arrows-slider-box__dots review-videos-slider__dots"></div>
					</div>
				</section>
			<?php } ?>

		    <section class="concierge-ad-box">
				<div class="concierge-ad-box__img-box">
					<img src="<?=get_template_directory_uri()?>/img/concierge-ad-icon.png" alt="booking">
				</div>
				<div class="concierge-ad-box__row">
					<div class="concierge-ad-box__text-box">

					<h2><?=carbon_get_the_post_meta('assistance_title')?></h2>
					<p><?=carbon_get_the_post_meta('assistance_text')?></p>
					</div>
					<div class="concierge-ad-box__price"><?=carbon_get_the_post_meta('price')?></div>
					<div class="concierge-ad-box__btn-box">
					<button onclick="location.href = '<?=carbon_get_the_post_meta('button_link')?>';" class="green-link-btn"><?=carbon_get_the_post_meta('button_title')?></button>
					</div>
				</div>
			</section>

		    <?php if($faq_items){ ?>
				<section class="concierge-faq-box">
					<?php if($faq_title){ ?>
						<div class="concierge-faq-box__header">
							<h2><?=$faq_title?></h2>
						</div>
					<?php } ?>
					<ul class="accordion" id="conciergeFAQAccordion">
						<?php foreach($faq_items as $item){ ?>
							<li class="accordion__item">
								<div class="accordion__header">
									<h4><?=$item['question']?></h4>
								</div>
								<div class="accordion__content">
									<?=apply_filters('the_content',$item['answes'])?>
								</div>
							</li>
						<?php } ?>
					</ul>
				</section>
			<?php } ?>
		</div>
		
	</main>
<?php endwhile; ?>
<?php get_footer(); ?>
