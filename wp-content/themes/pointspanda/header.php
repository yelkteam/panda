<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
  	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head(); ?>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <?=carbon_get_theme_option( 'ga' );?>
  <style>
      @font-face {
        font-family: 'Montserrat';
        font-style: normal;
        font-weight: 400;
        src: local('Montserrat Regular'), local('Montserrat-Regular'),
        url('/wp-content/themes/pointspanda/fonts/montserrat-v15-latin-regular.woff2') format('woff2'),
        url('/wp-content/themes/pointspanda/fonts/montserrat-v15-latin-regular.woff') format('woff');
        font-display: swap;
      }
      @font-face {
        font-family: 'Montserrat';
        font-style: italic;
        font-weight: 400;
        src: local('Montserrat Italic'), local('Montserrat-Italic'),
        url('/wp-content/themes/pointspanda/fonts/montserrat-v15-latin-italic.woff2') format('woff2'),
        url('/wp-content/themes/pointspanda/fonts/montserrat-v15-latin-italic.woff') format('woff');
        font-display: swap;
      }
      @font-face {
        font-family: 'Montserrat';
        font-style: normal;
        font-weight: 500;
        src: local('Montserrat Medium'), local('Montserrat-Medium'),
        url('/wp-content/themes/pointspanda/fonts/montserrat-v15-latin-500.woff2') format('woff2'),
        url('/wp-content/themes/pointspanda/fonts/montserrat-v15-latin-500.woff') format('woff');
        font-display: swap;
      }
      @font-face {
        font-family: 'Montserrat';
        font-style: italic;
        font-weight: 500;
        src: local('Montserrat Medium Italic'), local('Montserrat-MediumItalic'),
        url('/wp-content/themes/pointspanda/fonts/montserrat-v15-latin-500italic.woff2') format('woff2'),
        url('/wp-content/themes/pointspanda/fonts/montserrat-v15-latin-500italic.woff') format('woff');
        font-display: swap;
      }
      @font-face {
        font-family: 'Montserrat';
        font-style: normal;
        font-weight: 600;
        src: local('Montserrat SemiBold'), local('Montserrat-SemiBold'),
        url('/wp-content/themes/pointspanda/fonts/montserrat-v15-latin-600.woff2') format('woff2'),
        url('/wp-content/themes/pointspanda/fonts/montserrat-v15-latin-600.woff') format('woff');
        font-display: swap;
      }
      @font-face {
        font-family: 'Montserrat';
        font-style: italic;
        font-weight: 600;
        src: local('Montserrat SemiBold Italic'), local('Montserrat-SemiBoldItalic'),
        url('/wp-content/themes/pointspanda/fonts/montserrat-v15-latin-600italic.woff2') format('woff2'),
        url('/wp-content/themes/pointspanda/fonts/montserrat-v15-latin-600italic.woff') format('woff');
        font-display: swap;
      }
      @font-face {
        font-family: 'Montserrat';
        font-style: normal;
        font-weight: 700;
        src: local('Montserrat Bold'), local('Montserrat-Bold'),
        url('/wp-content/themes/pointspanda/fonts/montserrat-v15-latin-700.woff2') format('woff2'),
        url('/wp-content/themes/pointspanda/fonts/montserrat-v15-latin-700.woff') format('woff');
        font-display: swap;
      }
      @font-face {
        font-family: 'Montserrat';
        font-style: italic;
        font-weight: 700;
        src: local('Montserrat Bold Italic'), local('Montserrat-BoldItalic'),
        url('/wp-content/themes/pointspanda/fonts/montserrat-v15-latin-700italic.woff2') format('woff2'),
        url('/wp-content/themes/pointspanda/fonts/montserrat-v15-latin-700italic.woff') format('woff');
        font-display: swap;
      }
    </style>
  </head>
  <body <?php body_class(); ?>>
  <header id="mainHeader" class="header-wrapper">
    <div class="main-header wrapper">
	      <div id="logo" class="logo-box">
		      <a href="<?=get_home_url()?>">
            <svg width="96" height="48" viewBox="0 0 96 48" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M38.1784 11.117C40.4733 11.117 42.4152 12.6462 42.4152 14.7965C42.4152 16.899 40.6263 18.4759 38.1784 18.4759H36.0011V23.5889H33.459V11.117H38.1784ZM36.0011 16.3495H38.0489C39.061 16.3495 39.8025 15.8238 39.8025 14.7845C39.8025 13.7094 39.1434 13.2196 38.0489 13.2196H36.0011V16.3495Z" fill="#1B1B32"/>
              <path d="M56.3144 17.341C56.3144 20.8054 53.7135 23.792 49.8885 23.792C46.1578 23.792 43.4626 20.9726 43.4626 17.341C43.4626 13.7213 45.993 10.902 49.8885 10.902C53.4545 10.902 56.3144 13.6496 56.3144 17.341ZM46.0754 17.341C46.0754 19.8139 47.7701 21.6058 49.8885 21.6058C51.854 21.6058 53.7017 19.8258 53.7017 17.341C53.7017 15.0593 51.9128 13.0881 49.8885 13.0881C47.8407 13.0881 46.0754 14.8323 46.0754 17.341Z" fill="#1B1B32"/>
              <path d="M58.3625 23.5769V11.117H60.9753V23.5769H58.3625Z" fill="#1B1B32"/>
              <path d="M72.0498 11.117H74.5919V23.9353L66.3065 16.242V23.5769H63.7644V10.7586L72.0498 18.452V11.117Z" fill="#1B1B32"/>
              <path d="M82.2064 13.2315V23.5769H79.6643V13.2315H76.1807V11.117H85.7018V13.2315H82.2064Z" fill="#1B1B32"/>
              <path d="M90.2917 17.9144C88.3145 17.1737 87.2906 16.015 87.2906 14.223C87.2906 12.2638 89.056 10.902 91.1979 10.902C92.8927 10.902 94.2932 11.5351 95.6466 12.2997L94.5639 14.2947C92.975 13.3987 92.1983 13.0881 91.4215 13.0881C90.1976 13.0881 89.9033 13.8766 89.9033 14.4022C89.9033 14.9398 90.1505 15.4057 90.8095 15.6805L93.1751 16.6123C95.1876 17.4007 95.9997 18.5834 95.9997 20.1245C95.9997 22.3704 94.0225 23.8039 91.6569 23.8039C90.2799 23.8039 88.2086 23.0513 86.5845 22.0239L87.679 20.0169C89.5032 21.1757 90.6683 21.6177 91.7393 21.6177C92.8338 21.6177 93.387 20.9488 93.387 20.1245C93.387 19.2763 92.8574 18.8821 92.163 18.6312L90.2917 17.9144Z" fill="#1B1B32"/>
              <path d="M37.9893 24.4729C40.1783 24.4729 42.0143 25.9304 42.0143 28.1165C42.0143 30.2549 40.3195 31.7601 37.9893 31.7601H35.5178V36.9448H33.47V24.4729H37.9893ZM35.506 29.789H37.9775C39.1662 29.789 39.8841 29.1917 39.8841 28.1165C39.8841 27.0414 39.225 26.4441 37.9775 26.4441H35.506V29.789Z" fill="#1B1B32"/>
              <path d="M45.9691 34.1971L44.7804 36.9448H42.5679L48.4288 24.1265L54.2545 36.9448H52.0419L50.8768 34.1971H45.9691ZM48.4406 28.4271L46.7812 32.2977H50.0883L48.4406 28.4271Z" fill="#1B1B32"/>
              <path d="M64.2813 24.4729H66.3291V37.2912L57.9143 28.8811V36.9328H55.8665V24.1145L64.2813 32.5247V24.4729Z" fill="#1B1B32"/>
              <path d="M73.6731 24.4729C76.8508 24.4729 79.8048 27.2325 79.8048 30.6969C79.8048 34.0658 77.0861 36.7417 73.6731 36.9328H69.095V24.4729H73.6731ZM71.1545 34.9737H73.6731C75.8857 34.9737 77.6864 33.1101 77.6864 30.7089C77.6864 28.2718 75.9563 26.456 73.6731 26.456H71.1428V34.9737H71.1545Z" fill="#1B1B32"/>
              <path d="M83.7592 34.1971L82.5705 36.9448H80.3579L86.2189 24.1265L92.0328 36.9448H89.8202L88.6551 34.1971H83.7592ZM86.2307 28.4271L84.5712 32.2977H87.8783L86.2307 28.4271Z" fill="#1B1B32"/>
              <path d="M12.2046 11.4087C11.9693 11.4206 11.7339 11.4326 11.5103 11.4564C11.016 11.5042 10.5334 11.5759 10.0627 11.6834C9.10939 11.8865 8.20317 12.1971 7.34403 12.6152C7.02627 11.9821 6.90858 11.1817 7.08512 10.3693C7.42642 8.67298 8.86224 7.55003 10.2627 7.84869C11.6633 8.14734 12.5106 9.74814 12.2046 11.4087Z" fill="#1B1B32"/>
              <path d="M19.9958 13.7262C19.3603 13.2723 18.6659 12.8781 17.9598 12.5555C17.489 12.3405 17.0065 12.1493 16.524 11.994C16.371 11.9463 16.2062 11.8865 16.0532 11.8507C16.0297 11.8029 16.0179 11.7432 16.0061 11.6834C15.5942 10.011 16.3945 8.37433 17.795 8.01595C19.1955 7.66951 20.6549 8.74467 21.0668 10.4171C21.3728 11.7551 20.9138 13.0811 19.9958 13.7262Z" fill="#1B1B32"/>
              <path d="M18.0775 12.1016C18.0775 12.2569 18.0304 12.4241 17.948 12.5555C17.4773 12.3405 16.9947 12.1494 16.5122 11.9941C16.5593 11.6237 16.8888 11.3251 17.2654 11.3131C17.7126 11.3012 18.0657 11.6596 18.0775 12.1016Z" fill="white"/>
              <path d="M11.5339 11.2534C11.5339 11.325 11.5222 11.3848 11.5104 11.4564C11.0161 11.5042 10.5336 11.5759 10.0628 11.6834C9.99219 11.564 9.94511 11.4325 9.94511 11.2892C9.93334 10.8352 10.2864 10.4769 10.7336 10.4649C11.1691 10.4649 11.5222 10.8233 11.5339 11.2534Z" fill="white"/>
              <path d="M7.75554 3.05176e-05C7.885 0.0358692 8.14392 0.0597616 8.34399 0.179224C9.30905 0.788481 10.0387 1.58888 10.3447 2.73572C10.5095 3.35692 10.3683 3.61974 9.77981 3.8945C9.10898 4.21705 8.4146 4.5157 7.77908 4.89798C7.33185 5.1608 6.94348 5.54308 6.54333 5.88952C6.0608 6.30764 5.76658 6.30764 5.27228 5.88952C4.47198 5.19664 4.02476 4.30067 3.85999 3.2494C3.78938 2.83129 3.87176 2.43706 4.11891 2.10257C4.94274 1.02741 5.97842 0.274794 7.32009 0.023923C7.42601 0.0119767 7.53193 0.0119767 7.75554 3.05176e-05Z" fill="#1B1B32"/>
              <path d="M24.4912 3.14189C24.3265 4.57543 23.7616 5.54308 22.7847 6.25985C22.4081 6.53462 22.1139 6.45099 21.6667 6.08066C21.0547 5.57892 20.4427 5.06523 19.7954 4.61127C19.4188 4.34846 18.9598 4.21705 18.5479 4.02591C17.9712 3.76309 17.8182 3.47638 17.9948 2.85518C18.3125 1.68445 19.0775 0.860158 20.1014 0.322578C20.4192 0.155331 20.8782 0.179223 21.243 0.250901C22.4434 0.525664 23.4085 1.21855 24.1264 2.22203C24.3382 2.54458 24.4206 2.95075 24.4912 3.14189Z" fill="#1B1B32"/>
              <path d="M0.812256 13.344C0.929946 12.9736 1.00056 12.5675 1.1771 12.221C1.48309 11.6237 2.18923 11.3609 2.81299 11.552C4.28412 11.994 5.29625 14.0368 4.76665 15.5062C4.40181 16.5217 3.48382 16.88 2.5423 16.3544C1.53017 15.8049 0.859332 14.6103 0.812256 13.344Z" fill="#1B1B32"/>
              <path d="M11.4157 23.2593L8.46167 21.2046C8.70882 21.0134 8.9442 20.894 9.5915 21.0732H9.60326C9.61503 21.0851 9.6268 21.0851 9.63857 21.0851L13.334 22.1961L11.4157 23.2593Z" fill="#1B1B32"/>
              <path d="M15.3701 21.5391C16.3469 21.0015 17.3943 20.5236 18.3476 20.7148C18.583 20.7626 18.7125 20.8581 18.736 20.9895C18.7478 21.0015 18.7478 21.0254 18.7478 21.0373C18.7478 21.4316 17.9946 22.0528 17.818 22.1603C17.6768 22.2558 17.006 22.6501 15.9468 23.2713L15.8879 23.2952L14.5109 24.0358V24.0478L12.6397 28.8143L11.8747 29.3161L12.5455 25.099L9.06188 26.9746L8.34397 27.7989C8.15567 28.0139 7.77906 28.0856 7.69668 28.0378C7.62606 27.99 7.63783 27.9183 7.73198 27.763C7.74375 27.7511 7.74375 27.7511 7.74375 27.7511L8.2969 26.7835L6.42562 25.3738L6.40209 25.3499C6.39032 25.338 6.36678 25.338 6.35501 25.3141C6.23732 25.2305 6.11963 25.1349 6.11963 25.0632C6.1314 25.0274 6.14317 25.0035 6.19024 24.9796C6.35501 24.8482 6.63747 24.7287 6.63747 24.7168H6.64923C6.64923 24.7168 6.89638 24.5017 7.52014 24.8243L8.49697 25.3141L11.757 23.5341L13.9107 22.3514L15.3701 21.5391Z" fill="#1B1B32"/>
              <path d="M25.3152 24.1434C25.3152 19.8786 23.2673 16.0916 20.1133 13.7621C20.0897 13.7501 20.0544 13.7143 20.0309 13.7024C19.3953 13.2364 18.701 12.8303 17.9948 12.4958C17.5241 12.2688 17.0415 12.0777 16.5472 11.9104C16.4296 11.8626 16.3119 11.8387 16.1942 11.8029C16.1353 11.791 16.0882 11.7671 16.0294 11.7551C14.9584 11.4565 13.8286 11.2892 12.6635 11.2892C12.5575 11.2892 12.4634 11.2892 12.3692 11.3012C12.3222 11.3012 12.2751 11.3012 12.2398 11.3131C12.0044 11.3131 11.7808 11.3251 11.5454 11.3489C11.0276 11.3967 10.5215 11.4804 10.0272 11.5879C9.14453 11.779 8.29716 12.0538 7.49686 12.4241C7.43802 12.448 7.3674 12.4838 7.30856 12.5197C6.64949 12.8183 6.02574 13.2006 5.43729 13.6068C5.64913 14.3474 5.63736 15.1001 5.40198 15.7452C5.06068 16.7128 4.30746 17.2862 3.40125 17.2862C3.01287 17.2862 2.61272 17.1787 2.22434 16.9637C2.21258 16.9517 2.18904 16.9517 2.17727 16.9398C0.800293 18.9826 0 21.4674 0 24.1434C0 26.1145 0.447223 27.9781 1.23575 29.6506H1.22398C5.20191 38.9806 12.6635 48 12.6635 48C12.6635 48 20.125 38.9806 24.1029 29.6506H24.0912C24.8679 27.9781 25.3152 26.1025 25.3152 24.1434ZM12.6517 31.2872C8.75615 31.2872 5.60205 28.0856 5.60205 24.1314C5.60205 20.1772 8.75615 16.9756 12.6517 16.9756C16.5472 16.9756 19.7013 20.1772 19.7013 24.1314C19.7013 28.0976 16.5472 31.2872 12.6517 31.2872Z" fill="#1B1B32"/>
            </svg>
          </a>
		  </div>
		  <?php
			wp_nav_menu( [
				'theme_location'  => 'main_menu',
				'menu'            => '',
				'container'       => 'nav',
				'container_class' => 'menu-box',
				'container_id'    => 'mainMenu',
				'menu_class'      => '',
				'echo'            => true,
				'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul><button id="closeMenu" class="menu-box__close-btn"></button>',
			] );
			?>
			<div id="searchBox" class="search-box">
				<div class="search-box__btn-box">
					<button id="searchBtn" class="search-box__btn"></button>
				</div>
				<button id="menuToggle" class="menu-toggle"></button>
			</div>
			<form class="search-form" id="searchForm" action="<?php echo home_url( '/' ) ?>">
				<input id="searchInput" class="search-form__input" type="text" placeholder="" name="s" autocomplete="off">
				<button id="closeSearchBtn" class="search-form__close-btn" type="button"></button>
				<div id="searchResults" class="search-form__result-list">
				</div>
			</form>
      	</div>
	</header>
