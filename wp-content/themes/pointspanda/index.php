<?php get_header();?>
<?php
    $ccitems = carbon_get_theme_option( 'top_ccdeals' );
    $cctitle = carbon_get_theme_option( 'top_ccdealstitle' );

    $banner_img = carbon_get_theme_option( 'sbanner_img' );
    $banner_img1 = carbon_get_theme_option( 'sbanner_img1' );
    $banner_url = carbon_get_theme_option( 'sbanner_url' );

    $current_cat = get_query_var( 'cat' ) ;
    $placeholder = carbon_get_theme_option( 'defimage' );

    if($current_cat){
        $top_category = $popular_category = $current_cat;
    }else{
        $top_category = carbon_get_the_post_meta('top_last_posts');
        $popular_category = carbon_get_the_post_meta('top_last_posts');
    }

    $publposts = get_posts( array(
        'category' => $current_cat,
        'numberposts' => -1,
        'post_type'   => 'post',
        'post_status'=>'publish',
    ) );

    $published_posts = count($publposts);

    $default_posts_per_page = get_option( 'posts_per_page' );

    $cards_page = carbon_get_theme_option( 'ccpage' );

    $blog_subscr_title = carbon_get_post_meta(get_option( 'page_for_posts' ),'blog_subscr_title');
    $blog_subscr_subtitle = carbon_get_post_meta(get_option( 'page_for_posts' ),'blog_subscr_subtitle');
    $blog_subscr_image = carbon_get_post_meta(get_option( 'page_for_posts' ),'blog_subscr_image');
?>

<main class="blog-main-page-main-content">
	<div class="wrapper">
        <section class="breadcrumbs-box">
            <?php panda_breadcrumbs(); ?>
		</section>
		<section class="top-articles-box">
			<div class="top-articles-sidebar">
				<div class="top-articles-sidebar__header">
					<h4><?=__('Top articles','panda')?></h4>
					<svg width="18" height="24" viewBox="0 0 18 24" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M17.9499 14.0685C17.9491 14.0572 17.9483 14.0459 17.9474 14.0346C17.6685 10.4652 16.005 8.22618 14.5371 6.24998C13.1758 4.41754 12 2.83514 12 0.500856C12 0.313356 11.895 0.141981 11.7285 0.0560595C11.5615 -0.030378 11.3608 -0.0162217 11.209 0.094122C9.00098 1.67409 7.1587 4.33701 6.51516 6.87783C6.47138 7.05117 6.43132 7.22663 6.39466 7.40335C5.91577 9.7124 3.83739 10.7632 3.49514 8.42995C3.47217 8.27175 3.37552 8.13408 3.23489 8.05889C3.09281 7.98468 2.9258 7.97929 2.78175 8.05059C2.67483 8.10234 0.157219 9.38156 0.0107342 14.4891C0.000468576 14.659 0 14.8295 0 14.9999C0 19.9619 4.03758 23.9992 9 23.9992C9.00456 23.9996 9.00933 24.0001 9.01375 24C9.01795 23.9999 9.02214 23.9992 9.02634 23.9992C13.9766 23.985 18 19.9531 18 14.9999C18 14.7733 17.9577 14.1763 17.9499 14.0685ZM9 22.9993C7.34569 22.9993 6 21.5658 6 19.8037C6 19.7437 5.99953 19.6831 6.00389 19.6089C6.02118 18.967 6.83723 18.932 7.44655 19.1346C7.60537 19.1874 7.77972 19.2169 7.97072 19.2169C8.24709 19.2169 8.47073 18.9932 8.47073 18.7169C8.47073 18.0051 8.48541 17.1838 8.66264 16.4426C8.66436 16.4355 8.6661 16.4283 8.66787 16.4211C8.89063 15.519 10.171 15.6567 10.7056 16.4167C11.2843 17.2389 11.8824 18.089 11.9874 19.5386C11.9937 19.6245 12.0001 19.711 12.0001 19.8037C12 21.5658 10.6543 22.9993 9 22.9993Z" fill="#DD516B" fill-opacity="0.9"/>
					</svg>
				</div>
				<div class="side-last-news-list">
                    <?php
                        $latest_posts = get_latest_posts($top_category, 6);
                        if($latest_posts){
                    ?>
                        <ul>
                            <?php foreach($latest_posts as $lpost){ ?>
                                <li class="side-last-news-list__item">
                                    <div class="side-last-news-list__img-box">
                                        <?php
                                            if(has_post_thumbnail($lpost->ID)){
                                                echo get_the_post_thumbnail($lpost->ID,'icon');
                                            }else{
                                                echo wp_get_attachment_image($placeholder,'icon');
                                            }
                                        ?>
                                    </div>
                                    <a href="<?=get_the_permalink($lpost->ID)?>"><?=$lpost->post_title?></a>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
				</div>
            </div>
            <?php
                $popular = get_most_popular_post_in_category($popular_category, 1);
            ?>
			<div class="top-articles-slider-box">
                <?php if($popular){ ?>
                    <ul id="topArticlesSlider" class="top-articles-slider">
                        <?php foreach($popular as $ppost){ ?>
                            <li class="slide top-article-card">
                                <a href="<?=get_the_permalink($ppost->ID)?>">
                                    <div class="top-article-card__img-box">
                                        <?php
                                            if(has_post_thumbnail($ppost->ID)){
                                                echo get_the_post_thumbnail($ppost->ID,'large');
                                            }else{
                                                echo wp_get_attachment_image($placeholder,'large');
                                            }
                                        ?>
    								</div>
    								<?php

    									$posted = human_time_diff(get_the_time('U',$ppost));
    								?>
    								<div class="top-article-card__tag"><?=__('Trending','panda')?></div>
                                    <h2 class="top-article-card__title"><?=$ppost->post_title?></h2>
                                    <span class="top-article-card__view-all-link">
                                        <svg width="56" height="56" viewBox="0 0 56 56" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <circle cx="28" cy="28" r="27.5" stroke="white"/>
                                            <path d="M18 27.4C17.6686 27.4 17.4 27.6686 17.4 28C17.4 28.3314 17.6686 28.6 18 28.6V27.4ZM38.4243 28.4243C38.6586 28.1899 38.6586 27.8101 38.4243 27.5757L34.6059 23.7574C34.3716 23.523 33.9917 23.523 33.7574 23.7574C33.523 23.9917 33.523 24.3716 33.7574 24.6059L37.1515 28L33.7574 31.3941C33.523 31.6284 33.523 32.0083 33.7574 32.2426C33.9917 32.477 34.3716 32.477 34.6059 32.2426L38.4243 28.4243ZM18 28.6H38V27.4H18V28.6Z" fill="white"/>
                                        </svg>
                                    </span>
    								<div class="top-article-card__date"><?=get_the_date('F d, Y',$ppost->ID)?></div>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                    <div id="topArticlesSliderControls" class="slider-controls top-articles-slider__controls"></div>
                <?php } ?>
			</div>
		</section>
		<section class="main-blog-content">
			<section class="filtered-articles-box">
				<section class="articles-filters-box">
					<div id="categoriesDropdownList" class="categories-dropdown-list">
		                <?/*php
		                    $blogpage_id = get_option('page_for_posts');
		                    if($blogpage_id){
		                        if(!is_front_page() && is_home()){
		                            ?>
		                                <span class="categories-list__item categories-list__item--active"><?=__('All articles','panda')?></span>
		                            <?php
		                        }else{
		                            ?>
		                                <a href="<?=get_the_permalink($blogpage_id)?>" class="categories-list__item"><?=__('All articles','panda')?></a>
		                            <?php
		                        }
		                    }
		                    $categories = get_categories( [
		                        'taxonomy'     => 'category',
		                        'type'         => 'post',
		                        'child_of'     => 0,
		                        'parent'       => '',
		                        'orderby'      => 'name',
		                        'order'        => 'ASC',
		                        'hide_empty'   => 1,
		                        //'hierarchical' => 1,
		                    ] );

		                    if($categories){
		                        foreach($categories as $category){
		                            if($current_cat && $current_cat == $category->term_id ){
		                                ?>
		                                    <span class="categories-list__item categories-list__item--active"><?=$category->name?></span>
		                                <?php
		                            }else{
		                                ?>
		                                    <a href="<?=get_category_link($category->term_id)?>" class="categories-list__item"><?=$category->name?></a>
		                                <?php
		                            }
		                        }
		                    }  */
		                ?>
		                <div class="categories-dropdown-list__selected-item">
		                	<?php
		                	$blogpage_id = get_option('page_for_posts');
		                    if($blogpage_id){
		                        if(!is_front_page() && is_home()){
		                	?>
									<span>Category: <span class="categories-dropdown-list__category-text"><?=__('All articles','panda')?></span></span>
							<?php
								}else{
									$current_cat_obj = get_term($current_cat);?>
									<span>Category: <span class="categories-dropdown-list__category-text"><?=$current_cat_obj->name?></span></span>
							<?php
								}
							}
							?>
							<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M4.8 6.3999L8.00009 9.5999L11.2 6.39992" stroke="#146AFF" stroke-width="1.6" stroke-linecap="round" stroke-linejoin="round"/></svg>
						</div>
						<ul class="categories-dropdown-list__items">
							<li class="categories-dropdown-list__main-category-item"><a href="<?=get_the_permalink($blogpage_id)?>"><?=__('All articles','panda')?></a></li>
							<?php
							$categories = get_categories( [
		                        'taxonomy'     => 'category',
		                        'type'         => 'post',
		                        'child_of'     => 0,
		                        'parent'       => '',
		                        'orderby'      => 'count',
		                        'order'        => 'DESC',
		                        'hide_empty'   => 1,
		                        'hierarchical' => 1,
		                    ] );

		                    if($categories){
		                        foreach($categories as $category){
		                        	if($category->parent==0){
		                    ?>
								<li class="categories-dropdown-list__main-category-item"><a href="<?=get_category_link($category->term_id)?>"><?=$category->name?></a></li>

							<?php
							$args = array(
                                'orderby' => 'count',
                                'order' => 'DESC',
							    'parent' => $category->term_id,
							);

							$terms = get_terms('category', $args);
							foreach($terms as $term){
							?>
								<li class="categories-dropdown-list__subcategory-item"><a href="<?=get_category_link($term->term_id)?>"><?=$term->name?> <span class="categories-dropdown-list__count-text">(<?=$term->count?>)</span></a></li>
							<?php
										}
									}
								}
							}
							?>
						</ul>
					</div>
                    <?php
                        $quoteText = '';
                        if(!is_front_page() && is_home()):
                            $quoteText = apply_filters('the_content',carbon_get_post_meta(get_option( 'page_for_posts' ),'blog_quote'));
                        else:
                            $quoteText = category_description();
                        endif;

                        if($quoteText){
                    ?>
                        <div class="blog-blockquote">
                            <div class="blog-blockquote__img-box">
                                <img src="<?php echo get_stylesheet_directory_uri();?>/img/blog-main-page/blog-blockquote.png" alt="">
                            </div>
                            <?=$quoteText?>
                        </div>
                    <?php } ?>
				</section>
				<div class="horizontal-articles-list">
                    <?php
                        $i = 0;
                        while ( have_posts() ) : the_post();

                            if($i%3 == 2 ){
                                get_template_part( 'template-parts/list_article','nodescription' );
                            }else{
                                get_template_part( 'template-parts/list_article','description' );
                            }
                            $i++;

                        endwhile;
                    ?>
                </div>
                <?php if($published_posts > $default_posts_per_page){ ?>
                    <div class="view-all-link-box" id="view-all-link-box" data-page="1" data-cat="<?=($current_cat)?$current_cat:0?>" >
                        <a href="#">
                            <span class="view-all-link-box__arrow"></span>
                            <span class="view-all-link-box__text"><?=__('More Articles','panda')?></span>
                        </a>
                    </div>
                <?php } ?>
			</section>
			<section class="credit-cards-sidebar">
                <?php if($ccitems){ ?>
                    <div class="credit-cards-sidebar__header">
                        <h4><?=$cctitle?></h4>
                        <?php if(isset($cards_page[0]['id'])){ ?>
                            <a href="<?=get_the_permalink($cards_page[0]['id'])?>"><?=__('See all','panda')?></a>
                        <?php } ?>
                    </div>
                    <ul class="credit-card-box-vertical-list">
                        <?php
                            foreach($ccitems as $i => $item){
                                $clink = carbon_get_post_meta($item['id'], 'card_url');
								if ( empty($clink) ) {
									$clink = '#';
								}
                            ?>
                            <li class="credit-card-box__item">
                                <a class="credit-card-box__card-link" href="<?=$clink?>" rel="noopener noreferrer">
                                    <div class="credit-card-box__img-box">
                                        <?php
                                            if(has_post_thumbnail($item['id'])){
                                                echo get_the_post_thumbnail($item['id'],'small_card');
                                            }else{
                                                echo wp_get_attachment_image($placeholder,'small_card');
                                            }
                                        ?>
                                    </div>
                                    <div class="credit-card-box__card-info">
                                        <?php if($i == 0){ ?>
											<div class="credit-card-box__subtitle"><?=__('Best Total Value','panda')?></div>
										<?php } ?>
                                        <div class="credit-card-box__title"><?=get_the_title($item['id'])?></div>
                                        <div class="credit-card-box__info">
                                        <?php
											$current = carbon_get_post_meta($item['id'],'current_offer');
											if($current){
												?>
												<span class="credit-card-box__mark-text"><?=__('Current offer:','panda')?></span> <?=$current?>
												<?php
											}else{
												$in_bonus = carbon_get_post_meta($item['id'],'text_offer');
												echo $in_bonus;
											}
										?>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                    <div id="creditCardsSlider" class="credit-card-box__header">
                        <h2><?=$cctitle?></h2>
                        <div class="credit-cards-slider">
                            <?php
                                foreach($ccitems as $i => $item){
                                    $clink = carbon_get_post_meta($item['id'], 'card_url');
                                    if ( empty($clink) ) {
                                        $clink = '#';
                                    }
                                ?>
                                <div class="slide credit-cards-slider__item credit-card-box__item">
                                    <a class="credit-card-box__card-link" href="<?=$clink?>" rel="noopener noreferrer">
                                        <div class="credit-card-box__img-box">
                                        <?php
                                            if(has_post_thumbnail($item['id'])){
                                                echo get_the_post_thumbnail($item['id'],'small_card');
                                            }else{
                                                echo wp_get_attachment_image($placeholder,'small_card');
                                            }
                                        ?>
                                        </div>
                                        <div class="credit-card-box__card-info">
                                            <?php if($i == 0){ ?>
                                                <div class="credit-card-box__subtitle"><?=__('Best Total Value','panda')?></div>
                                            <?php } ?>
                                            <div class="credit-card-box__title"><?=get_the_title($item['id'])?></div>
                                            <div class="credit-card-box__info">
                                                <?php
                                                    $current = carbon_get_post_meta($item['id'],'current_offer');
                                                    if($current){
                                                        ?>
                                                        <span class="credit-card-box__mark-text"><?=__('Current offer:','panda')?></span> <?=$current?>
                                                        <?php
                                                    }else{
                                                        $in_bonus = carbon_get_post_meta($item['id'],'text_offer');
                                                        echo $in_bonus;
                                                    }
                                                ?>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            <?php } ?>
                            <div id="creditCardsSliderControls" class="credit-cards-slider__controls"></div>
                        </div>
                        <?php if(isset($cards_page[0]['id'])){ ?>
                            <a class="credit-card-box__link--transparent" href="<?=get_the_permalink($cards_page[0]['id'])?>">
                                <span><?=__('See all','panda')?></span>
                                <svg width="42" height="10" viewBox="0 0 42 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 4.4C0.668629 4.4 0.4 4.66863 0.4 5C0.4 5.33137 0.668629 5.6 1 5.6V4.4ZM41.4243 5.42426C41.6586 5.18995 41.6586 4.81005 41.4243 4.57574L37.6059 0.757359C37.3716 0.523045 36.9917 0.523045 36.7574 0.757359C36.523 0.991674 36.523 1.37157 36.7574 1.60589L40.1515 5L36.7574 8.39411C36.523 8.62843 36.523 9.00833 36.7574 9.24264C36.9917 9.47696 37.3716 9.47696 37.6059 9.24264L41.4243 5.42426ZM1 5.6H41V4.4H1V5.6Z" fill="#146AFF"/>
                                </svg>
                            </a>
                        <?php } ?>
                    </div>
                <?php } ?>

                <div class="credit-cards-sidebar__ad-box">
					<iframe
						src="https://o1.qnsr.com/sjsc/o1/ff2.html?;p=src%3D664331;n=203;c=1670329/485404/223057;s=7273;d=40;w=300;h=240" frameborder=0 marginheight=0 marginwidth=0 scrolling="no" allowTransparency="true" width=300 height=240>
                    </iframe>
                </div>
			</section>
      <section class="home-page-subscribe-form-box">
        <div class="subscribe-form-box">
          <div class="subscribe-form-box__wrapper">
            <div class="subscribe-form-box__info">
              <h3 class="subscribe-form-box__title"><?=$blog_subscr_title;?></h3>
              <p class="subscribe-form-box__description"><?=$blog_subscr_subtitle;?></p>
            </div>
            <?php get_template_part('template-parts/subscribe-form'); ?>
          </div>
        </div>
      </section>
        </section>
    </div>
</main>
<?php get_footer();?>
