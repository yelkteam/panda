<?php 
	// Template Name: Cards
?>
<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
	<?php 
		$pagetoptext = carbon_get_the_post_meta('pagetoptext');

		$cardfilter_show = carbon_get_theme_option( 'cardfilter_show' );
	?>

	<main class="credit-cards-page-main-content">
	  <div class="wrapper">
	    <section class="breadcrumbs-box">
	      <?php panda_breadcrumbs(); ?>
	    </section>
	    <section class="main-header-with-image main-header-with-image--big-header">
	      <div class="main-header-with-image__info-box">
	        <?=apply_filters('the_content',$pagetoptext)?>	
	      </div>
	      <div class="main-header-with-image__img-box">
	        <?=get_the_post_thumbnail(null,'full')?>
	      </div>
	    </section>
		<?php 
			if($cardfilter_show){
				$terms = get_terms( [
					'taxonomy' => 'card-category',
					'hide_empty' => false,
				] );
				if($terms){ 
			?>
					<section class="tabs-categories-box">
					<ul id="tabsCategoriesList" class="tabs-categories-list">
						<li class="tabs-categories-list__item">
							<span class="tabs-categories-list__link tabs-categories-list__item--active">
								<span class="tabs-categories-list__img-box">
									<img src="<?=get_template_directory_uri()?>/img/all-categories.svg" alt="all cats" >
								</span>
								<span class="tabs-categories-list__text"><?=__('All','panda')?></span>
							</span>
						</li>
						<?php foreach ($terms as $term) { ?>
							<?php 
								$icon = carbon_get_term_meta( $term->term_id, 'cicon' );
								$name = carbon_get_term_meta( $term->term_id, 'shortname' );
								if(!$name){
									$name =$term->name; 
								}
							?>
							<li class="tabs-categories-list__item">
								<a class="tabs-categories-list__link" href="<?=get_term_link($term->term_id)?>">
									<span class="tabs-categories-list__img-box">
										<?=wp_get_attachment_image($icon,'icon')?>
									</span>
									<span class="tabs-categories-list__text"><?=$name?></span>
								</a>
							</li>
						<?php } ?>
					</ul>
					</section>
			<?php } ?>
		<?php } ?>
	    <section class="credit-cards-list-box">
	    <?php 
	    	$posts = get_posts( array(
				'numberposts' => -1,
				'orderby'     => 'date',
				'order'       => 'DESC',
				'post_type'   => 'cards',
			) );
	    	if($posts){
	    		?>
	      			<ul class="credit-cards-list">
	    		<?php
				foreach( $posts as $post ){
					setup_postdata($post);

					$clink = carbon_get_the_post_meta('card_url');

					?>
					 <li class="credit-cards-list__item">
			          <h2><?=get_the_title()?></h2>
			          <div class="credit-cards-list__card-box">
			            <div class="credit-cards-list__img-box">
			              <?=get_the_post_thumbnail(null,'big_thumbnail')?>
			            </div>
			            <div class="card-statistics">
			              <div class="card-statistics__first-row">
			                <div class="card-statistics__item">
			                  <div class="card-statistics__title">Intro Bonus</div>
			                  <div class="card-statistics__value">60,000 points</div>
			                </div>
			                <div class="card-statistics__item">
			                  <div class="card-statistics__title">Annual Fee</div>
			                  <div class="card-statistics__value">$95</div>
			                </div>
			                <div class="card-statistics__item">
			                  <div class="card-statistics__title">Regular APR</div>
			                  <div class="card-statistics__value">17.49% - 24.49% variable</div>
			                </div>
			              </div>
			              <div class="card-statistics__second-row">
			                <div class="card-statistics__diagram-icon">
			                  <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
			                    <rect y="25.2632" width="4" height="6.73684" rx="2" fill="#146AFF"/>
			                    <path d="M14 14.6316C14 13.527 14.8954 12.6316 16 12.6316C17.1046 12.6316 18 13.527 18 14.6316V30C18 31.1046 17.1046 32 16 32C14.8954 32 14 31.1046 14 30V14.6316Z" fill="#146AFF"/>
			                    <path d="M7 21.3684C7 20.2638 7.89543 19.3684 9 19.3684C10.1046 19.3684 11 20.2638 11 21.3684V30C11 31.1046 10.1046 32 9 32C7.89543 32 7 31.1046 7 30V21.3684Z" fill="#146AFF"/>
			                    <path d="M21 8.73681C21 7.63225 21.8954 6.73682 23 6.73682C24.1046 6.73682 25 7.63225 25 8.73682V30C25 31.1045 24.1046 32 23 32C21.8954 32 21 31.1045 21 30V8.73681Z" fill="#D7E3F6"/>
			                    <path d="M28 2C28 0.895431 28.8954 0 30 0C31.1046 0 32 0.895431 32 2V30C32 31.1046 31.1046 32 30 32C28.8954 32 28 31.1046 28 30V2Z" fill="#D7E3F6"/>
			                  </svg>
			                </div>
			                <div class="card-statistics__credit-rating">
			                  <div class="card-statistics__value">Good to Excellent</div>
			                  <div class="card-statistics__regular-text">Credit Recommended (670-850)</div>
			                </div>
			                <div class="card-statistics__stars-box">
			                  <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
			                    <path d="M8.53167 1.25247C8.69364 0.819299 9.30636 0.819299 9.46833 1.25247L11.3528 6.29217C11.4231 6.48009 11.5989 6.60782 11.7993 6.61657L17.1747 6.85146C17.6367 6.87165 17.826 7.45437 17.4641 7.74227L13.2534 11.0919C13.0964 11.2168 13.0293 11.4234 13.0829 11.6168L14.5206 16.8016C14.6441 17.2473 14.1484 17.6074 13.7628 17.3522L9.27595 14.3826C9.10865 14.2719 8.89135 14.2719 8.72405 14.3826L4.23722 17.3522C3.85157 17.6074 3.35587 17.2473 3.47945 16.8016L4.91714 11.6168C4.97075 11.4234 4.9036 11.2168 4.74659 11.0919L0.535885 7.74227C0.173966 7.45437 0.363304 6.87165 0.825328 6.85146L6.2007 6.61657C6.40114 6.60782 6.57693 6.48009 6.6472 6.29217L8.53167 1.25247Z" fill="#FFA800"/>
			                  </svg>
			                  <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
			                    <path d="M8.53167 1.25247C8.69364 0.819299 9.30636 0.819299 9.46833 1.25247L11.3528 6.29217C11.4231 6.48009 11.5989 6.60782 11.7993 6.61657L17.1747 6.85146C17.6367 6.87165 17.826 7.45437 17.4641 7.74227L13.2534 11.0919C13.0964 11.2168 13.0293 11.4234 13.0829 11.6168L14.5206 16.8016C14.6441 17.2473 14.1484 17.6074 13.7628 17.3522L9.27595 14.3826C9.10865 14.2719 8.89135 14.2719 8.72405 14.3826L4.23722 17.3522C3.85157 17.6074 3.35587 17.2473 3.47945 16.8016L4.91714 11.6168C4.97075 11.4234 4.9036 11.2168 4.74659 11.0919L0.535885 7.74227C0.173966 7.45437 0.363304 6.87165 0.825328 6.85146L6.2007 6.61657C6.40114 6.60782 6.57693 6.48009 6.6472 6.29217L8.53167 1.25247Z" fill="#FFA800"/>
			                  </svg>
			                  <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
			                    <path d="M8.53167 1.25247C8.69364 0.819299 9.30636 0.819299 9.46833 1.25247L11.3528 6.29217C11.4231 6.48009 11.5989 6.60782 11.7993 6.61657L17.1747 6.85146C17.6367 6.87165 17.826 7.45437 17.4641 7.74227L13.2534 11.0919C13.0964 11.2168 13.0293 11.4234 13.0829 11.6168L14.5206 16.8016C14.6441 17.2473 14.1484 17.6074 13.7628 17.3522L9.27595 14.3826C9.10865 14.2719 8.89135 14.2719 8.72405 14.3826L4.23722 17.3522C3.85157 17.6074 3.35587 17.2473 3.47945 16.8016L4.91714 11.6168C4.97075 11.4234 4.9036 11.2168 4.74659 11.0919L0.535885 7.74227C0.173966 7.45437 0.363304 6.87165 0.825328 6.85146L6.2007 6.61657C6.40114 6.60782 6.57693 6.48009 6.6472 6.29217L8.53167 1.25247Z" fill="#FFA800"/>
			                  </svg>
			                  <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
			                    <path d="M8.53167 1.25247C8.69364 0.819299 9.30636 0.819299 9.46833 1.25247L11.3528 6.29217C11.4231 6.48009 11.5989 6.60782 11.7993 6.61657L17.1747 6.85146C17.6367 6.87165 17.826 7.45437 17.4641 7.74227L13.2534 11.0919C13.0964 11.2168 13.0293 11.4234 13.0829 11.6168L14.5206 16.8016C14.6441 17.2473 14.1484 17.6074 13.7628 17.3522L9.27595 14.3826C9.10865 14.2719 8.89135 14.2719 8.72405 14.3826L4.23722 17.3522C3.85157 17.6074 3.35587 17.2473 3.47945 16.8016L4.91714 11.6168C4.97075 11.4234 4.9036 11.2168 4.74659 11.0919L0.535885 7.74227C0.173966 7.45437 0.363304 6.87165 0.825328 6.85146L6.2007 6.61657C6.40114 6.60782 6.57693 6.48009 6.6472 6.29217L8.53167 1.25247Z" fill="#FFA800"/>
			                  </svg>
			                  <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
			                    <path d="M8.53167 1.25247C8.69364 0.819299 9.30636 0.819299 9.46833 1.25247L11.3528 6.29217C11.4231 6.48009 11.5989 6.60782 11.7993 6.61657L17.1747 6.85146C17.6367 6.87165 17.826 7.45437 17.4641 7.74227L13.2534 11.0919C13.0964 11.2168 13.0293 11.4234 13.0829 11.6168L14.5206 16.8016C14.6441 17.2473 14.1484 17.6074 13.7628 17.3522L9.27595 14.3826C9.10865 14.2719 8.89135 14.2719 8.72405 14.3826L4.23722 17.3522C3.85157 17.6074 3.35587 17.2473 3.47945 16.8016L4.91714 11.6168C4.97075 11.4234 4.9036 11.2168 4.74659 11.0919L0.535885 7.74227C0.173966 7.45437 0.363304 6.87165 0.825328 6.85146L6.2007 6.61657C6.40114 6.60782 6.57693 6.48009 6.6472 6.29217L8.53167 1.25247Z" fill="#D7E3F6"/>
			                  </svg>
			                </div>
			                <div class="card-statistics__mark">4.6/5</div>
			              </div>
			            </div>
			          </div>
			         	<?php the_content(); ?>
			         	<?php if($clink){ ?>
			          		<a class="read-more-link" href="<?=$clink?>"><?=__('Learn How to Apply','panda')?></a>
			          	<?php } ?>
			        </li>
					<?php
				}
				?>
					</ul>
				<?php 
			}
			wp_reset_postdata(); // сброс
	    ?>
		    <?php get_template_part('template-parts/share','vertical'); ?>
	    </section>
	  </div>
	</main>
	<?php endwhile; ?>

<?php get_footer(); ?>