<?php 
	// Template Name: Service
?>
<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<?php 
	$pagetoptext = carbon_get_the_post_meta('pagetoptext');
	$three_cols_block = carbon_get_the_post_meta('tcols_items');

	$compare_block_data = get_compare_block_data(get_the_ID());
	
	
	$compare_block_side_class = array(
										'l' => 'two-columns-marker-list__column--first',
										'r' => 'two-columns-marker-list__column--second'
									);
									
	$testimonial_title = carbon_get_the_post_meta('testimonial_title');
	$testimonial_items = carbon_get_the_post_meta('testimonial_items');

	$faq_title = carbon_get_the_post_meta('faq_title');
	$faq_items = carbon_get_the_post_meta('faq_items');
									
?>

	<main class="concierge-page-main-content">
		<div class="wrapper">
			<section class="breadcrumbs-box">
				<?php panda_breadcrumbs(); ?>
			</section>
			<section class="main-header-with-image">
				<div class="main-header-with-image__info-box">
					<?=apply_filters('the_content',$pagetoptext)?>
				</div>
				<div class="main-header-with-image__img-box">
					<?=get_the_post_thumbnail(null,'full')?>
				</div>
			</section>
			<?php if($three_cols_block){ ?>
				<section class="concierge-list-box">
					<ul class="concierge-list">
						<?php foreach($three_cols_block as $item){ ?>
							<li class="concierge-list__item">
								<div class="concierge-list__img-box">
									<?=wp_get_attachment_image($item['img'],'medium')?>
								</div>
								<?=apply_filters('the_content',$item['description'])?>
							</li>
						<?php } ?>
					</ul>
				</section>
			<?php } ?>
			<?php if($compare_block_data && $compare_block_side_class){ ?>
				<section class="consulting-concierge-list-box">
					<div class="two-columns-marker-list">
						<?php foreach($compare_block_side_class as $side => $class){?>
							<div class="two-columns-marker-list__column <?=$class?>">
								<div class="two-columns-marker-list__column-header">
									<div class="two-columns-marker-list__text-box">
										<?php if(!empty($compare_block_data[$side]['top'])){ ?>
											<div class="two-columns-marker-list__subtitle"><?=$compare_block_data[$side]['top']?></div>
										<?php } ?>
										<?php if(!empty($compare_block_data[$side]['title'])){ ?>
											<h3><?=$compare_block_data[$side]['title']?></h3>
										<?php } ?>
									</div>
									<?php if(!empty($compare_block_data[$side]['price'])){ ?>
										<div class="two-columns-marker-list__price"><?=$compare_block_data[$side]['price']?> 
											<?php if(!empty($compare_block_data[$side]['priceperiod'])){ ?>
												<span class="two-columns-marker-list__price-subtitle">/ <?=$compare_block_data[$side]['priceperiod']?></span>
											<?php } ?>
										</div>
									<?php } ?>
								</div>
								<?php if( !empty($compare_block_data[$side]['items']) || !empty($compare_block_data[$side]['okitems']) ){ ?>
									<ul>
										<?php foreach($compare_block_data[$side]['items'] as $item){ ?>
											<li><?=$item['item']?></li>
										<?php } ?>
										<?php foreach($compare_block_data[$side]['okitems'] as $item){ ?>
											<li class="green-marker"><?=$item['item']?></li>
										<?php } ?>
									</ul>
								<?php } ?>
								<div class="pay-buttons-list">
                                  <?php if( !empty($compare_block_data[$side]['paypal'])){ ?>
                                    <?=do_shortcode($compare_block_data[$side]['paypal'])?>
                                  <?php } ?>
                                  <span class="pay-buttons-list__middle-text">or</span>
                                  <?php if( !empty($compare_block_data[$side]['stripe'])){ ?>
                                    <?=do_shortcode($compare_block_data[$side]['stripe'])?>
                                  <?php } ?>
                                </div>
								<?php if(!empty($compare_block_data[$side]['bottom'])){ ?>
									<div class="two-columns-marker-list__footnote">
										<?=$compare_block_data[$side]['bottom']?>
									</div>
								<?php } ?>
							</div>
						<?php } ?>
					</div>
				</section>
			<?php } ?>
			<?php if($testimonial_items){ ?>
				<section class="testimonials-slider-box base-arrows-slider-box">
					<div class="base-arrows-slider-box__header testimonials-slider-box__header">
						<?php if($testimonial_title){ ?>
							<h2><?=$testimonial_title?></h2>
						<?php } ?>
						<div class="testimonials-slider-box__arrows base-arrows-slider-box__arrows">
						<button class="base-arrows-slider-box__prev-arrow">
							<svg width="8" height="16" viewBox="0 0 10 19" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M8.29289 0.909592C8.68342 0.519068 9.31658 0.519068 9.70711 0.909592C10.0976 1.30012 10.0976 1.93328 9.70711 2.32381L8.29289 0.909592ZM1 9.6167L0.292893 10.3238C-0.0976315 9.93328 -0.0976315 9.30012 0.292893 8.90959L1 9.6167ZM9.70711 16.9096C10.0976 17.3001 10.0976 17.9333 9.70711 18.3238C9.31658 18.7143 8.68342 18.7143 8.29289 18.3238L9.70711 16.9096ZM9.70711 2.32381L1.70711 10.3238L0.292893 8.90959L8.29289 0.909592L9.70711 2.32381ZM1.70711 8.90959L9.70711 16.9096L8.29289 18.3238L0.292893 10.3238L1.70711 8.90959Z" fill="#146AFF"/>
							</svg>
						</button>
						<button class="base-arrows-slider-box__next-arrow">
							<svg width="8" height="16" viewBox="0 0 10 19" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M1.70654 1.25473C1.3157 0.864517 0.682536 0.865026 0.292326 1.25586C-0.097885 1.6467 -0.0973765 2.27987 0.293461 2.67008L1.70654 1.25473ZM9 9.94956L9.70654 10.6572C9.89442 10.4697 10 10.2151 10 9.94956C10 9.68407 9.89442 9.42947 9.70654 9.24189L9 9.94956ZM0.293461 17.229C-0.0973765 17.6193 -0.097885 18.2524 0.292326 18.6433C0.682536 19.0341 1.3157 19.0346 1.70654 18.6444L0.293461 17.229ZM0.293461 2.67008L8.29346 10.6572L9.70654 9.24189L1.70654 1.25473L0.293461 2.67008ZM8.29346 9.24189L0.293461 17.229L1.70654 18.6444L9.70654 10.6572L8.29346 9.24189Z" fill="#146AFF"/>
							</svg>
						</button>
						</div>
					</div>
					<div class="swiper-container review-videos-slider" id="reviewVideosSlider">
						<div class="swiper-wrapper">
							<?php foreach($testimonial_items as $item){ ?>
								<div class="swiper-slide">
									<iframe src="<?=$item['video']?>" width="640" height="360" frameborder="0" allow="fullscreen" allowfullscreen></iframe>
								</div>
							<?php } ?>
					</div>
					<div class="base-arrows-slider-box__dots review-videos-slider__dots"></div>
				</section>
			<?php } ?>
			<section class="concierge-ad-box">
			<div class="concierge-ad-box__img-box">
				<img src="<?=get_template_directory_uri()?>/img/concierge-ad-icon.png" alt="booking">
			</div>
			<div class="concierge-ad-box__row">
				<div class="concierge-ad-box__text-box">

				<h2><?=carbon_get_the_post_meta('assistance_title')?></h2>
				<p><?=carbon_get_the_post_meta('assistance_text')?></p>
				</div>
				<div class="concierge-ad-box__price"><?=carbon_get_the_post_meta('price')?></div>
				<div class="concierge-ad-box__btn-box">
				<button onclick="location.href = '<?=carbon_get_the_post_meta('button_link')?>';" class="green-link-btn"><?=carbon_get_the_post_meta('button_title')?></button>
				</div>
			</div>
			</section>
			<?php if($faq_items){ ?>
				<section class="concierge-faq-box">
					<?php if($faq_title){ ?>
						<div class="concierge-faq-box__header">
							<h2><?=$faq_title?></h2>
						</div>
					<?php } ?>
					<ul class="accordion" id="conciergeFAQAccordion">
						<?php foreach($faq_items as $item){ ?>
							<li class="accordion__item">
								<div class="accordion__header">
									<h4><?=$item['question']?></h4>
								</div>
								<div class="accordion__content">
									<?=apply_filters('the_content',$item['answes'])?>
								</div>
							</li>
						<?php } ?>
					</ul>
				</section>
			<?php } ?>
		</div>
	</main>
    
<?php endwhile; ?>

<?php get_footer(); ?>