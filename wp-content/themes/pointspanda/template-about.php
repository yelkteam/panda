<?php 
	// Template Name: About
?>
<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
	<?php 
		$pagetoptext = carbon_get_the_post_meta('pagetoptext');

		$team_title = carbon_get_the_post_meta('teamblock_title');
		$team_items = carbon_get_the_post_meta('teamblock_items');
	?>
	<main class="about-page-main-content">
		<div class="wrapper">
			<section class="breadcrumbs-box">
				<?php panda_breadcrumbs(); ?>
			</section>
			<section class="main-header-with-image main-header-with-image--big-header">
				<div class="main-header-with-image__info-box">
					<?=apply_filters('the_content',$pagetoptext)?>	
				</div>
				<div class="main-header-with-image__img-box">
					<?=get_the_post_thumbnail(null,'full')?>
				</div>
			</section>
			<?php if($team_items){ ?>
				<section class="panda-team-box">
					<?php if($team_title){ ?>
						<div class="panda-team-box__header">
							<h2><?=$team_title?></h2>
						</div>
					<?php } ?>
					<div class="panda-team-box__people-list">
						<?php foreach($team_items as $i => $item){ ?>
							<div class="team-people-card <?=($i%2==0?'team-people-card--revert':'')?>">
								<div class="team-people-card__avatar-box">
									<div class="team-people-card__img-box">
										<?=wp_get_attachment_image($item['photo'],'teacher')?>
									</div>
									<?php if($item['position']){ ?>
										<div class="team-people-card__tag"><?=$item['position']?></div>
									<?php } ?>
								</div>
								<div class="team-people-card__info-box">
									<?=apply_filters('the_content',$item['description'])?>	
								</div>
							</div>
						<?php } ?>
					</div>
				</section>
			<?php } ?>
		</div>
	</main>

<?php endwhile; ?>

<?php get_footer(); ?>