<?php 
	// Template Name: Work with us
?>
<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
	<?php 
		$pagetoptext = carbon_get_the_post_meta('pagetoptext');

		$programs = carbon_get_the_post_meta('program_items');
	?>
	<main class="work-with-us-main-content">
		<div class="wrapper">
			<section class="breadcrumbs-box">
				<?php panda_breadcrumbs(); ?>
			</section>
			<section class="main-header-with-image main-header-with-image--big-header">
				<div class="main-header-with-image__info-box">
					<?=apply_filters('the_content',$pagetoptext)?>	
				</div>
				<div class="main-header-with-image__img-box">
					<?=get_the_post_thumbnail(null,'full')?>
				</div>
			</section>
			<?php if($programs){ ?>
				<section class="affiliate-program-box">
					<ul class="affiliate-program-list">
						<?php foreach ($programs as $item) { ?>
							<li class="affiliate-program-list__item">
								<div class="affiliate-program-list__img-box">
									<?=wp_get_attachment_image($item['img'],'medium')?>
								</div>
								<div class="affiliate-program-list__text-box">
									<?=apply_filters('the_content',$item['description'])?>	
									<?php if($item['btext']){ ?>
										<a href="<?=$item['blink']?>" class="green-link-btn"><?=$item['btext']?></a>
									<?php } ?>
								</div>
							</li>
						<?php } ?>
					</ul>
				</section>
			<?php } ?>
		</div>
	</main>
<?php endwhile; ?>

<?php get_footer(); ?>