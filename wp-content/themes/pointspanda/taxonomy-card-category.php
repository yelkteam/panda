<?php get_header(); ?>
<?php 
	$currentCat = get_queried_object();
	$image = carbon_get_term_meta( $currentCat->term_id, 'thumbnail' );
	$cards_page = carbon_get_theme_option( 'ccpage' );

	$cardfilter_show = carbon_get_theme_option( 'cardfilter_show' );
?>
	<main class="credit-cards-page-main-content">
	  <div class="wrapper">
	    <section class="breadcrumbs-box">
	      <?php panda_breadcrumbs(); ?>
	    </section>
	    <section class="main-header-with-image main-header-with-image--big-header">
	      <div class="main-header-with-image__info-box">
	        <?=$currentCat->description?>	
	      </div>
	      <div class="main-header-with-image__img-box">
	        <?=wp_get_attachment_image($image,'full')?>
	      </div>
	    </section>
		<?php 
		if($cardfilter_show){
	    	$terms = get_terms( [
				'taxonomy' => 'card-category',
				'hide_empty' => false,
			] );
			if($terms){ 
	    ?>
			    <section class="tabs-categories-box">
			      <ul id="tabsCategoriesList" class="tabs-categories-list">
				  		<li class="tabs-categories-list__item">
							<a class="tabs-categories-list__link" href="<?=get_the_permalink($cards_page[0]['id'])?>">
								<span class="tabs-categories-list__img-box">
									<img src="<?=get_template_directory_uri()?>/img/all-categories.svg" alt="all cats" >
								</span>
								<span class="tabs-categories-list__text"><?=__('All','panda')?></span>
							</a>
						</li>
			        <?php foreach ($terms as $term) { ?>
			        	<?php 
			        		$icon = carbon_get_term_meta( $term->term_id, 'cicon' );
			        		$name = carbon_get_term_meta( $term->term_id, 'shortname' );
			        		if(!$name){
			        			$name =$term->name; 
							}
							
							$link = "#";
							if($term->term_id != $currentCat->term_id){
								$link = get_term_link($term->term_id);
							}
			        	?>
							<li class="tabs-categories-list__item <?=($term->term_id == $currentCat->term_id)?'tabs-categories-list__item--active':''?>">
								<?php if($term->term_id != $currentCat->term_id){ ?>
									<a class="tabs-categories-list__link" href="<?=$link?>">
								<?php }else{ ?>
									<span class="tabs-categories-list__link">
								<?php } ?>
									<span class="tabs-categories-list__img-box">
										<?=wp_get_attachment_image($icon,'icon')?>
									</span>
									<span class="tabs-categories-list__text"><?=$name?></span>
								<?php if($term->term_id != $currentCat->term_id){ ?>
									</a>
								<?php }else{ ?>
									</span>
								<?php } ?>
							</li>
			        <?php } ?>
			      </ul>
			    </section>
		<?php } ?>
		<?php } ?>
	    <section class="credit-cards-list-box">
		    <ul class="credit-cards-list">
	    		<?php while ( have_posts() ) : the_post(); ?>

					<?php 
						$clink = carbon_get_the_post_meta('card_url');
					?>
					 <li class="credit-cards-list__item">
			          <h2><?=get_the_title()?></h2>
			          <div class="credit-cards-list__card-box">
			            <div class="credit-cards-list__img-box">
			              <?=get_the_post_thumbnail(null,'big_thumbnail')?>
			            </div>
			          </div>
			         	<?php the_content(); ?>
			         	<?php if($clink){ ?>
			          		<a class="credit-cards-list__read-more-link" href="<?=$clink?>"><?=__('Learn How to Apply','panda')?></a>
			          	<?php } ?>
			        </li>
					<?php endwhile; ?>
				</ul>
		    <?php get_template_part('template-parts/share','vertical'); ?>
	    </section>
	  </div>
	</main>

<?php get_footer(); ?>