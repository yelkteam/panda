<?php get_header(); ?>

	<?php 
		global $wp_query;
	 ?>
	<main class="search-result-page-main-content">
	  <div class="wrapper">
	    <section class="search-result-page-main-content__header">
	    	<?php if(isset($_GET['s'])){ ?>
	      		<h2><?=__('Result Search','panda')?> “<?=$_GET['s']?>”</h2>
	      		<h4><?=$wp_query->found_posts?> <?=__('results found','panda')?></h4>
	      	<?php } ?>
	    </section>
	    <section class="search-result-list-box">
	      <ul class="search-result-list">
	      	<?php 
	      		while ( have_posts() ) : the_post();
	      			if(get_post_type() == 'cards'){
	      				get_template_part( 'template-parts/search','card' ); 
	      			}else{
	      				get_template_part( 'template-parts/search','post' ); 
	      			}
	      		endwhile;
	      	?>	       
	      </ul>
	    </section>
	  </div>
	</main>

<?php get_footer(); ?>