<?php 
	// Template Name: Course
?>
<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<?php
    $pagetoptext = carbon_get_the_post_meta('pagetoptext');

    $abouttext = carbon_get_the_post_meta('aboutblock_text');
    $aboutvideo = carbon_get_the_post_meta('aboutblock_video');
    
    $wdwl_title = carbon_get_the_post_meta('wdwl_title');
    $wdwl_items = carbon_get_the_post_meta('wdwl_items');

    $teachers = carbon_get_the_post_meta('teachers_items');

    $ititle = carbon_get_the_post_meta('includes_title');
    $iitems = carbon_get_the_post_meta('includes_items');

    $cbanner_title = carbon_get_the_post_meta('cbanner_title');
    $cbanner_price = carbon_get_the_post_meta('cbanner_price');
    $cbanner_btext = carbon_get_the_post_meta('cbanner_btext');
    $cbanner_burl = carbon_get_the_post_meta('cbanner_burl');
?>

    <main class="courses-page-main-content">
        <div class="wrapper">
            <section class="breadcrumbs-box">
                <?php panda_breadcrumbs(); ?>
            </section>
            <section class="main-header-with-image main-header-with-image--big-header">
                <div class="main-header-with-image__info-box">
                    <?=apply_filters('the_content',$pagetoptext)?>
                </div>
                <div class="main-header-with-image__img-box">
                    <?=get_the_post_thumbnail(null,'full')?>
                </div>
            </section>
            <section class="course-about-box">
                <div class="course-about-box__text-box">
                    <?=apply_filters('the_content',$abouttext)?>
                </div>
                <div class="course-about-box__video-box">
                    <?php if($aboutvideo){ ?>
                        <iframe src="<?=$aboutvideo?>" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                    <?php } ?>
                </div>
            </section>
            <?php if($wdwl_items){ ?>
                <section class="learn-list-box">
                    <?php if($wdwl_title){ ?>
                        <div class="learn-list-box__header">
                            <h2><?=$wdwl_title?></h2>
                        </div>
                    <?php } ?>
                    <ul class="learn-list">
                        <?php foreach($wdwl_items as $item){ ?>
                            <li class="learn-list__item">
                                <div class="learn-list__img-box">
                                    <?=wp_get_attachment_image($item['img'],'thumbnail')?>
                                </div>
                                <?=apply_filters('the_content', $item['item'])?>
                            </li>
                        <?php } ?>
                    </ul>
                </section>
            <?php } ?>
            <?php if($teachers){ ?>
                <?php foreach($teachers as $teacher){ ?>
                    <section class="people-card-box">
                        <div class="people-card-box__avatar-box">
                            <div class="people-card-box__img-box">
                                <?=wp_get_attachment_image($teacher['img'],'teacher')?>
                            </div>
                            <?php if($teacher['achiev']){ ?>
                                <div class="people-card-box__profession-tag"><?=$teacher['achiev']?></div>
                            <?php } ?>
                        </div>
                        <div class="people-card-box__info-box">
                            <?php if($teacher['position']){ ?>
                                <div class="people-card-box__subtitle"><?=$teacher['position']?></div>
                            <?php } ?>
                            <?php if($teacher['name']){ ?>
                                <h2 class="people-card-box__title"><?=$teacher['name']?></h2>
                            <?php } ?>
                            
                            <p class="people-card-box__description">
                                <?=$teacher['description']?>
                            </p>
                            <ul class="rating-panel">
                                <?php if($teacher['rating']){ ?>
                                    <li class="rating-panel__item">
                                        <div class="rating-panel__mark"><?=$teacher['rating']?></div>
                                        <div class="rating-panel__name"><?=__('teacher rating','panda')?></div>
                                    </li>
                                <?php } ?>
                                <?php if($teacher['reviews']){ ?>
                                    <li class="rating-panel__item">
                                        <div class="rating-panel__mark"><?=$teacher['reviews']?></div>
                                        <div class="rating-panel__name"><?=ngettext(__('review','panda'), __('reviews','panda'), $teacher['reviews'])?></div>
                                    </li>
                                <?php } ?>
                                <?php if($teacher['students']){ ?>
                                    <li class="rating-panel__item">
                                        <div class="rating-panel__mark"><?=$teacher['students']?></div>
                                        <div class="rating-panel__name"><?=ngettext(__('student','panda'), __('students','panda'), $teacher['students'])?></div>
                                    </li>
                                <?php } ?>
                                <?php if($teacher['courses']){ ?>
                                    <li class="rating-panel__item">
                                        <div class="rating-panel__mark"><?=$teacher['courses']?></div>
                                        <div class="rating-panel__name"><?=ngettext(__('course','panda'), __('courses','panda'), $teacher['courses'])?></div>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </section>
                <?php } ?>
            <?php } ?>
            <?php if($iitems){ ?>
                <section class="course-includes-box">
                    <?php if($ititle){ ?>
                        <div class="course-includes-box__header">
                            <h2><?=$ititle?></h2>
                        </div>
                    <?php } ?>
                    <ul class="course-includes-list">
                        <?php foreach($iitems as $item){ ?>
                            <li class="course-includes-list__item">
                                <div class="course-includes-list__img-box">
                                    <?=wp_get_attachment_image($item['img'],'big_icon')?>
                                </div>
                                <p class="course-includes-list__title"><?=$item['item']?></p>
                            </li>
                        <?php } ?>
                    </ul>
                </section>
            <?php } ?>
            <section class="course-ad-box">
                <div class="course-ad-box__text-box">
                    <?php if($cbanner_title){ ?>
                        <h2><?=$cbanner_title?></h2>
                    <?php } ?>
                    <?php if($cbanner_price){ ?>
                        <div class="course-ad-box__price"><?=$cbanner_price?></div>
                    <?php } ?>
                </div>
                <?php if($cbanner_btext){ ?>
                    <div class="course-ad-box__btn-box">
                        <a href="<?=$cbanner_burl?>" class="course-ad-box__btn green-link-btn"><?=$cbanner_btext?></a>
                    </div>
                <?php } ?>
            </section>
        </div>
    </main>

<?php endwhile; ?>

<?php get_footer(); ?>