<?php
	// Template Name: Frontpage
?>
<?php get_header(); ?>

<main class="main-content">
    <div class="wrapper">

<?php while ( have_posts() ) : the_post(); ?>

	<?php
		$topdiscl = carbon_get_theme_option('topdiscl');
		$cards_page = ''; // carbon_get_theme_option( 'ccpage' );

		$placeholder = carbon_get_theme_option( 'defimage' );

		if($topdiscl){
	?>
		<div class="ad-top-box">
			<div class="ad-top-box__icon"></div>
			<?=apply_filters('the_content',$topdiscl)?>
		</div>
	<?php } ?>
	<?php
		$category = carbon_get_the_post_meta('popular_posts');
		$popular = get_most_popular_posts($category, 3);
	?>
        <section class="last-news-box">
	        <div class="popular-news-slider">
				<?php if($popular){ ?>
					<ul id="popularNewsSlider" class="slider">
						<?php foreach($popular as $ppost){ ?>
							<li class="slide slider__item">
								<a href="<?=get_the_permalink($ppost->ID)?>">
									<div class="slider__img-box">
										<?php
											if(has_post_thumbnail($ppost->ID)){
												echo get_the_post_thumbnail($ppost->ID,'large');
											}else{
												echo wp_get_attachment_image($placeholder,'large');
											}
										?>
									</div>
									<?php

										$posted = human_time_diff(get_the_time('U',$ppost));
									?>
									<div class="popular-news-slider__tag"><?=__('Trending','panda')?></div>
									<h3 class="popular-news-slider__title"><?=$ppost->post_title?></h3>
									<div class="popular-news-slider__date"><?=get_the_date('F d, Y',$ppost->ID)?><!--<?=__('News','panda')?> <?=$posted?>--></div>
								</a>
							</li>
						<?php } ?>
					</ul>
					<div id="sliderControls" class="popular-news-slider__controls"></div>
				<?php } ?>
	        </div>
          <div class="side-last-news-list">
			  <?php
			  	$category = carbon_get_the_post_meta('top_last_posts');
				$latest_posts = get_latest_posts($category, 10);
				if($latest_posts){
			  ?>
				<ul>
					<?php foreach($latest_posts as $lpost){ ?>
						<li class="side-last-news-list__item">
							<div class="side-last-news-list__img-box">
								<?php
									if(has_post_thumbnail($lpost->ID)){
										echo get_the_post_thumbnail($lpost->ID,'icon');
									}else{
										echo wp_get_attachment_image($placeholder,'icon');
									}
								?>
							</div>
							<a href="<?=get_the_permalink($lpost->ID)?>"><?=$lpost->post_title?></a>
						</li>
					<?php } ?>
				</ul>
			<?php } ?>
          </div>
		</section>
		<?php
			$ccitems = carbon_get_theme_option( 'top_ccdeals' );
			if($ccitems){
				$cctitle = carbon_get_theme_option( 'top_ccdealstitle' );
		?>
				<section class="credit-card-box">
					<div id="creditCardsSlider" class="credit-card-box__header">
						<?php if($cctitle){ ?>
						<h2><?=$cctitle?></h2>
						<?php } ?>
						<div class="credit-cards-slider">
							<?php
								foreach($ccitems as $i => $ccitem){
									$clink = carbon_get_post_meta($ccitem['id'], 'card_url');
									if ( empty($clink) ) {
										$clink = '#';
									}
									?>
									<div class="slide credit-cards-slider__item credit-card-box__item">
										<a class="credit-card-box__card-link" href="<?=$clink?>" rel="noopener noreferrer">
											<div class="credit-card-box__img-box">
												<?php
													if(has_post_thumbnail($ccitem['id'])){
														echo get_the_post_thumbnail($ccitem['id'],'small_card');
													}else{
														echo wp_get_attachment_image($placeholder,'small_card');
													}
												?>
											</div>
											<div class="credit-card-box__card-info">
												<?php if($i == 0){ ?>
													<div class="credit-card-box__subtitle"><?=__('Best Total Value','panda')?></div>
												<?php } ?>
												<div class="credit-card-box__title"><?=get_the_title($ccitem['id'])?></div>
												<div class="credit-card-box__info">
												<?php
													$current = carbon_get_post_meta($ccitem['id'],'current_offer');
													if($current){
														?>
														<span class="credit-card-box__mark-text"><?=__('Current offer:','panda')?></span> <?=$current?>
														<?php
													}else{
														$in_bonus = carbon_get_post_meta($ccitem['id'],'text_offer');
														echo $in_bonus;
													}
												?>
												</div>
											</div>
										</a>
									</div>
							<?php } ?>
							<div id="creditCardsSliderControls" class="credit-cards-slider__controls"></div>
							</div>
							<?php if(isset($cards_page[0]['id'])){ ?>
							<a class="credit-card-box__link--transparent" href="<?=get_the_permalink($cards_page[0]['id'])?>">
								<span><?=__('See all','panda')?></span>
								<svg width="42" height="10" viewBox="0 0 42 10" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M1 4.4C0.668629 4.4 0.4 4.66863 0.4 5C0.4 5.33137 0.668629 5.6 1 5.6V4.4ZM41.4243 5.42426C41.6586 5.18995 41.6586 4.81005 41.4243 4.57574L37.6059 0.757359C37.3716 0.523045 36.9917 0.523045 36.7574 0.757359C36.523 0.991674 36.523 1.37157 36.7574 1.60589L40.1515 5L36.7574 8.39411C36.523 8.62843 36.523 9.00833 36.7574 9.24264C36.9917 9.47696 37.3716 9.47696 37.6059 9.24264L41.4243 5.42426ZM1 5.6H41V4.4H1V5.6Z" fill="#146AFF"/>
								</svg>
							</a>
							<?php } ?>
					</div>
					<div class="credit-card-box__row">
						<div class="credit-card-box__list">
							<?php
								foreach($ccitems as $i => $ccitem){
									$clink = carbon_get_post_meta($ccitem['id'], 'card_url');
									if ( empty($clink) ) {
										$clink = '#';
									}
								?>
								<div class="credit-card-box__item">
									<a class="credit-card-box__card-link" href="<?=$clink?>" rel="noopener noreferrer">
										<div class="credit-card-box__img-box">
											<?php
													if(has_post_thumbnail($ccitem['id'])){
														echo get_the_post_thumbnail($ccitem['id'],'small_card');
													}else{
														echo wp_get_attachment_image($placeholder,'small_card');
													}
												?>
										</div>
										<div class="credit-card-box__card-info">
											<?php if($i == 0){ ?>
													<div class="credit-card-box__subtitle"><?=__('Best Total Value','panda')?></div>
											<?php } ?>
											<div class="credit-card-box__title"><?=get_the_title($ccitem['id'])?></div>
											<div class="credit-card-box__info">
													<?php
														$current = carbon_get_post_meta($ccitem['id'],'current_offer');
														if($current){
															?>
															<span class="credit-card-box__mark-text"><?=__('Current offer:','panda')?></span> <?=$current?>
															<?php
														}else{
															$in_bonus = carbon_get_post_meta($ccitem['id'],'text_offer');
															echo $in_bonus;
														}
													?>
												</div>
										</div>
									</a>
								</div>
							<?php } ?>
						</div>
						<?php if(isset($cards_page[0]['id'])){ ?>
							<a class="credit-card-box__link" href="<?=get_the_permalink($cards_page[0]['id'])?>">
								<span><?=__('See all','panda')?></span>
								<svg width="42" height="10" viewBox="0 0 42 10" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M1 4.4C0.668629 4.4 0.4 4.66863 0.4 5C0.4 5.33137 0.668629 5.6 1 5.6V4.4ZM41.4243 5.42426C41.6586 5.18995 41.6586 4.81005 41.4243 4.57574L37.6059 0.757359C37.3716 0.523045 36.9917 0.523045 36.7574 0.757359C36.523 0.991674 36.523 1.37157 36.7574 1.60589L40.1515 5L36.7574 8.39411C36.523 8.62843 36.523 9.00833 36.7574 9.24264C36.9917 9.47696 37.3716 9.47696 37.6059 9.24264L41.4243 5.42426ZM1 5.6H41V4.4H1V5.6Z" fill="#146AFF"/>
								</svg>
							</a>
						<?php } ?>
					</div>
				</section>
		<?php } ?>
		<?php
		  	$mailchimp_description = carbon_get_the_post_meta('mailchimp_description');
		?>
		<section class="blog-box">
			<section class="home-page-subscribe-form-box">
				<div class="subscribe-form-box">
					<div class="subscribe-form-box__wrapper">
						<div class="subscribe-form-box__info">
							<?=$mailchimp_description;?>
						</div>
						<?php get_template_part('template-parts/subscribe-form'); ?>
          			</div>
        		</div>
			</section>
			<?php
				$popular_cat = carbon_get_the_post_meta( 'popular_cat' );
				if($popular_cat):
			?>
			<section class="home-page-categories-list-box">
				<ul class="home-page-categories-list">
					<?php foreach($popular_cat as $i => $popular_cat_item): ?>
					<li class="home-page-categories-list__item">
						<a href="<?=$popular_cat_item['cat_link'];?>">
							<div class="home-page-categories-list__img-box">
								<?php echo wp_get_attachment_image($popular_cat_item['cat_image'],'full');?>
							</div>
							<h5><?=$popular_cat_item['cat_title'];?></h5>
						</a>
					</li>
					<?php endforeach;?>
				</ul>
			</section>
			<?php endif;?>
			<?php
				$youtube_description = carbon_get_the_post_meta('youtube_description');
			  	$youtube_script = carbon_get_the_post_meta('youtube_script');
			?>
			<section class="home-page-youtube-box youtube-box">
				<div class="youtube-box__subscribe-box">
					<div class="youtube-box__icon-box">
						<img src="<?php echo get_stylesheet_directory_uri();?>/img/icon-youtube.png" alt="">
					</div>
					<div class="youtube-box__text-box">
						<?=$youtube_description;?>
					</div>
				</div>
				<div class="youtube-box__video-box">
					<?=do_shortcode($youtube_script);?>
				</div>
			</section>
			<?php
				$title = carbon_get_the_post_meta('bottomblog_title');
				$category = carbon_get_the_post_meta('bottom1_row');
				$posts = get_latest_posts($category,3);
			?>
			<div class="blog-cards-box">
				<?php if($title){ ?>
					  <h2><?=$title?></h2>
				<?php } ?>
				<div class="blog-cards">
					<?php
						foreach($posts as $post):
							setup_postdata($post);
					?>
					<div class="blog-card">
						<a class="blog-card__link" href="<?=get_the_permalink()?>">
							<div class="blog-card__img-box">
								<?php
									if(has_post_thumbnail()){
										echo get_the_post_thumbnail(null,'thumbnail');
									}else{
										echo wp_get_attachment_image($placeholder,'thumbnail');
									}
								?>
								<?php
									$primary_cat_id = get_post_meta( $post->ID, 'rank_math_primary_category', true );

									if ( $primary_cat_id ) {
									    $product_cat = get_term( $primary_cat_id, 'category' );
										?>
										<object>
											<a href="<?=get_category_link($product_cat->term_id)?>" class="blog-card__img-tag"><?=$product_cat->name;?></a>
										</object>
										<?php
									}
								?>
							</div>
							<div class="blog-card__info">
								<h4 class="blog-card__title"><?=get_the_title()?></h4>
								<p class="blog-card__content">
									<?php echo get_the_excerpt(); ?>
								</p>
								<div class="blog-card__date"><?=get_the_date('F d, Y')?></div>
							</div>
						</a>
					</div>
					<?php
						endforeach;
						wp_reset_postdata();
					?>
				</div>
			</div>
			<div class="not-description-blog-cards">
				<?php
					$category = carbon_get_the_post_meta('bottom3_row');
					$posts = get_latest_posts($category,2);
					if($posts):
				?>
				<?php
					foreach($posts as $post):
						setup_postdata($post);
				?>
				<div class="not-description-card">
					<a href="<?=get_the_permalink()?>">
						<div class="not-description-card__img-box">
							<?php
								if(has_post_thumbnail()){
									echo get_the_post_thumbnail(null,'big_thumbnail');
								}else{
									echo wp_get_attachment_image($placeholder,'big_thumbnail');
								}
							?>
							<?php
								$primary_cat_id = get_post_meta( $post->ID, 'rank_math_primary_category', true );

								if ( $primary_cat_id ) {
								    $product_cat = get_term( $primary_cat_id, 'category' );
									?>
									<object>
										<a href="<?=get_category_link($product_cat->term_id)?>" class="blog-card__img-tag"><?=$product_cat->name;?></a>
									</object>
									<?php
								}
							?>
						</div>
						<h3 class="not-description-card__title"><?=get_the_title()?></h3>
						<div class="not-description-card__date"><?=get_the_date('F d, Y')?></div>
					</a>
				</div>
				<?php endforeach;?>
				<?php endif;
				wp_reset_postdata();
				?>
			</div>
			<div class="view-all-link-box">
				<a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>">
					<span class="view-all-link-box__arrow"></span>
					<span class="view-all-link-box__text"><?=__('view all articles','panda')?></span>
				</a>
			</div>
		</section>
        <?php
        /*
	    <section class="instagram-box">
	      <?php $inst = carbon_get_the_post_meta('instagram_title');
	      echo $inst;
	      ?>
	      <?=do_shortcode('[instagram-feed]');?>
	    </section>
        */
        ?>
<?php endwhile; ?>

	</div>
</main>
<?php get_footer(); ?>
