<?php get_header(); ?>

<?php
	$content = carbon_get_theme_option( 'errcontent' );
	$image = carbon_get_theme_option( 'errimage' );
?>

<main class="page-404-main-content">
  <div class="wrapper">
    <section class="content-404">
      <div class="content-404__info-box">
        <?=apply_filters('the_content',$content)?>
        <a href="<?php echo home_url( '/' ) ?>" class="green-link-btn"><?=__('go to home','panda')?></a>
      </div>
      <div class="content-404__img-box">
        <?=wp_get_attachment_image($image,'full')?>
      </div>
    </section>
  </div>
</main>

<?php get_footer(); ?>