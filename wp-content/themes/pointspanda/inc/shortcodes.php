<?php 

/*
	Shortcodes
*/

function mark_text_func( $atts, $content ){
	 return '<span class="mark">'.$content.'</span>';
}
add_shortcode('mark_text', 'mark_text_func');

/*
	Add buttons to panel
*/
/*
function appthemes_add_quicktags() {
	if ( ! wp_script_is('quicktags') )
		return;

	?>
	<script type="text/javascript">
		QTags.addButton( 'bluemark', 'mark_text', '[mark_text]', '[/mark_text]', 'x', 'Mark text' );
	</script>
	<?php
}
add_action( 'admin_print_footer_scripts', 'appthemes_add_quicktags' );
*/

function panda_mce_buttons_2( $buttons ) {	
	/**
	 * Add in a core button that's disabled by default
	 */

    array_push( $buttons, '', 'mark_text_btn' );

	return $buttons;
}
add_filter( 'mce_buttons_2', 'panda_mce_buttons_2' );

add_filter('mce_external_plugins', 'panda_tinymce_plugin');
function panda_tinymce_plugin() {
    $plugins_array = array(
                         'mark_text_btn' => get_template_directory_uri() .'/js/editor_plugin.js'
                     );
    return $plugins_array;
}