<?php
add_action('init', 'panda_post_types', 0);
function panda_post_types()
{
    register_post_type('cards', array(
        'public' => true,
        'supports' => array('title', 'thumbnail', 'editor', 'excerpt'),
        'menu_position' => null,
        'menu_icon' => 'dashicons-editor-table',
        'labels' => array(
            'name'                     => __('Cards','panda'),
            'singular_name'            => __('Credit Cards','panda'),
            'add_new'                  => _x('Add New', 'card'),
            'add_new_item'             => __('Add new Card','panda'),
            'edit_item'                => __('Edit Card','panda'),
            'new_item'                 => __('New Card','panda'),
            'view_item'                => __('View Card','panda'),
            'search_items'             => __('Search Cards','panda'),
            'view_items'               => __('View Cards','panda'),
            'attributes'               => __('Cards Attributes','panda'),
        ),
        'show_in_nav_menus'  => false,
        'publicly_queryable'    => false,
        'exclude_from_search'   => true,
        'query_var'             => false,
        'hierarchical' => false,
        'rewrite' => array('slug' => 'cards', 'with_front' => false),
    ));

    register_taxonomy('card-category', array('cards'), array(
        'hierarchical'  => true,
        'labels'        => array(
            'name'              => _x( 'Card Categories', 'taxonomy general name' ),
            'singular_name'     => _x( 'Card Category', 'taxonomy singular name' ),
            'search_items'      =>  __( 'Search Card Category' ),
            'all_items'         => __( 'All Card Categories' ),
            'edit_item'         => __( 'Edit Category' ),
            'update_item'       => __( 'Update Category' ),
            'add_new_item'      => __( 'Add New Category' ),
            'new_item_name'     => __( 'New Category Name' ),
            'menu_name'         => __( 'Categories' ),
        ),
        'show_ui'       => true,
        'show_in_nav_menus'     => false,
        'show_tagcloud'         => false,
        'publicly_queryable'    => false,
        'query_var'             => false,
        'show_admin_column' => true,
        'hierarchical'          => true,

    ));
}
