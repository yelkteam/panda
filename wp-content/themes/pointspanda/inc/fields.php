<?php
use Carbon_Fields\Container;
use Carbon_Fields\Block;
use Carbon_Fields\Field;

add_action( 'carbon_fields_register_fields', 'panda_theme_options' );
function panda_theme_options()
{
    Container::make( 'theme_options', __( 'Theme Options', 'panda' ) )
        ->add_tab( __('Top disclosure','panda'), array(
            Field::make( 'rich_text', 'topdiscl',__(  'Top disclosure', 'panda' )  ),
        ) )
        ->add_tab( __('Google Analytics','panda'), array(
            Field::make( 'textarea', 'ga',__(  'Snippet', 'panda' )  ),
        ) )
        ->add_tab( __('Footer','panda'), array(
            Field::make( 'rich_text', 'footercontent', __( 'Footer text', 'panda' )  ),
            Field::make( 'text', 'th_copyright', __( 'Copyright', 'panda' )  ),
            Field::make( 'association', 'footer_termbox', __( 'Footer Politics pages', 'panda' )  )
                    ->set_types( array(
                        array(
                            'type' => 'post',
                            'post_type' => 'page',
                        ),
                    ) )->set_max(2),
            Field::make( 'association', 'footer_last', __( 'Footer Posts Category', 'panda' )  )
                    ->set_types( array(
                        array(
                            'type' => 'term',
                            'taxonomy' => 'category',
                        ),
                    ) )
                ->help_text(__( 'When the field is blank posts select from all categories', 'panda' ) ),
        ) )
        ->add_tab( __('Contacts','panda'), array(
            Field::make( 'text', 'pphone', __( 'Phone', 'panda' )  ),
            Field::make( 'text', 'pemail', __( 'Email', 'panda' )  ),
        ) )
        ->add_tab( __('Social','panda'), array(
            Field::make( 'text', 'pyoutube', __( 'Youtube', 'panda' )  ),
            Field::make( 'text', 'pinstagram', __( 'Instagram', 'panda' )  ),
            Field::make( 'text', 'ptwitter', __( 'Twitter', 'panda' )  ),
            Field::make( 'text', 'pfacebook', __( 'Facebook', 'panda' )  ),
            Field::make( 'text', 'plinkedin', __( 'Linkedin', 'panda' )  ),
        ) )
        ->add_tab( __('Share Text','panda'), array(
            Field::make( 'text', 'share_text',__(  'Share Text', 'panda' )  ),
        ) )
        ->add_tab( __('Global blocks','panda'), array(

            Field::make( 'image', 'defimage', __( 'Default Image', 'panda' )  )->set_required(1),

            Field::make( 'separator', 'ccpage_sep',__(  'Main Credit cards page', 'panda' )  ),
            Field::make( 'association', 'ccpage', __( 'Select page', 'panda' )  )
                    ->set_types( array(
                        array(
                            'type' => 'post',
                            'post_type' => 'page',
                        ),
                    ) )->set_max(1)->set_required(1),

            Field::make( 'separator', 'ccdeals_sep',__(  'Top Credit card deals', 'panda' )  ),
            Field::make( 'text', 'top_ccdealstitle',__(  'Title', 'panda' )  ),
            Field::make( 'association', 'top_ccdeals', __( 'Top Credit card deals', 'panda' )  )
                    ->set_types( array(
                        array(
                            'type' => 'post',
                            'post_type' => 'cards',
                        ),
                    ) )->set_max(4),

            Field::make( 'separator', 'sidebar_sep',__(  'Sidebar block', 'panda' )  ),
            Field::make( 'image', 'sbanner_img', __( 'Image PC', 'panda' )  )->set_width(50),
            Field::make( 'image', 'sbanner_img1', __( 'Image Tablet', 'panda' )  )->set_width(50),
            Field::make( 'text', 'sbanner_url', __( 'Url', 'panda' )  )->set_default_value('#'),
            /*
            Field::make( 'separator', 'discl_sep',__(  'Disclosure text', 'panda' )  ),
            Field::make( 'rich_text', 'disclosure1', __( 'First row', 'panda' )  ),
            Field::make( 'rich_text', 'disclosure2', __( 'Second row', 'panda' )  ),*/

            Field::make( 'separator', 'discl_post_sep',__(  'Blog Post Disclosure text', 'panda' )  ),
            Field::make( 'rich_text', 'disclosure_post', __( 'Content', 'panda' )  ),

            Field::make( 'separator', 'cardslider_sep',__(  'Single cards page Interesting block', 'panda' )  ),
            Field::make( 'checkbox', 'cardslider_show', __('Show block', 'panda') ),

            Field::make( 'separator', 'cardfilter_sep',__(  'Cards filter block', 'panda' )  ),
            Field::make( 'checkbox', 'cardfilter_show', __('Show block', 'panda') ),

            Field::make( 'separator', 'after_auth_text_sep',__(  'Blog disclosure before comments', 'panda' )  ),
            Field::make( 'rich_text', 'after_auth_text', __( 'Content', 'panda' )  ),

        ) )
        ->add_tab( __('Cards Rating','panda'), array(
            Field::make( 'text', 'cardtext5',__(  'Text Rating 5', 'panda' )  ),
            Field::make( 'text', 'cardtext4',__(  'Text Rating 4', 'panda' )  ),
            Field::make( 'text', 'cardtext3',__(  'Text Rating 3', 'panda' )  ),
            Field::make( 'text', 'cardtext2',__(  'Text Rating 2', 'panda' )  ),
            Field::make( 'text', 'cardtext1',__(  'Text Rating 1', 'panda' )  ),
        ) )
        ->add_tab( __('Post Card Block','panda'), array(
            Field::make( 'separator', 'post_card_cardblock_sep',__(  'Card Block', 'panda' )  ),
            Field::make( 'text', 'post_card_cardblock_title',__(  'Title', 'panda' )  ),
            Field::make( 'rich_text', 'post_card_cardblock_stitle',__(  'Description Offer', 'panda' )  ),
            Field::make( 'image', 'post_card_cardblock_image',__(  'Image', 'panda' )  )->set_width(50),
            Field::make( 'text', 'post_card_cardblock_iurl', __( 'Image url', 'panda' )  )->set_width(50)->set_default_value('#'),
            Field::make( 'text', 'post_card_cardblock_btext',__(  'Button text', 'panda' )  )->set_width(50),
            Field::make( 'text', 'post_card_cardblock_burl', __( 'Button url', 'panda' )  )->set_width(50)->set_default_value('#'),
        ) )
        ->add_tab( __('Subscription Form text fields','panda'), array(
            /*Field::make( 'rich_text', 'mailchimp_description_homepage',__(  'For Homepage', 'panda' )  ),*/
            Field::make( 'rich_text', 'mailchimp_description_sidebar',__(  'For Sidebar', 'panda' )  ),
            Field::make( 'rich_text', 'mailchimp_description_popup',__(  'For Popup', 'panda' )  ),
        ) )
        ->add_tab( __('404 page','panda'), array(
            Field::make( 'rich_text', 'errcontent', __( 'Page content', 'panda' )  ),
            Field::make( 'image', 'errimage', __( 'Image', 'panda' )  ),
        ) );
}

add_action( 'carbon_fields_register_fields', 'panda_meta_fields' );
function panda_meta_fields()
{
    Container::make( 'term_meta', __( 'Term Options', 'panda' ) )
        ->where( 'term_taxonomy', '=', 'card-category' )
        ->add_fields( array(
            Field::make( 'image', 'thumbnail',__(  'Category Thumbnail', 'panda' )  ),
            Field::make( 'image', 'cicon',__(  'Category icon', 'panda' )  )->set_required(1),
            Field::make( 'text', 'shortname',__(  'Category Shortname', 'panda' )  ),
        ) );

    Container::make( 'post_meta', __('Page Top','panda') )
        ->show_on_post_type(array('page'))
        ->hide_on_template(array('template-frontpage.php', 'template-wtf.php'))
        ->add_fields(array(
            Field::make( 'rich_text', 'pagetoptext',__(  'Top Text', 'panda' )  ),
        ));

    Container::make( 'post_meta', __('Card info','panda') )
        ->show_on_post_type(array('cards'))
        ->add_fields(array(
            Field::make( 'separator', 'main_block',__(  'Main Card Information', 'panda' )  ),
            Field::make( 'text', 'current_offer',__(  'Current Offer', 'panda' )  ),
            Field::make( 'text', 'text_offer',__(  'Card subtitle', 'panda' )  ),

            Field::make( 'checkbox', 'infoblock_show', __('Show Card Info', 'panda') ),

            Field::make( 'complex', 'card_options', __(  'Card Options', 'panda' ) )
                    ->add_fields( array(
                        Field::make( 'text', 'title',__(  'Title', 'panda' )  )->set_required(1)->set_width(50),
                        Field::make( 'text', 'content',__(  'Value', 'panda' )  )->set_required(1)->set_width(50),
                    ) ),

            Field::make( 'text', 'card_url',__(  'Card URL', 'panda' )  ),

            Field::make( 'separator', 'recommended_block',__(  'Credit Recommended information', 'panda' )  ),
            Field::make( 'select', 'recommend_rating',__(  'Choose rating', 'panda' )  )->set_options( array(
                                                                                                                '1' => 1,
                                                                                                                '2' => 2,
                                                                                                                '3' => 3,
                                                                                                                '4' => 4,
                                                                                                                '5' => 5,
                                                                                                            ) )->set_default_value('5')->set_width(33)
                                                                                                            ->help_text( __('You can change the icon and inscription for the rating on the <a href="/wp-admin/admin.php?page=crb_carbon_fields_container_theme_options.php" target="_blank">theme settings page<a>','panda') ),

            Field::make( 'text', 'recommend_stitle',__(  'Credit recommended', 'panda' )  )->set_width(66),
        ));

    Container::make( 'post_meta', __('Card info','panda') )
        ->show_on_post_type(array('post'))->where( 'post_term', '=', array(
        'field' => 'slug',
        'value' => 'reviews',
        'taxonomy' => 'category',
    ) )
        ->add_fields(array(
            Field::make( 'separator', 'main_block',__(  'Main Card Information', 'panda' )  ),
            Field::make( 'text', 'current_offer',__(  'Current Offer', 'panda' )  ),
            Field::make( 'checkbox', 'infoblock_show', __('Show Card Info', 'panda') ),
            Field::make( 'complex', 'card_options', __(  'Card Options', 'panda' ) )
                ->add_fields( array(
                    Field::make( 'text', 'title',__(  'Title', 'panda' )  )->set_required(1)->set_width(50),
                    Field::make( 'text', 'content',__(  'Value', 'panda' )  )->set_required(1)->set_width(50),
                ) ),
            Field::make( 'text', 'card_url',__(  'Card URL', 'panda' )  ),
            Field::make( 'separator', 'recommended_block',__(  'Credit Recommended information', 'panda' )  ),
            Field::make( 'select', 'recommend_rating',__(  'Choose rating', 'panda' )  )
                ->set_options( array(
                    '1' => 1,
                    '2' => 2,
                    '3' => 3,
                    '4' => 4,
                    '5' => 5,
                ) )->set_default_value('5')->set_width(33)
                ->help_text( __('You can change the icon and inscription for the rating on the <a href="/wp-admin/admin.php?page=crb_carbon_fields_container_theme_options.php" target="_blank">theme settings page<a>','panda') ),
            Field::make( 'text', 'recommend_stitle',__(  'Bottom rating text', 'panda' )  )->set_width(66),
        ));

    Container::make( 'post_meta', __('Page Blocks','panda') )
        ->show_on_post_type(array('page'))
        ->show_on_template(array('template-frontpage.php'))
        ->add_fields(array(
/*
            Field::make( 'text', 'ccdealstitle',__(  'Credit card deals Title', 'panda' )  ),
            Field::make( 'association', 'ccdeals', __( 'Credit card deals', 'panda' )  )
                    ->set_types( array(
                        array(
                            'type' => 'post',
                            'post_type' => 'cards',
                        ),
                    ) )->set_max(4),
*/
            Field::make( 'rich_text', 'mailchimp_description',__(  'Mailchimp description', 'panda' )  ),
            //Field::make( 'image', 'mailchimp_image',__(  'Mailchimp image', 'panda' )  ),
            Field::make( 'complex', 'popular_cat', __(  'Popular categories', 'panda' ) )
                ->add_fields( array(
                    Field::make( 'text', 'cat_title',__(  'Category title', 'panda' )  )->set_required(1)->set_width(50),
                    Field::make( 'text', 'cat_link',__(  'Category link', 'panda' )  )->set_required(1)->set_width(50),
                    Field::make( 'image', 'cat_image',__(  'Category image', 'panda' )  )->set_required(1)->set_width(50),
                ) ),
            Field::make( 'rich_text', 'youtube_description',__(  'Youtube description', 'panda' )  ),
            Field::make( 'rich_text', 'youtube_script',__(  'Youtube script', 'panda' )  ),
            Field::make( 'rich_text', 'instagram_title',__(  'Instagram title', 'panda' )  ),

            Field::make( 'association', 'popular_posts', __( 'Popular blog Posts Category', 'panda' )  )
                    ->set_types( array(
                        array(
                            'type' => 'term',
                            'taxonomy' => 'category',
                        ),
                    ) )
                ->help_text(__( 'When the field is blank posts select from all categories', 'panda' ) ),
            Field::make( 'association', 'top_last_posts', __( 'Top Posts Block', 'panda' )  )
                ->set_types( array(
                    array(
                        'type' => 'term',
                        'taxonomy' => 'category',
                    ),
                ) )
            ->help_text(__( 'When the field is blank posts select from all categories', 'panda' ) ),

            Field::make( 'separator', 'freecons_sep',__(  'Free consultation block', 'panda' )  ),
            Field::make( 'text', 'freecons_title',__(  'Title', 'panda' )  ),
            Field::make( 'rich_text', 'freecons_stitle',__(  'Description Offer', 'panda' )  ),
            Field::make( 'image', 'freecons_image',__(  'Image', 'panda' )  ),
            Field::make( 'text', 'freecons_btext',__(  'Button text', 'panda' )  )->set_width(50),
            Field::make( 'text', 'freecons_burl', __( 'Button url', 'panda' )  )->set_width(50)->set_default_value('#'),

            Field::make( 'separator', 'bottomblog_sep',__(  'Bottom Posts rows', 'panda' )  ),
            Field::make( 'text', 'bottomblog_title',__(  'Title', 'panda' )  ),
            Field::make( 'association', 'bottom1_row', __( 'First Posts row Category', 'panda' )  )
                    ->set_types( array(
                        array(
                            'type' => 'term',
                            'taxonomy' => 'category',
                        ),
                    ) )
                    ->help_text(__( 'When the field is blank posts select from all categories', 'panda' ) ),
            Field::make( 'association', 'bottom2_row', __( 'Second Posts row Category', 'panda' )  )
                ->set_types( array(
                    array(
                        'type' => 'term',
                        'taxonomy' => 'category',
                    ),
                ) )
                ->help_text(__( 'When the field is blank posts select from all categories', 'panda' ) ),
            Field::make( 'association', 'bottom3_row', __( 'Third Posts row Category', 'panda' )  )
                    ->set_types( array(
                        array(
                            'type' => 'term',
                            'taxonomy' => 'category',
                        ),
                    ) )
                    ->help_text(__( 'When the field is blank posts select from all categories', 'panda' ) ),
        ));

    Container::make( 'post_meta', __('Page Blocks','panda') )
        ->show_on_post_type(array('page'))
        ->where( 'post_id', '=', get_option( 'page_for_posts' ) )
        ->add_fields(array(
            Field::make( 'rich_text', 'blog_quote',__(  'Quote', 'panda' )  ),
            Field::make( 'text', 'blog_subscr_title',__(  'Subscribe title', 'panda' )  ),
            Field::make( 'text', 'blog_subscr_subtitle',__(  'Subscribe subtitle', 'panda' )  ),
            Field::make( 'image', 'blog_subscr_image',__(  'Subscribe image', 'panda' )  ),

            Field::make( 'association', 'top_last_posts', __( 'Top Posts Block', 'panda' )  )
            ->set_types( array(
                array(
                    'type' => 'term',
                    'taxonomy' => 'category',
                ),
            ) )
            ->help_text(__( 'When the field is blank posts select from all categories', 'panda' ) ),

            Field::make( 'association', 'popular_posts', __( 'Popular blog Posts Category', 'panda' )  )
                    ->set_types( array(
                        array(
                            'type' => 'term',
                            'taxonomy' => 'category',
                        ),
                    ) )
                ->help_text(__( 'When the field is blank posts select from all categories', 'panda' ) ),

        ));

    Container::make( 'post_meta', __('Post blocks','panda') )
        ->show_on_post_type(array('post'))
        ->add_fields(array(

            Field::make( 'separator', 'cardblock_sep',__(  'Card Block', 'panda' )  ),
            Field::make( 'text', 'cardblock_title',__(  'Title', 'panda' )  ),
            Field::make( 'rich_text', 'cardblock_stitle',__(  'Description Offer', 'panda' )  ),
            Field::make( 'image', 'post_card_cardblock_image',__(  'Image', 'panda' )  )->set_width(50),
            Field::make( 'text', 'post_card_cardblock_iurl', __( 'Image url', 'panda' )  )->set_width(50)->set_default_value(''),
            Field::make( 'text', 'cardblock_btext',__(  'Button text', 'panda' )  )->set_width(50),
            Field::make( 'text', 'cardblock_burl', __( 'Button url', 'panda' )  )->set_width(50)->set_default_value(''),
            Field::make( 'checkbox', 'cardblock_show', __( 'Hide Card' ) )->set_option_value( 'yes' )

        ));

        Container::make( 'post_meta', __('Page Blocks','panda') )
            ->show_on_post_type(array('page'))
            ->show_on_template(array('template-about.php'))
            ->add_fields(array(
                Field::make( 'separator', 'teamblock_sep',__(  'Team Block', 'panda' )  ),
                Field::make( 'text', 'teamblock_title',__(  'Title', 'panda' )  ),
                Field::make( 'complex', 'teamblock_items', __(  'Team', 'panda' ) )
                    ->add_fields( array(
                        Field::make( 'text', 'position',__(  'Position', 'panda' )  ),
                        Field::make( 'image', 'photo',__(  'Photo', 'panda' )  )->set_required(1),
                        Field::make( 'rich_text', 'description',__(  'Content', 'panda' )  )->set_required(1),
                    ) ),
            ));

        Container::make( 'post_meta', __('Page Blocks','panda') )
            ->show_on_post_type(array('page'))
            ->show_on_template(array('template-service.php'))
            ->add_fields(array(
                Field::make( 'separator', 'tcolsblock_sep',__(  'Three cols Block', 'panda' )  ),
                Field::make( 'complex', 'tcols_items', __(  'Blocks', 'panda' ) )
                    ->add_fields( array(
                        Field::make( 'image', 'img',__(  'Image', 'panda' )  )->set_required(1),
                        Field::make( 'rich_text', 'description',__(  'Description', 'panda' )  )->set_required(1),
                    ) )->set_max(3),

                Field::make( 'separator', 'compareblock_sep',__(  'Compare Block', 'panda' )  ),

                Field::make( 'text', 'compare_ltop',__(  'Top text', 'panda' )  )->set_width(50),
                Field::make( 'text', 'compare_rtop',__(  'Top text', 'panda' )  )->set_width(50),

                Field::make( 'text', 'compare_ltitle',__(  'Title', 'panda' )  )->set_width(50)->set_required(1),
                Field::make( 'text', 'compare_rtitle',__(  'Title', 'panda' )  )->set_width(50)->set_required(1),

                Field::make( 'text', 'compare_lprice',__(  'Left Price', 'panda' )  )->set_width(25),
                Field::make( 'text', 'compare_lpriceperiod',__(  'Left price period', 'panda' )  )->set_width(25),
                Field::make( 'text', 'compare_rprice',__(  'Right Price', 'panda' )  )->set_width(25),
                Field::make( 'text', 'compare_rpriceperiod',__(  'Right price period', 'panda' )  )->set_width(25),

                Field::make( 'complex', 'compare_litems', __(  'Items', 'panda' ) )
                    ->add_fields( array(
                        Field::make( 'text', 'item',__(  'Text', 'panda' )  )->set_required(1),
                    ) )->set_width(50),
                Field::make( 'complex', 'compare_ritems', __(  'Items', 'panda' ) )
                    ->add_fields( array(
                        Field::make( 'text', 'item',__(  'Text', 'panda' )  )->set_required(1),
                    ) )->set_width(50),

                Field::make( 'complex', 'compare_lokitems', __(  'Checkmark items', 'panda' ) )
                    ->add_fields( array(
                        Field::make( 'text', 'item',__(  'Text', 'panda' )  )->set_required(1),
                    ) )->set_width(50),
                Field::make( 'complex', 'compare_rokitems', __(  'Checkmark items', 'panda' ) )
                    ->add_fields( array(
                        Field::make( 'text', 'item',__(  'Text', 'panda' )  )->set_required(1),
                    ) )->set_width(50),

                Field::make( 'text', 'compare_lpaypal',__(  'Paypal shortcode', 'panda' )  )->set_width(50),
                Field::make( 'text', 'compare_rpaypal',__(  'Paypal shortcode', 'panda' )  )->set_width(50),
                Field::make( 'text', 'compare_lstripe',__(  'Stripe shortcode', 'panda' )  )->set_width(50),
                Field::make( 'text', 'compare_rstripe',__(  'Stripe shortcode', 'panda' )  )->set_width(50),
                Field::make( 'text', 'compare_lbottom',__(  'Bottom text', 'panda' )  )->set_width(50),
                Field::make( 'text', 'compare_rbottom',__(  'Bottom text', 'panda' )  )->set_width(50),

                Field::make( 'separator', 'testimonialsblock_sep',__(  'Testimonials Block', 'panda' )  ),
                Field::make( 'text', 'testimonial_title',__(  'Title', 'panda' )  ),
                Field::make( 'complex', 'testimonial_items', __(  'Items', 'panda' ) )
                    ->add_fields( array(
                        Field::make( 'text', 'video',__(  'Vimeo video link', 'panda' )  )->set_required(1),
                    ) ),

                Field::make( 'separator', 'assblock_sep',__(  'Assistance Block', 'panda' )  ),
                Field::make( 'text', 'assistance_title',__(  'Title', 'panda' )  ),
                Field::make( 'rich_text', 'assistance_text',__(  'Text', 'panda' )  ),
                Field::make( 'text', 'price',__(  'Price', 'panda' )  ),
                Field::make( 'text', 'button_title',__(  'Button Title', 'panda' )  ),
                Field::make( 'text', 'button_link',__(  'Button Link', 'panda' )  ),

                Field::make( 'separator', 'faqblock_sep',__(  'FAQ Block', 'panda' )  ),
                Field::make( 'text', 'faq_title',__(  'Title', 'panda' )  ),
                Field::make( 'complex', 'faq_items', __(  'Items', 'panda' ) )
                        ->add_fields( array(
                            Field::make( 'text', 'question',__(  'Question', 'panda' )  )->set_required(1),
                            Field::make( 'rich_text', 'answes',__(  'Answer', 'panda' )  )->set_required(1),
                        ) )
                        ->set_collapsed(1)
                        ->set_header_template('
                                <% if (question) { %>
                                    <%- question %>
                                <% } %>
                            '),

            ));
        /*---------------------Fields for 4 adv pages---------------------*/
        Container::make( 'post_meta', __('Page Blocks','panda') )
            ->show_on_template('template-wtf.php')
            ->add_fields(array(
                Field::make( 'rich_text', 'top_applet',__(  'Top Applet', 'panda' )  ),
            ));

        /*-------------------Fields for travel template-------------------*/
        Container::make( 'post_meta', __('Page Blocks','panda') )
            ->show_on_post_type(array('page'))
            ->show_on_template('template-travel.php')
            ->add_fields(array(
                Field::make( 'rich_text', 'video_text',__(  'Video text', 'panda' )  ),
                Field::make( 'rich_text', 'video_frame',__(  'Video frame', 'panda' )  ),
                Field::make( 'rich_text', 'note',__(  'Note', 'panda' )  ),
                Field::make( 'separator', 'step_sep',__(  'Step 1', 'panda' )  ),
                Field::make( 'rich_text', 'first_step_text',__(  'Text', 'panda' )  ),
                Field::make( 'image', 'first_step_image',__(  'Image', 'panda' )  ),
                Field::make( 'separator', 'step_sep_2',__(  'Step 2', 'panda' )  ),
                Field::make( 'rich_text', 'second_step_text',__(  'Text', 'panda' )  ),
                Field::make( 'image', 'second_step_image',__(  'Image', 'panda' )  ),
                Field::make( 'separator', 'step_sep_3',__(  'Step 3', 'panda' )  ),
                Field::make( 'rich_text', 'third_step_text',__(  'Text', 'panda' )  ),
                Field::make( 'image', 'third_step_image',__(  'Image', 'panda' )  ),
                Field::make( 'separator', 'step_sep_4',__(  'Step 4', 'panda' )  ),
                Field::make( 'rich_text', 'fourth_step_text',__(  'Text', 'panda' )  ),
                Field::make( 'image', 'fourth_step_image',__(  'Image', 'panda' )  ),
                Field::make( 'separator', 'three_blocks',__(  'Benefits', 'panda' )  ),
                Field::make( 'rich_text', 'first_benefit',__(  'Text', 'panda' )  ),
                Field::make( 'rich_text', 'second_benefit',__(  'Text', 'panda' )  ),
                Field::make( 'rich_text', 'third_benefit',__(  'Text', 'panda' )  ),
                Field::make( 'separator', 'step_sep_5',__(  'Step 5', 'panda' )  ),
                Field::make( 'rich_text', 'fifth_step_text',__(  'Text', 'panda' )  ),
                Field::make( 'image', 'fifth_step_image',__(  'Image', 'panda' )  ),

                Field::make( 'separator', 'tcolsblock_sep',__(  'Three cols Block', 'panda' )  ),
                Field::make( 'complex', 'tcols_items', __(  'Blocks', 'panda' ) )
                    ->add_fields( array(
                        Field::make( 'image', 'img',__(  'Image', 'panda' )  )->set_required(1),
                        Field::make( 'rich_text', 'description',__(  'Description', 'panda' )  )->set_required(1),
                    ) )->set_max(3),

                Field::make( 'separator', 'compareblock_sep1',__(  'Compare Block', 'panda' )  ),

                Field::make( 'text', 'compare_ltop',__(  'Top text', 'panda' )  ),

                Field::make( 'text', 'compare_ltitle',__(  'Title', 'panda' )  )->set_required(1),

                Field::make( 'text', 'compare_lprice',__(  'Price', 'panda' )  )->set_width(50),
                Field::make( 'text', 'compare_lpriceperiod',__(  'price period', 'panda' )  )->set_width(50),

                Field::make( 'complex', 'compare_litems', __(  'Items', 'panda' ) )
                    ->add_fields( array(
                        Field::make( 'text', 'item',__(  'Text', 'panda' )  )->set_required(1),
                    ) ),

                Field::make( 'complex', 'compare_lokitems', __(  'Checkmark items', 'panda' ) )
                    ->add_fields( array(
                        Field::make( 'text', 'item',__(  'Text', 'panda' )  )->set_required(1),
                    ) ),

                Field::make( 'text', 'compare_lpaypal',__(  'Paypal shortcode', 'panda' )  )->set_width(50),
                Field::make( 'text', 'compare_lstripe',__(  'Stripe shortcode', 'panda' )  )->set_width(50),
                Field::make( 'text', 'compare_lbottom',__(  'Bottom text', 'panda' )  )->set_width(50),

                Field::make( 'separator', 'teammeetblock_sep',__(  'Meet the team Block', 'panda' )  ),
                Field::make( 'text', 'teammeet_title',__(  'Title', 'panda' )  )->set_required(1),
                Field::make( 'image', 'teammeet_img',__(  'Image', 'panda' )  )->set_required(1),
                Field::make( 'rich_text', 'teammeet_description',__(  'Description', 'panda' )  ),
                Field::make( 'text', 'teammeet_position',__(  'Position', 'panda' )  ),

                Field::make( 'separator', 'sheduleblock_sep',__(  'Shedule Block', 'panda' )  ),
                Field::make( 'rich_text', 'sheduleblock_description',__(  'Description', 'panda' )  ),
                Field::make( 'text', 'sheduleblock_btext',__(  'Button text', 'panda' )  )->set_width(50),
                Field::make( 'text', 'sheduleblock_burl', __( 'Button url', 'panda' )  )->set_width(50)->set_default_value('#'),


                Field::make( 'separator', 'testimonialsblock_sep',__(  'Testimonials Block', 'panda' )  ),
                Field::make( 'text', 'testimonial_title',__(  'Title', 'panda' )  ),
                Field::make( 'complex', 'testimonial_items', __(  'Items', 'panda' ) )
                    ->add_fields( array(
                        Field::make( 'text', 'video',__(  'Vimeo video link', 'panda' )  )->set_required(1),
                    ) ),

                Field::make( 'separator', 'assblock_sep',__(  'Assistance Block', 'panda' )  ),
                Field::make( 'text', 'assistance_title',__(  'Title', 'panda' )  ),
                Field::make( 'rich_text', 'assistance_text',__(  'Text', 'panda' )  ),
                Field::make( 'text', 'price',__(  'Price', 'panda' )  ),
                Field::make( 'text', 'button_title',__(  'Button Title', 'panda' )  ),
                Field::make( 'text', 'button_link',__(  'Button Link', 'panda' )  ),

                Field::make( 'separator', 'faqblock_sep',__(  'FAQ Block', 'panda' )  ),
                Field::make( 'text', 'faq_title',__(  'Title', 'panda' )  ),
                Field::make( 'complex', 'faq_items', __(  'Items', 'panda' ) )
                        ->add_fields( array(
                            Field::make( 'text', 'question',__(  'Question', 'panda' )  )->set_required(1),
                            Field::make( 'rich_text', 'answes',__(  'Answer', 'panda' )  )->set_required(1),
                        ) )
                        ->set_collapsed(1)
                        ->set_header_template('
                                <% if (question) { %>
                                    <%- question %>
                                <% } %>
                            '),

            ));

        Container::make( 'post_meta', __('Page Blocks','panda') )
            ->show_on_post_type(array('page'))
            ->show_on_template(array('template-course.php'))
            ->add_fields(array(
                Field::make( 'separator', 'aboutblock_sep',__(  'About the course', 'panda' )  ),
                Field::make( 'rich_text', 'aboutblock_text',__(  'About text', 'panda' )  ),
                Field::make( 'text', 'aboutblock_video',__(  'About Video link', 'panda' )  ),

                Field::make( 'separator', 'wdwlblock_sep',__(  'What do you learn', 'panda' )  ),
                Field::make( 'text', 'wdwl_title',__(  'Title', 'panda' )  ),
                Field::make( 'complex', 'wdwl_items', __(  'Items', 'panda' ) )
                        ->add_fields( array(
                            Field::make( 'image', 'img',__(  'Item image', 'panda' )  )->set_required(1)->set_width(30),
                            Field::make( 'rich_text', 'item',__(  'Item text', 'panda' )  )->set_required(1)->set_width(70),
                        ) )
                        ->set_collapsed(1),

                Field::make( 'separator', 'teachersblock_sep',__(  'Teachers', 'panda' )  ),
                Field::make( 'complex', 'teachers_items', __(  'Teachers list', 'panda' ) )
                        ->add_fields( array(
                            Field::make( 'image', 'img',__(  'Photo', 'panda' )  )->set_required(1),
                            Field::make( 'text', 'name',__(  'Name', 'panda' )  )->set_required(1),
                            Field::make( 'text', 'position',__(  'Position', 'panda' )  ),
                            Field::make( 'text', 'achiev',__(  'Main achievements', 'panda' )  ),
                            Field::make( 'text', 'description',__(  'Description', 'panda' )  ),

                            Field::make( 'text', 'rating',__(  'Rating', 'panda' )  )->set_width(25),
                            Field::make( 'text', 'reviews',__(  'Reviews', 'panda' )  )->set_width(25),
                            Field::make( 'text', 'students',__(  'Students', 'panda' )  )->set_width(25),
                            Field::make( 'text', 'courses',__(  'Courses', 'panda' )  )->set_width(25),
                        ) )
                        ->set_collapsed(1)
                        ->set_header_template('
                                <% if (name) { %>
                                    <%- name %>
                                <% } %>
                            '),

                Field::make( 'separator', 'includesblock_sep',__(  'Course includes', 'panda' )  ),
                Field::make( 'text', 'includes_title',__(  'Title', 'panda' )  ),
                Field::make( 'complex', 'includes_items', __(  'Items', 'panda' ) )
                        ->add_fields( array(
                            Field::make( 'image', 'img',__(  'Item image', 'panda' )  )->set_required(1)->set_width(30),
                            Field::make( 'text', 'item',__(  'Item text', 'panda' )  )->set_required(1)->set_width(70),
                        ) )
                        ->set_collapsed(1)->set_max(5),

                Field::make( 'separator', 'cbannerblock_sep',__(  'Banner with button', 'panda' )  ),
                Field::make( 'text', 'cbanner_title',__(  'Title', 'panda' )  ),
                Field::make( 'text', 'cbanner_price',__(  'Price', 'panda' )  ),
                Field::make( 'text', 'cbanner_btext',__(  'Button text', 'panda' )  )->set_width(50),
                Field::make( 'text', 'cbanner_burl', __( 'Button url', 'panda' )  )->set_width(50)->set_default_value('#'),

            ));

        Container::make( 'post_meta', __('Page Blocks','panda') )
            ->show_on_post_type(array('page'))
            ->show_on_template(array('template-work.php'))
            ->add_fields(array(
                Field::make( 'separator', 'programsblock_sep',__(  'Programs', 'panda' )  ),
                Field::make( 'complex', 'program_items', __(  'Programs list', 'panda' ) )
                    ->add_fields( array(
                        Field::make( 'image', 'img',__(  'Image', 'panda' )  )->set_required(1),
                        Field::make( 'rich_text', 'description',__(  'Description', 'panda' )  )->set_required(1),
                        Field::make( 'text', 'btext',__(  'Button text', 'panda' )  )->set_width(50),
                        Field::make( 'text', 'blink',__(  'Button link', 'panda' )  )->set_width(50),
                        //Field::make( 'text', 'cbanner_burl', __( 'Button url', 'panda' )  )->set_width(50)->set_default_value('#'),
                    ) )->set_required(1),
            ));

}

add_action( 'carbon_fields_register_fields', 'panda_content_blocks' );
function panda_content_blocks()
{
    Block::make( 'ProssCons' )
    ->add_fields( [
        Field::make( 'separator', 'pross_sep',__(  'Pross', 'panda' )  ),
        Field::make( 'text', 'pross_title',__(  'Title', 'panda' )  ),
        Field::make( 'rich_text', 'pross_content',__(  'Content', 'panda' )  ),
        Field::make( 'separator', 'cons_sep',__(  'Cons', 'panda' )  ),
        Field::make( 'text', 'cons_title',__(  'Title', 'panda' )  ),
        Field::make( 'rich_text', 'cons_content',__(  'Content', 'panda' )  ),
     ] )
     ->set_render_callback( function( $data ) {
         ?>
            <div class="two-columns-marker-list">
                <div class="two-columns-marker-list__column two-columns-marker-list__column--first">
                    <div class="two-columns-marker-list__column-header">
                        <?php if($data['pross_title']){ ?>
                            <h3><?=$data['pross_title']?></h3>
                        <?php } ?>
                    </div>
                    <?=apply_filters('the_content',$data['pross_content'])?>
                </div>
                <div class="two-columns-marker-list__column two-columns-marker-list__column--second">
                    <div class="two-columns-marker-list__column-header">
                        <?php if($data['cons_title']){ ?>
                            <h3><?=$data['cons_title']?></h3>
                        <?php } ?>
                    </div>
                    <?=apply_filters('the_content',$data['cons_content'])?>
                </div>
            </div>
         <?php
     });


    Block::make( 'Slider' )
    ->add_fields( [
        Field::make( 'media_gallery', 'images',__(  'Images', 'panda' )  )->set_duplicates_allowed( false )->set_required(1),
     ] )
    ->set_render_callback( function( $data ) {
        if(isset($data['images'])){
         ?>
            <div class="blog-post-carousel-container">
				<div class="swiper-wrapper">
                    <?php foreach($data['images'] as $img){ ?>
                        <div class="swiper-slide">
                            <?=wp_get_attachment_image($img,'large')?>
                        </div>
                    <?php } ?>
				</div>
				<div class="swiper-button-next"></div>
				<div class="swiper-button-prev"></div>
				<div class="swiper-pagination"></div>
			</div>
         <?php
        }
    });
}