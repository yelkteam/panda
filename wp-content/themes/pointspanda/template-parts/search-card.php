<li class="search-result-list__item search-result-list__item--credit-card blog-card">
    <a href="<?=get_the_permalink()?>" class="blog-card__link">
		<div class="blog-card__img-box">
			<?=get_the_post_thumbnail(null,'big-thumbnail')?>
		</div>
		<div class="blog-card__info">
			<h4 class="blog-card__title"><?=get_the_title()?></h4>
			<div class="blog-card__content">
			    <?php the_excerpt(); ?>
			</div>
			<object>
				<a class="read-more-link" href="<?=get_the_permalink()?>"><?=__('View this card','panda')?></a>
			</object>
		</div>
	</a>
</li>