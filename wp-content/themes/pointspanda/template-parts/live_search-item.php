<?php
	global $post;
?>
    <li class="search-form__list-item">
		<a href="<?=get_the_permalink()?>">
			<div class="search-form__img-box">
				<?=get_the_post_thumbnail(null,'icon')?>
			</div>
			    <div>
	    			<div class="search-form__item-category"><?=get_post_type_title($post->post_type)?></div>
		    		<div class="search-form__result-item">
			    	<span><?=$post->post_title?></span>
					<span class="search-form__mark"><?=get_post_terms_string($post)?></span>
				</div>
			</div>
		</a>
	</li>