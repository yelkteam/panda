<?php 
	$tags = wp_get_post_terms(get_the_ID(),'category');
	$placeholder = carbon_get_theme_option( 'defimage' );
?>

<li class="search-result-list__item search-result-list__item blog-card">
    <a href="<?=get_the_permalink()?>" class="blog-card__link">
		<div class="blog-card__img-box">
			<?php 
													if(has_post_thumbnail()){
														echo get_the_post_thumbnail(null,'big-thumbnail');
													}else{
														echo wp_get_attachment_image($placeholder,'big-thumbnail');
													}
										?>
			<?php 
				if($tags){
					foreach($tags as $tag){
						?>
							<object>
			                	<a href="<?=get_category_link($tag->term_id)?>" class="blog-card__img-tag"><?=$tag->name?></a>
			              	</object>
						<?php 
						break;
					}
				}
			?>
		</div>
		<div class="blog-card__info">
			<h4 class="blog-card__title"><?=get_the_title()?></h4>
			<div class="blog-card__content">
				<?php the_excerpt(); ?>
			</div>
			<div class="blog-card__date"><?=get_the_date('F d,Y')?></div>
		</div>
	</a>
</li>