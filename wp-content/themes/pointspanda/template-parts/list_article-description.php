<?php
    $tags = wp_get_post_terms(get_the_ID(),'category');
    $placeholder = carbon_get_theme_option( 'defimage' );
?>
<div class="horizontal-article-card">
    <a href="<?=get_the_permalink()?>" class="horizontal-article-card__link">
        <div class="horizontal-article-card__img-box">
            <?php
                if(has_post_thumbnail()){
                    echo get_the_post_thumbnail(null,'thumbnail');
                }else{
                    echo wp_get_attachment_image($placeholder,'thumbnail');
                }
            ?><!--
            <?php
    			if($tags){
    				foreach($tags as $tag){
    					?>
                            <object>
                                <a href="<?=get_category_link(get_primary_category($tag->term_id))?>" class="horizontal-article-card__tag card-tag"><?=get_cat_name(get_primary_category($tag->term_id));?></a>
                            </object>
                        <?php
                        break;
    				}
    			}
    		?>-->
            <?php
                $primary_cat_id = get_post_meta( $post->ID, 'rank_math_primary_category', true );

                if ( $primary_cat_id ) {
                    $product_cat = get_term( $primary_cat_id, 'category' );
                    ?>
                    <object>
                        <a href="<?=get_category_link($product_cat->term_id)?>" class="blog-card__img-tag"><?=$product_cat->name;?></a>
                    </object>
                    <?php
                }
            ?>
        </div>
        <div class="horizontal-article-card__text-content">
            <h4 class="horizontal-article-card__title"><?php the_title(); ?></h4>
            <p class="horizontal-article-card__description"><?=get_the_excerpt()?></p>
            <div class="horizontal-article-card__date"><?=get_the_date('F d, Y')?></div>
        </div>
    </a>
</div>