<div id="mlb2-3426361" class="ml-form-embedContainer ml-subscribe-form ml-subscribe-form-3426361<?php echo (is_single())?' collapsed-subscribe-box__form':''; ?>">
    <div class="ml-form-align-center">
        <div class="ml-form-embedWrapper embedForm">
            <div class="ml-form-embedBody ml-form-embedBodyHorizontal row-form">
                <div class="ml-form-embedContent" style="margin-bottom:0"></div>
                    <form class="ml-block-form" action="https://static.mailerlite.com/webforms/submit/y2y4z4" data-code="y2y4z4" method="post" target="_blank">
                        <div class="ml-form-formContent subscribe-form-box__form">
                            <div class="ml-field-group ml-field-email ml-validate-email ml-validate-required mc-field-group">
                                <input type="email" class="form-control subscribe-form-box__input <?php echo (is_single())?' collapsed-subscribe-box__input base-subscribe-input':''; ?>" data-inputmask="" name="fields[email]" placeholder="Email" autocomplete="email">
                            </div>
                            <div class="clear">
                                <input type="submit" class="button green-link-btn subscribe-form-box__btn <?php echo (is_single())?' collapsed-subscribe-box__submit-button':''; ?>"  value="Subscribe" >
                                <button disabled="disabled" style="display:none" type="button" class="loading"> <div class="ml-form-embedSubmitLoad"><div></div><div></div><div></div><div></div></div> </button>
                            </div>
                        </div>
                    <input type="hidden" name="ml-submit" value="1">
                </form>
            </div>
            <div class="ml-form-successBody row-success" style="display:none">
                <div class="ml-form-successContent">
                    <p>You have successfully joined our subscriber list.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
	function ml_webform_success_3426361(){
	  var r=ml_jQuery||jQuery;
	  r(".ml-subscribe-form-3426361 .row-success").show();
	  r('.subscribe-form-box__input').val('');
	  setTimeout(function () {
      r(".ml-subscribe-form-3426361 .row-success").hide();
	  }, 3000);
	}
</script>
<img src="https://track.mailerlite.com/webforms/o/3426361/y2y4z4?v1611576327" width="1" height="1" style="max-width:1px;max-height:1px;visibility:hidden;padding:0;margin:0;display:block" alt="." border="0">
