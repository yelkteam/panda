<?php 
/*
*
* Template Name: WTF?
*
*/
get_header();?>

<?php while ( have_posts() ) : the_post(); ?>
    <main class="page-main-content">
        <div class="wrapper">
            <section class="breadcrumbs-box">
                <?php panda_breadcrumbs(); ?>
            </section>
            <section class="post-title-big-poster">
                <div class="post-title-big-poster__img-box">
                    <?=get_the_post_thumbnail( null, 'large' )?>
                </div>
                <h1 class="post-title-big-poster__title"><?=get_the_title()?></h1>
            </section>
            <section class="page-content-box">

                <section class="page-content">

                    <?php the_content(); ?>

                </section>
                
            </section>
            <?php echo carbon_get_the_post_meta('top_applet'); ?>
        </div>
    </main>
<?php endwhile; ?>

<?php get_footer();?>