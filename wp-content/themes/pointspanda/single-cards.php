<?php get_header();?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php 

		$clink = carbon_get_the_post_meta('card_url');
		$cardslider_show = carbon_get_theme_option('cardslider_show');

		$infoblock_show = carbon_get_the_post_meta('infoblock_show');

		$disclosure1 = carbon_get_theme_option( 'disclosure1' );
		$disclosure2 = carbon_get_theme_option( 'disclosure2' );

	?>
    <main class="show-credit-card-page-main-content">
	  <div class="wrapper">
	    <section class="breadcrumbs-box">
	      <?php panda_breadcrumbs(); ?>
	    </section>

	    <section class="card-header-info-box">
	      <div class="card-header-info-box__header">
	        <?php the_title('<h1>','</h1>'); ?>
	        <?php 
	        	if($infoblock_show ){
	        		$card_options = carbon_get_the_post_meta('card_options');
	        		$recommend_rating = carbon_get_the_post_meta('recommend_rating');
	        		$recommend_rating = intval($recommend_rating);

	        		$recommend_stitle = carbon_get_the_post_meta('recommend_stitle');

	        		$rating_text = carbon_get_theme_option( 'cardtext'.$recommend_rating );
	        ?>
		        <div class="card-statistics">
		          <div class="card-statistics__first-row">
		          	<?php if($card_options){ ?>
		          		<?php foreach ($card_options as $option) { ?>
		          			<div class="card-statistics__item">
				              <div class="card-statistics__title"><?=$option['title']?></div>
				              <div class="card-statistics__value"><?=$option['content']?></div>
				            </div>	
		          		<?php } ?>
			        <?php } ?>
		          </div>
		          <div class="card-statistics__second-row">
		          	<?php if($recommend_rating){ ?>
			            <div class="card-statistics__diagram-icon">
			              <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
			                <rect y="25.2632" width="4" height="6.73684" rx="2" fill="<?=(($recommend_rating >= 1)?'#146AFF':'#D7E3F6')?>"/>
			                <path d="M14 14.6316C14 13.527 14.8954 12.6316 16 12.6316C17.1046 12.6316 18 13.527 18 14.6316V30C18 31.1046 17.1046 32 16 32C14.8954 32 14 31.1046 14 30V14.6316Z" fill="<?=(($recommend_rating >= 3)?'#146AFF':'#D7E3F6')?>"/>
			                <path d="M7 21.3684C7 20.2638 7.89543 19.3684 9 19.3684C10.1046 19.3684 11 20.2638 11 21.3684V30C11 31.1046 10.1046 32 9 32C7.89543 32 7 31.1046 7 30V21.3684Z" fill="<?=(($recommend_rating >= 2)?'#146AFF':'#D7E3F6')?>"/>
			                <path d="M21 8.73681C21 7.63225 21.8954 6.73682 23 6.73682C24.1046 6.73682 25 7.63225 25 8.73682V30C25 31.1045 24.1046 32 23 32C21.8954 32 21 31.1045 21 30V8.73681Z" fill="<?=(($recommend_rating >= 4)?'#146AFF':'#D7E3F6')?>"/>
			                <path d="M28 2C28 0.895431 28.8954 0 30 0C31.1046 0 32 0.895431 32 2V30C32 31.1046 31.1046 32 30 32C28.8954 32 28 31.1046 28 30V2Z" fill="<?=(($recommend_rating > 4)?'#146AFF':'#D7E3F6')?>"/>
			              </svg>
			            </div>
			            <div class="card-statistics__credit-rating">
			              <div class="card-statistics__value"><?=$rating_text?></div>
			              <div class="card-statistics__regular-text"><?=$recommend_stitle?></div>
			            </div>
			        <?php } ?>

			        <?php if(function_exists('the_ratings')) { the_ratings(); } ?>

		          </div>
		        </div>
		    <?php } ?>
	        <div class="card-header-info-box__img-box">
	          <?=get_the_post_thumbnail( null, 'big_thumbnail' )?>
	        </div>
	      </div>
	      <div class="card-header-info-box__text-box">
	        <?php the_content(); ?>
	      </div>
	      <div class="card-header-info-box__actions">
	      	<?php if($clink){ ?>
		        	<a class="read-more-link" href="<?=$clink?>"><?=__('Learn How to Apply','panda')?></a>
		    <?php } ?>
		    <?php if($disclosure1 || $disclosure2){ ?>
		        <button id="popupBtn" class="card-header-info-box__popup-btn">
		          <?=__('Disclosures','panda')?>
		          <svg width="16" height="17" viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
		            <path d="M2 14.9777V2.5C2 2.22386 2.22386 2 2.5 2H13.5C13.7761 2 14 2.22386 14 2.5V14.9777C14 15.3938 13.5215 15.6278 13.193 15.3724L8.30697 11.5721C8.12642 11.4317 7.87358 11.4317 7.69303 11.5721L2.80697 15.3724C2.47854 15.6278 2 15.3938 2 14.9777Z" stroke="#6B6B79" stroke-width="1.6" stroke-linecap="round" stroke-linejoin="round"/>
		          </svg>
		        </button>
	        <?php } ?>
	      </div>
	    </section>

		<?php 
			if($cardslider_show){

	    	$cats = wp_get_post_terms( get_the_ID(), 'card-category', array('fields' => 'ids')  );
	    	
	    	$args = array(
				'posts_per_page' => 4,
				'tax_query' => array(
									array(
										'taxonomy' => 'card-category',
										'field' => 'term_id',
										'operator' => 'IN',
										'terms' => $cats,
									),
									),
			);

			$query = new WP_Query( $args );

			if ( $query->have_posts() ) {
		?>

		    <section class="credit-cards-arrows-slider-box base-arrows-slider-box">
		      <div class="base-arrows-slider-box__header">
		        <h2><?=__('You might also be interested in','panda')?></h2>
		        <div class="base-arrows-slider-box__arrows">
		          <button class="base-arrows-slider-box__prev-arrow">
		            <svg width="8" height="16" viewBox="0 0 10 19" fill="none" xmlns="http://www.w3.org/2000/svg">
		              <path d="M8.29346 1.25473C8.6843 0.864517 9.31746 0.865026 9.70767 1.25586C10.0979 1.6467 10.0974 2.27987 9.70654 2.67008L8.29346 1.25473ZM1 9.94956L0.293461 10.6572C0.105577 10.4697 0 10.2151 0 9.94956C0 9.68407 0.105577 9.42947 0.293461 9.24189L1 9.94956ZM9.70654 17.229C10.0974 17.6193 10.0979 18.2524 9.70767 18.6433C9.31746 19.0341 8.6843 19.0346 8.29346 18.6444L9.70654 17.229ZM9.70654 2.67008L1.70654 10.6572L0.293461 9.24189L8.29346 1.25473L9.70654 2.67008ZM1.70654 9.24189L9.70654 17.229L8.29346 18.6444L0.293461 10.6572L1.70654 9.24189Z" fill="#146AFF"/>
		            </svg>
		          </button>
		          <button class="base-arrows-slider-box__next-arrow">
		            <svg width="8" height="16" viewBox="0 0 10 19" fill="none" xmlns="http://www.w3.org/2000/svg">
		              <path d="M1.70654 1.25473C1.3157 0.864517 0.682536 0.865026 0.292326 1.25586C-0.097885 1.6467 -0.0973765 2.27987 0.293461 2.67008L1.70654 1.25473ZM9 9.94956L9.70654 10.6572C9.89442 10.4697 10 10.2151 10 9.94956C10 9.68407 9.89442 9.42947 9.70654 9.24189L9 9.94956ZM0.293461 17.229C-0.0973765 17.6193 -0.097885 18.2524 0.292326 18.6433C0.682536 19.0341 1.3157 19.0346 1.70654 18.6444L0.293461 17.229ZM0.293461 2.67008L8.29346 10.6572L9.70654 9.24189L1.70654 1.25473L0.293461 2.67008ZM8.29346 9.24189L0.293461 17.229L1.70654 18.6444L9.70654 10.6572L8.29346 9.24189Z" fill="#146AFF"/>
		            </svg>
		          </button>
		        </div>
		      </div>
		      <div class="swiper-container credit-cards-arrows-slider" id="creditCardsArrowsSlider">
		        <div class="swiper-wrapper">

		          <?php  while ( $query->have_posts() ) { $query->the_post(); ?>
			          <div class="swiper-slide">
			            <div class="blog-card">
			            	<a href="<?=get_the_permalink()?>" class="blog-card__link">
				              <div class="blog-card__img-box">
				                <?=get_the_post_thumbnail( null, 'thumbnail' )?>
				              </div>
				              <div class="blog-card__info">
				                <h4 class="blog-card__title"><?=get_the_title()?></h4>
				                <div class="blog-card__content">
				                  <?php the_excerpt(); ?>
				                </div>
				                <object>
				                    <a class="read-more-link" href="<?=get_the_permalink()?>"><?=__('Keep reading','panda')?></a>
				                </object>
				              </div>
				          	</a>
			            </div>
			          </div>
					<?php  } wp_reset_postdata(); ?>		          

		        </div>
		        <div class="credit-cards-arrows-slider-box__dots base-arrows-slider-box__dots"></div>
		      </div>
		    </section>
			<?php  }  ?>
		<?php  }  ?>
	  </div>
	  <div id="disclosuresModal" class="modal">
	    <div class="modal__content">
	      <div class="modal__text-box">
	        <?=apply_filters('the_content',$disclosure1)?>
	      </div>
	      <div class="modal__text-box">
	        <?=apply_filters('the_content',$disclosure2)?>
	      </div>
	      <button class="modal__close-btn">
	        <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
	          <path d="M12.5667 2.56501C12.8791 2.25259 12.8791 1.74605 12.5667 1.43364C12.2542 1.12122 11.7477 1.12122 11.4353 1.43364L12.5667 2.56501ZM1.43394 11.435C1.12152 11.7474 1.12152 12.2539 1.43394 12.5664C1.74636 12.8788 2.25289 12.8788 2.56531 12.5664L1.43394 11.435ZM11.4353 1.43364L1.43394 11.435L2.56531 12.5664L12.5667 2.56501L11.4353 1.43364Z" fill="#1B1B32"/>
	          <path d="M11.4353 12.5664C11.7477 12.8788 12.2542 12.8788 12.5667 12.5664C12.8791 12.2539 12.8791 11.7474 12.5667 11.435L11.4353 12.5664ZM2.56531 1.43364C2.25289 1.12122 1.74636 1.12122 1.43394 1.43364C1.12152 1.74606 1.12152 2.25259 1.43394 2.56501L2.56531 1.43364ZM12.5667 11.435L2.56531 1.43364L1.43394 2.56501L11.4353 12.5664L12.5667 11.435Z" fill="#1B1B32"/>
	        </svg>
	      </button>
	    </div>
	  </div>
	</main>

    <?php endwhile; ?>

<?php get_footer();?>