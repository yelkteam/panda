  "use strict";

  var openModal = initModal('disclosuresModal');
  var popupBtn = document.getElementById('popupBtn');
  if (openModal && popupBtn) {
    popupBtn.addEventListener('click', openModal);
  }
