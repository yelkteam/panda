"use strict";

function initSelect(id, svgArrow, messageId) {
	if (messageId === void 0) {
		messageId = '';
	}

	var selectWrapper = document.getElementById(id);
	var select = selectWrapper.querySelector('select');
	var message = document.getElementById(messageId);
	var arrow = document.createElement('div');
	arrow.classList.add('select-arrow-btn');
	arrow.innerHTML = svgArrow.trim();
	var selectHeader = document.createElement('div');
	selectHeader.classList.add('select-header');
	selectHeader.innerHTML = select.options[select.selectedIndex].innerHTML;
	selectWrapper.appendChild(selectHeader);
	var selectItems = document.createElement('div');
	selectItems.classList.add('select-items', 'select-hide');
	selectItems.classList.add('select-hide');
	selectItems.style.paddingTop = '0';

	if (message) {
		message.innerHTML = select.options[select.selectedIndex].dataset.message;
	}

	var _loop = function _loop(i) {
		var item = document.createElement('div');

		if (i === 0) {
			item.classList.add('selected-item');
		}

		item.innerHTML = select.options[i].innerHTML;
		item.addEventListener('click', function () {
			for (var _i = 0; _i < select.length; _i += 1) {
				if (select.options[_i].innerHTML === item.innerHTML) {
					select.selectedIndex = _i;
					selectHeader.childNodes[0].textContent = item.innerHTML;
					var selectedItem = item.parentNode.querySelector('.selected-item');
					selectedItem.classList.remove('selected-item');
					item.classList.add('selected-item');
					close();
					break;
				}
			}

			if (message) {
				showMessage(message);
			}
		});
		selectItems.appendChild(item);
	};

	for (var i = 0; i < select.length; i += 1) {
		_loop(i);
	}

	selectWrapper.appendChild(selectItems);
	selectHeader.appendChild(arrow);
	selectHeader.addEventListener('click', function () {
		if (selectHeader.nextSibling.classList.contains('select-hide') && !selectHeader.classList.contains('select-arrow-active')) {
			selectItems.style.paddingTop = selectHeader.offsetHeight / 2 + "px";
		} else {
			selectItems.style.paddingTop = '0';
		}

		selectHeader.nextSibling.classList.toggle('select-hide');
		selectHeader.classList.toggle('select-arrow-active');
	});

	function close() {
		selectItems.style.paddingTop = '0';
		selectHeader.classList.remove('select-arrow-active');
		selectItems.classList.add('select-hide');
	}

	document.addEventListener('click', function (_ref) {
		var target = _ref.target;

		if (target !== selectHeader && !isDescendant(selectHeader, target)) {
			close();
		}
	});

	function showMessage(target) {
		if (selectHeader.nextSibling.classList.contains('select-hide') && !selectHeader.classList.contains('select-arrow-active')) {
			if (!target.style.transition) {
				message.style.transition = 'opacity 1s linear';
			}

			var messageHtml = select.options[select.selectedIndex].dataset.message;

			if (messageHtml) {
				target.innerHTML = messageHtml;
				target.style.opacity = '1';
			} else {
				target.style.opacity = '0'
			}

			if (target.style.opacity === '1') {
				target.style.visibility = 'initial';
			}

			target.addEventListener('transitionend', setDisplayNone);
		}
	}
}

function setDisplayNone(e) {
	if (window.getComputedStyle(e.target).opacity === '0') {
		e.target.style.visibility = 'visible';
	}

	e.target.removeEventListener('transitionend', setDisplayNone);
}

function isDescendant(parent, child) {
	var node = child.parentNode;

	while (node !== null) {
		if (node === parent) {
			return true;
		}

		node = node.parentNode;
	}

	return false;
}

function initAutoResizeTextarea(id) {
	var textarea = document.getElementById(id);
	textarea.style.height = textarea.scrollHeight + "px";
	textarea.addEventListener('input', onInput);

	function onInput() {
		this.style.height = 'auto';
		this.style.height = this.scrollHeight + "px";
	}
}

function validateForm(id) {
	var form = document.getElementById(id);
	var inputs = form.querySelectorAll('input[type=text], textarea.comment-form__field ');
	var messageText = 'Field is required';
	var messageVisibleClassName = 'comment-form__validation-message--visible';
	var inputErrorClassName = 'comment-form__field--error';

	for (var i = 0; i < inputs.length; i += 1) {
		inputs[i].addEventListener('blur', function (event) {
			var input = event.target;
			if (input.value !== '' && !input.dataset.linkContains) {
				input.classList.remove(inputErrorClassName);
				input.nextElementSibling.classList.remove(messageVisibleClassName);
			}
		});
	}

	if (form) {
		form.addEventListener('submit', function (event) {
			var isError = false;

			for (var i = 0; i < inputs.length; i += 1) {
				var message = inputs[i].nextElementSibling;

				if (inputs[i].value === '') {
					isError = true;
					message.innerHTML = messageText;
					inputs[i].classList.add(inputErrorClassName);
					message.classList.add(messageVisibleClassName);
				} else if (!inputs[i].dataset.linkContains) {
					inputs[i].classList.remove(inputErrorClassName);
					message.classList.remove(messageVisibleClassName);
				} else {
					isError = true;
					message.innerHTML = 'Warning: URLs in comments are not allowed. Please, try again.';
					inputs[i].classList.add(inputErrorClassName);
					message.classList.add(messageVisibleClassName);
				}
			}

			if (isError) {
				event.preventDefault();
			}
		});

		initMessageSanitize();
	}
}

function initMessageSanitize() {
	var field = document.getElementById('messageTextarea');

	if (!field) {
		return;
	}

	var message = field.nextElementSibling;
	var messageVisibleClassName = 'comment-form__validation-message--visible';
	var inputErrorClassName = 'comment-form__field--error';

	field.addEventListener('input', function () {
		var regexp = /(((https?:\/\/)|(www\.))[^\s]+)/g;

		if (regexp.test(field.value)) {
			message.innerHTML = 'Warning: URLs in comments are not allowed. Please, try again.';
			field.classList.add(inputErrorClassName);
			message.classList.add(messageVisibleClassName);
			field.dataset.linkContains = 'true';
		} else {
			field.classList.remove(inputErrorClassName);
			message.classList.remove(messageVisibleClassName);
			field.dataset.linkContains = '';
		}
	});
}
