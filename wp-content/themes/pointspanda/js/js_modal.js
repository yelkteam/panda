"use strict";

function initModal(id, afterClose) {
	if (typeof afterClose !== 'function') {
		afterClose = function (){}
	}
	var modal = document.getElementById(id);
	var closeBtn = modal.querySelector('.modal__close-btn');
	modal.addEventListener('click', function (_ref) {
		var target = _ref.target;

		if (target === modal) {
			modal.style.display = 'none';
			afterClose();
		}
	});
	closeBtn.addEventListener('click', function (_ref2) {
		var currentTarget = _ref2.currentTarget;

		if (currentTarget === closeBtn) {
			modal.style.display = 'none';
			afterClose();
		}
	});

	function open() {
		modal.style.display = 'flex';
	}

	return open;
}
