"use strict";

  initSelect('requestSelect', "<svg width=\"6\" height=\"5\" viewBox=\"0 0 6 5\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n\t\t\t<path d=\"M5.36923 0.400026C5.70451 0.400026 5.89101 0.787753 5.68174 1.0497L3.31329 4.01423C3.15316 4.21467 2.8484 4.21467 2.68827 4.01423L0.319822 1.0497C0.110545 0.787753 0.297053 0.400026 0.632333 0.400026H5.36923Z\" fill=\"#6B6B79\"/>\n\t\t </svg>", 'infoText');
  initAutoResizeTextarea('messageTextarea');

(function ($) {

    $(document).ready(function () {
        show_contacts();
        select_theme();
    });
    
    function show_contacts(){
    	$('#show_email').on('click', function(){
    		let data = {
                        'action': 'show_email',
                    };
            let $this = $(this);
            $.ajax({
                        url: vars_ajax.ajax_url,
                        data: data,
                        type: 'POST',
                        success: function (resonse) {
                            $this.parent().children('.contacts-list__text').text(resonse);
                        },
                        error: function (error) {
                            console.log('search error',error);
                        },
            });
    	});

    	$('#show_phone').on('click', function(){
    		let data = {
                        'action': 'show_phone',
                    };
            let $this = $(this);
            $.ajax({
                        url: vars_ajax.ajax_url,
                        data: data,
                        type: 'POST',
                        success: function (resonse) {
                            $this.parent().children('.contacts-list__text').text(resonse);
                        },
                        error: function (error) {
                            console.log('search error',error);
                        },
            });
    	});
    }


   		function select_theme(){
   			$('.contact-form').on('click','.select-items>div',function(){
   				$('#form input[name=theme]').val($(this).text())
   			});
   		}

  })( jQuery );