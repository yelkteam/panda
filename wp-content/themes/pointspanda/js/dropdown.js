"use strict";

function initDropDownList(id, itemClass, activeClass) {
  var list = document.getElementById(id);
  var toggleItem = list.querySelector("." + activeClass);
  toggleItem.addEventListener('click', function () {
    var items = list.querySelectorAll("." + itemClass + ":not(." + activeClass + ")");
    items.forEach(function (el) {
      el.classList.toggle('dropdown-item');
    });
    list.classList.toggle('opened-dropdown-list');
  });
}

function initAbsoluteDropDownList(id, itemClass, activeClass) {
  var list = document.getElementById(id);

  if (!list) {
    return;
  }

  var toggleItem = list.querySelector("." + activeClass);

  toggleItem.addEventListener('click', function () {
    list.classList.toggle('opened-dropdown-list');
  });

  list.addEventListener('click', function (e) {
    e.stopPropagation();
  });

  document.addEventListener('click', function (e) {
    if (e.target !== list) {
      list.classList.remove('opened-dropdown-list');
    }
  });
}
