initAccordion('conciergeFAQAccordion');

	new Swiper('#reviewVideosSlider', {
		slidesPerView: 1,
		spaceBetween: 20,
		loop: true,
		navigation: {
			nextEl: '.base-arrows-slider-box__next-arrow',
			prevEl: '.base-arrows-slider-box__prev-arrow',
		},
		pagination: {
			el: '.review-videos-slider__dots',
			clickable: true,
		},
		breakpoints: {
			600: {
				slidesPerView: 2,
			},
			768: {
				slidesPerView: 3,
			}
		}
	});
