var isSlidersStart = false;
var creditCardsSlider = null;

function startBlogPageSliders() {
  function start() {
    creditCardsSlider = startSlider('creditCardsSlider', 5000, 'creditCardsSliderControls', true);
    isSlidersStart = true;
  }

  function stopSliders() {
    stopSlider(creditCardsSlider);
    isSlidersStart = false;
  }

  function onResize() {
    var clientWidth = document.body.clientWidth;

    if (clientWidth <= 600 && !isSlidersStart) {
      start();
    } else if (clientWidth > 600 && isSlidersStart) {
      stopSliders();
    }
  }

  if (document.documentElement.clientWidth <= 600) {
    start();
  }

  window.addEventListener('resize', onResize);
}

startBlogPageSliders();

new Swiper('#blogCardsArrowsSlider', {
  slidesPerView: 1,
  spaceBetween: 20,
  loop: true,
  navigation: {
    nextEl: '.base-arrows-slider-box__next-arrow',
    prevEl: '.base-arrows-slider-box__prev-arrow',
  },
  pagination: {
    el: '.blog-cards-arrows-slider-box__dots',
    clickable: true,
  },
  breakpoints: {
    600: {
      slidesPerView: 2,
    },
    768: {
      slidesPerView: 3,
    }
  }
});

new Swiper('.blog-post-carousel-container', {
  slidesPerView: 1,
  spaceBetween: 20,
  loop: true,
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },
});

initAutoResizeTextarea('messageTextarea');
	validateForm('commentform');

"use strict";

function isElementInViewport(el, offsetTop) {
  if (offsetTop === void 0) {
    offsetTop = 0;
  }

  var rect = el.getBoundingClientRect();
  var windowHeight = window.innerHeight || document.documentElement.clientHeight;
  var windowWidth = window.innerWidth || document.documentElement.clientWidth;
  var vertInView = rect.top <= windowHeight && rect.top + rect.height >= offsetTop;
  var horInView = rect.left <= windowWidth && rect.left + rect.width >= 0;
  return vertInView && horInView;
}

function initFixedSidebarBlock(selector) {
  // var el = document.querySelector(selector);
  //
  // if (!el) {
  //   return;
  // }
  //
  // var copy = el.cloneNode(true);
  // copy.classList.add('transparent');
  // copy.classList.add('blog-post-sidebar-social-box--copy');
  // copy = el.parentNode.appendChild(copy);
  // window.addEventListener('scroll', function () {
  //   if (window.innerWidth <= 768) {
  //     copy.classList.add('transparent');
  //     return;
  //   }
  //
  //   if (window.pageYOffset < el.parentNode.offsetTop || isElementInViewport(el, 50) || window.pageYOffset >= el.parentNode.offsetHeight + el.parentNode.offsetTop - copy.offsetHeight - 180) {
  //     copy.classList.add('transparent');
  //   } else {
  //     copy.classList.remove('transparent');
  //   }
  // });
}

initFixedSidebarBlock('.blog-post-sidebar-social-box');

function initCollapsedSidebar(selector) {
  var sidebar = document.querySelector(selector);
  if (!sidebar) {
    return;
  }

  var btn = sidebar.querySelector('.collapsed-subscribe-box__toggle-btn');

  btn.addEventListener('click', function () {
    sidebar.classList.toggle('collapsed-subscribe-box--opened');
  });
}

initCollapsedSidebar('.aside-collapsed-subscribe-box');
initCollapsedSidebar('.top-collapsed-subscribe-box');
