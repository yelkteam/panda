"use strict";

function toggleMenu(btnId, menuId) {
  var btn = document.getElementById(btnId);
  btn.addEventListener('click', function () {
    var menu = document.getElementById(menuId);

    var _window$getComputedSt = window.getComputedStyle(menu),
      display = _window$getComputedSt.display;

    if (display === 'none') {
      menu.style.display = 'block';
    } else {
      menu.style.display = 'none';
      menu.style.display = '';
    }
  });
}

function closeMenu(btnId, menuId) {
  var btn = document.getElementById(btnId);
  btn.addEventListener('click', function () {
    var menu = document.getElementById(menuId);
    menu.style.display = 'none';
    menu.style.display = '';
  });
}
