"use strict";



function startSlider(id, time, controlsId, disableOnDesktop) {
  if (time === void 0) {
    time = 2000;
  }

  if (controlsId === void 0) {
    controlsId = '';
  }

  if (disableOnDesktop === void 0) {
    disableOnDesktop = false;
  }

  var slider = document.getElementById(id);

  if (!slider) {
    return
  }

  var items = slider.querySelectorAll('.slide');
  var index = 0;
  var controls = document.getElementById(controlsId);

  if (controls) {
    var onDotClick = function onDotClick(_ref) {
      var slideNumber = _ref.target.dataset.index;
      slideNumber = +slideNumber;
      setSlide(slideNumber);
      window.clearInterval(timer);
      timer = setInterval(setSlide, time);
    };

    for (var i = 0; i < items.length; i += 1) {
      var div = document.createElement('div');
      controls.appendChild(div);
    }

    controls.firstChild.classList.add('active');
    var dots = controls.childNodes;
    for (var i = 0; i < dots.length; i += 1) {
      dots[i].dataset.index = i.toString();
      dots[i].addEventListener('click', onDotClick);
    }
  }

  for (var i = 0; i < items.length; i += 1) {
    items[i].style.transition = 'opacity 1s linear';
  }

  items[0].classList.add('current');

  function setSlide(newIndex) {
    if (newIndex === void 0) {
      newIndex = false;
    }

    items[index].classList.remove('current');

    if (controls) {
      for (var i = 0; i < dots.length; i += 1) {
        dots[i].classList.remove('active');
      }
    }

    if (newIndex !== false) {
      index = newIndex;
    } else {
      index = items[index + 1] ? index + 1 : 0;
    }

    items[index].classList.add('current');

    if (controls) {
      controls.childNodes[index].classList.add('active');
    }
  }

  var timer = setInterval(setSlide, time);

  if (disableOnDesktop) {
    window.addEventListener('resize', function () {
      if (document.body.clientWidth > 600) {
        clearInterval(timer);
      }
    });
  }

  return {
    timer: timer,
    controls: controls
  };
}

function stopSlider(sliderInstance) {
  window.clearInterval(sliderInstance.timer);
  sliderInstance.controls.innerHTML = '';
}


