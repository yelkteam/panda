"use strict";

function initModal(id, afterClose) {
	if (typeof afterClose !== 'function') {
		afterClose = function (){}
	}
	var modal = document.getElementById(id);
	var closeBtn = modal.querySelector('.modal__close-btn');
	modal.addEventListener('click', function (_ref) {
		var target = _ref.target;

		if (target === modal) {
			modal.style.display = 'none';
			afterClose();
		}
	});
	closeBtn.addEventListener('click', function (_ref2) {
		var currentTarget = _ref2.currentTarget;

		if (currentTarget === closeBtn) {
			modal.style.display = 'none';
			afterClose();
		}
	});

	function open() {
		modal.style.display = 'flex';
	}

	return open;
}

function initBaseModal(selector, onClose, btnSelector) {
	if (onClose === void 0) {
		onClose = function onClose() {};
	}

	var modal = document.querySelector(selector);
	var btn = document.querySelector(btnSelector);

	var open = function open() {
		return modal.classList.add('visible');
	};

	var close = function close() {
		onClose();
		return modal.classList.remove('visible');
	};

	if (modal) {
		if (btn) {
			btn.addEventListener('click', function () {
				return modal.classList.add('visible');
			});
		}

		var closeBtn = modal.querySelector('.base-modal__close-btn');
		var overlay = modal.querySelector('.base-modal__overlay');

		if (closeBtn) {
			closeBtn.addEventListener('click', close);
		}

		if (overlay) {
			overlay.addEventListener('click', function (_ref) {
				var target = _ref.target,
					currentTarget = _ref.currentTarget;

				if (target === currentTarget) {
					close();
				}
			});
		}
	}

	return {
		el: modal,
		open: open,
		close: close
	};
}

function initModalShowingRules(selector) {
	var modal = initBaseModal(selector, function () {
		eraseCookie('pageNumber');
		setCookie('disableSubscribePopup', 'true', 14);
	});

	if (getCookie('disableSubscribePopup') === 'true') {
		return;
	}

	var pageNumber = getCookie('pageNumber') || 0;
	setCookie('pageNumber', (+pageNumber + 1).toString());

	if (+getCookie('pageNumber') > 1) {
		setTimeout(function () {
			return modal.open();
		}, 15000);
	}
}
