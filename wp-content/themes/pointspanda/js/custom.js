(function ($) {

    $(document).ready(function () {
        live_search();
        load_more();
        breadcrumbs_back();
    });
    
    function live_search(){
            
        $('#searchInput').on('keyup', function(e){
            
            let s = $(this).val();
            if(s.length > 3){
                let data = {
                        'action': 'live_search',
                        'search': s,
                    };

                $.ajax({
                        url: vars_ajax.ajax_url,
                        data: data,
                        type: 'POST',
                        success: function (resonse) {
                            $('#searchResults').html(resonse).addClass('visible');
                        },
                        error: function (error) {
                            console.log('search error',error);
                        },
                });
            }else{
                $('#searchResults').removeClass('visible');
            }
        });
    }

    function load_more(){
        $('#view-all-link-box a').on('click',function(e){
            e.preventDefault();

            let $parent = $(this).parent(); 

            let data = {
                'action': 'load_more_posts',
                'page': $parent.data('page'),
                'cat': $parent.data('cat'),
            };

            $.ajax({
                    url: vars_ajax.ajax_url,
                    data: data,
                    type: 'POST',
                    dataType: 'json',
                    success: function (resonse) {
                        if(resonse){
                            $('.main-blog-content .horizontal-articles-list').append(resonse.html);
                            if(resonse.has_more){
                                $parent.data('page',resonse.page);
                            }else{
                                $parent.hide();
                            }
                        }
                    },
                    error: function (error) {
                        console.log('search error',error);
                    },
            });

        });

        $('#view-all-author-link-box a').on('click',function(e){
            e.preventDefault();

            let $parent = $(this).parent(); 

            let data = {
                'action': 'load_more_posts',
                'page': $parent.data('page'),
                'auth': $parent.data('auth'),
            };
            
            $.ajax({
                    url: vars_ajax.ajax_url,
                    data: data,
                    type: 'POST',
                    dataType: 'json',
                    success: function (resonse) {
                        if(resonse){
                            $('.main-blog-content .horizontal-articles-list').append(resonse.html);
                            if(resonse.has_more){
                                $parent.data('page',resonse.page);
                            }else{
                                $parent.hide();
                            }
                        }
                    },
                    error: function (error) {
                        console.log('search error',error);
                    },
            });

        });
    }

    function breadcrumbs_back(){
        $('.breadcrumbs__item--back a').on('click',function(e){
            e.preventDefault();
            window.history.back();
        });
    }

  })( jQuery );