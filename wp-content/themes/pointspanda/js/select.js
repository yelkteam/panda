function initSelect(id) {
  const selectWrapper = document.getElementById(id);
  const select = selectWrapper.querySelector('select');

  const selectHeader = document.createElement('div');
  selectHeader.classList.add('select-header');
  selectHeader.innerHTML = select.options[select.selectedIndex].innerHTML;

  selectWrapper.appendChild(selectHeader);

  const selectItems = document.createElement('div');
  selectItems.classList.add('select-items', 'select-hide');

  for (let i = 0; i < select.length; i += 1) {
    const item = document.createElement('div');

    item.innerHTML = select.options[i].innerHTML;
    item.addEventListener('click', () => {
      for (let i = 0; i < select.length; i += 1) {
        if (select.options[i].innerHTML === item.innerHTML) {
          select.selectedIndex = i;
          selectHeader.innerHTML = item.innerHTML;

          const selectedItem = item.parentNode.querySelector('.selected-item');
          selectedItem.classList.remove('.selected-item');
          item.classList.add('.selected-item');

          break;
        }
      }
    });

    selectItems.appendChild(item);
  }

  selectWrapper.appendChild(selectItems);

  selectHeader.addEventListener('click', () => {
    selectHeader.nextSibling.classList.toggle('select-hide');
    selectHeader.classList.toggle('select-arrow-active');
  });
}