  "use strict";

  var isSlidersStart = false;
  var topArticlesSlider = null;
  var creditCardsSlider = null;

  function startBlogPageSliders() {
    function start() {
      topArticlesSlider = startSlider('topArticlesSlider', 5000, 'topArticlesSliderControls', 'top-article-card__title', true);
      creditCardsSlider = startSlider('creditCardsSlider', 5000, 'creditCardsSliderControls', true);
      isSlidersStart = true;
    }

    function showOnlyFirstSlide(type) {
      if (type === void 0) {
        type = true;
      }

      var slides = document.querySelectorAll('#topArticlesSlider li');
      for (var i = 0; i < slides.length; i += 1) {
        if (i === 0) {
          slides[i].classList.add('current');
        } else {
          slides[i].classList.remove('current');
        }
      }
    }

    function stopSliders() {
      stopSlider(topArticlesSlider);
      stopSlider(creditCardsSlider);
      showOnlyFirstSlide();
      isSlidersStart = false;
    }

    function onResize() {
      var clientWidth = document.body.clientWidth;

      if (clientWidth <= 600 && !isSlidersStart) {
        start();
      } else if (clientWidth > 600 && isSlidersStart) {
        stopSliders();
      }
    }

    if (document.documentElement.clientWidth <= 600) {
      start();
    } else {
      showOnlyFirstSlide();
    }

    window.addEventListener('resize', onResize);
  }

  startBlogPageSliders();
  initAbsoluteDropDownList('categoriesDropdownList', 'categories-dropdown-list__items li', 'categories-dropdown-list__selected-item');