"use strict";

function initSearch(btnId, hideMenuId, formId) {
  var btn = document.getElementById(btnId);
  var menu = document.getElementById(hideMenuId);
  var form = document.getElementById(formId);
  var input = form.querySelector('input');
  var closeBtn = form.querySelector('button');
  var searchResultsBlock = form.querySelector('#searchResults');
  var menuToggleBtn = document.getElementById('menuToggle');
  var logo = document.getElementById('logo');
  var header = document.querySelector('#mainHeader .main-header.wrapper');
  var isFormOpened = false;

  function openForm() {
    if (!isFormOpened) {
      var offsetRight = 0;
      var offsetLeft = 0;
      input.style.color = '#1b1b32';
      menu.style.opacity = '0';

      if (document.documentElement.clientWidth > 600) {
        input.style.paddingRight = '20px';
        offsetRight = document.getElementById('searchBox').offsetWidth + 36;
        offsetLeft = logo.offsetWidth + 41;
        form.style.bottom = "7px";
      } else {
        offsetLeft = 24;
        offsetRight = 24;
        logo.style.opacity = '0';
        menuToggleBtn.style.width = '0';
        menuToggleBtn.style.marginLeft = '0';
        closeBtn.style.right = closeBtn.offsetWidth + btn.offsetWidth + 10 + "px";
        input.style.paddingRight = "calc(" + (btn.offsetWidth + closeBtn.offsetWidth) + "px * 1.75)";
        form.style.bottom = document.getElementById('mainHeader').classList.contains('sticky-top') ? '-5px' : '17px';
      }

      form.style.right = offsetRight + "px";
      form.style.width = header.offsetWidth - offsetLeft - offsetRight + 'px';
      form.style.visibility = 'visible';
      closeBtn.style.display = 'block';
      form.addEventListener('transitionend', afterFormOpen);
    }
  }

  function closeForm(_ref) {
    var target = _ref.target;

    if (isFormOpened && target.id !== formId && target.id !== input.id && target.id !== btnId) {
      searchResultsBlock.style.visibility = 'hidden';
      form.style.width = '0';
      input.style.padding = '0';
      input.style.color = 'transparent';
      input.placeholder = '';
      closeBtn.style.display = 'none';
      closeBtn.style.right = '0';
      menuToggleBtn.style.width = '40px';
      menuToggleBtn.style.marginLeft = '27px';
      logo.style.opacity = '1';
      form.addEventListener('transitionend', afterFormClose);
    }
  }

  function afterFormOpen() {
    input.placeholder = 'Search';
    isFormOpened = true;
    form.removeEventListener('transitionend', afterFormOpen);
  }

  function afterFormClose() {
    menu.style.opacity = '1';
    setTimeout(function () {
      searchResultsBlock.style.visibility = 'visible';
    }, 1000);
    searchResultsBlock.classList.remove('visible');
    isFormOpened = false;
    form.removeEventListener('transitionend', afterFormClose);
  }

  btn.addEventListener('click', openForm);
  document.addEventListener('scroll', closeForm);
  document.addEventListener('click', closeForm);
  // input.addEventListener('input', function () {
  //   searchResultsBlock.classList.add('visible');
  // });
  window.addEventListener('orientationchange', closeForm);
}
