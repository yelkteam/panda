(function() {
    tinymce.PluginManager.add( 'mark_text_btn', function( editor, url ) {
        editor.addButton('mark_text_btn', {
            title: 'Mark text',
            cmd: 'mark_text_btn',
            icon: 'backcolor',
        }); 
        editor.addCommand('mark_text_btn', function() {

            selected = tinyMCE.activeEditor.selection.getContent();

            if( selected ){
                //If text is selected when button is clicked
                //Wrap shortcode around it.
                content =  '[mark_text]'+selected+'[/mark_text]';
            }else{
                content =  '[mark_text][/mark_text]';
            }

            tinyMCE.activeEditor.selection.setContent(content);

        });
    });

})();