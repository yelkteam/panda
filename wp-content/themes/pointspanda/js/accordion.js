"use strict";

function initAccordion(id) {
	var accordion = document.getElementById(id);
	var items = accordion.querySelectorAll('.accordion__item');

	for (var i = 0; i < items.length; i += 1) {
		var itemHeader = items[i].querySelector('.accordion__header');

		if (itemHeader) {
			itemHeader.addEventListener('click', function (i) {
				var expandedClass = 'accordion__expanded-item';
				var activeItem = accordion.querySelector("." + expandedClass);

				if (!items[i].classList.contains(expandedClass) && activeItem) {
					activeItem.classList.remove(expandedClass);
				}

				items[i].classList.toggle(expandedClass);
			}.bind(null, i));
		}
	}
}
