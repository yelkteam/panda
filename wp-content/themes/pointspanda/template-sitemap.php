<?php 
	// Template Name: Sitemap
?>
<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

    <main class="sitemap-main-content">
      <div class="wrapper">
        <section class="breadcrumbs-box">
          <?php panda_breadcrumbs(); ?>
        </section>
        <section class="sitemap-box">
          <?php the_title('<h1>','</h1>'); ?>
            <ul class="sitemap-list">
            <?php 
                $categories = get_terms( 'card-category' );
                if($categories){
                    ?>
                    <?php 
                
                    foreach ($categories as $category) {
                        ?>
                            <li class="sitemap-list__item">
                                <a href="<?=get_category_link( $category->term_id)?>"><?=$category->name?></a>
                        <?php 
                                    
                        $pages = get_posts(
                                                        array(
                                                                'numberposts' => -1,
                                                                'post_type' => 'cards',
                                                                'orderby' => 'title',
                                                                'order' => 'ASC',
                                                                'tax_query' => array(
                                                                                        'taxonomy' => 'card-category',
                                                                                        'field' => 'id',
                                                                                        'terms' => $category->term_id,
                                                                                ),
                                                            )
                                                    );
                        if($pages){
                            ?>
                            <ul class="sitemap-list--nested">
                            <?php 
                                foreach ($pages as $page) {
                                    ?>
                                        <li class="sitemap-list__item--nested">
                                            <a href="<?=get_the_permalink($page->ID)?>"><?=$page->post_title?></a>
                                        </li>
                                    <?php
                                }
                            ?>
                            </ul>
                            <?php 
                        }
                        ?>
                            </li>
                        <?php 
                    }

                }
            ?>

            <?php 
                $categories = get_categories();
                if($categories){
                    ?>
                    <?php 
                    foreach ($categories as $category) {
                        ?>
                            <li class="sitemap-list__item">
                                <a href="<?=get_category_link( $category->term_id)?>"><?=$category->name?></a>
                        <?php
                                
                        $pages = get_posts(array('numberposts' => -1, 'post_type' => 'post','orderby' => 'title','order' => 'ASC', 'category' => $category->term_id ));
                        if($pages){
                            ?>
                                <ul class="sitemap-list--nested">
                            <?php 
                            foreach ($pages as $page) {
                                ?>
                                    <li class="sitemap-list__item--nested">
                                        <a href="<?=get_the_permalink($page->ID)?>"><?=$page->post_title?></a>
                                    </li>
                                <?php 
                            }
                            ?>
                            </ul>
                            <?php 
                        }
                        ?>
                            </li>
                        <?php 
                    }
                    
                }
                
                ?>

                <?php
                
                $pages = get_posts(array('numberposts' => -1, 'post_type' => 'page','orderby' => 'title', 'order' => 'ASC','exclude' => array(get_the_ID()) ));
                if($pages){

                    ?>
                        <?php foreach ($pages as $page) { ?>
                                <li class="sitemap-list__item">
                                    <a href="<?=get_the_permalink($page->ID)?>"><?=$page->post_title?></a>
                                </li>
                        <?php  } ?>
                <?php  } ?>
            </ul>
        </section>
      </div>
    </main>

<?php endwhile; ?>

<?php get_footer(); ?>