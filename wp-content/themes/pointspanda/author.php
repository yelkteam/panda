<?php get_header();?>
<?php
    $ccitems =  carbon_get_theme_option( 'top_ccdeals' );
    $cctitle = carbon_get_theme_option( 'top_ccdealstitle' );

    $banner_img = carbon_get_theme_option( 'sbanner_img' );
    $banner_img1 = carbon_get_theme_option( 'sbanner_img1' );
    $banner_url = carbon_get_theme_option( 'sbanner_url' );

    $curauth = get_query_var('author_name');
    $placeholder = carbon_get_theme_option( 'defimage' );

    $top_category = carbon_get_the_post_meta('top_last_posts');
    $popular_category = carbon_get_the_post_meta('top_last_posts');

    $authQuery = new WP_Query(array(
                                'posts_per_page' => -1,
                                'post_type'   => 'post',
                                'post_status'=>'publish',
                                'author_name' => $curauth,
                                'fields' => 'ids',
                                'no_found_rows' => true,
                            ));

    $published_posts = $authQuery->post_count;

    $default_posts_per_page = get_option( 'posts_per_page' );

    $cards_page = carbon_get_theme_option( 'ccpage' );

    $blog_subscr_title = carbon_get_post_meta(get_option( 'page_for_posts' ),'blog_subscr_title');
    $blog_subscr_subtitle = carbon_get_post_meta(get_option( 'page_for_posts' ),'blog_subscr_subtitle');
    $blog_subscr_image = carbon_get_post_meta(get_option( 'page_for_posts' ),'blog_subscr_image');
?>

<main class="blog-main-page-main-content author-blog-main-page-main-content">
	<div class="wrapper">
        <section class="breadcrumbs-box">
            <?php panda_breadcrumbs(); ?>
		</section>
		<section class="top-articles-box">
			<div class="top-articles-sidebar">
				<div class="top-articles-sidebar__header">
					<h4><?=__('Top articles','panda')?></h4>
					<svg width="18" height="24" viewBox="0 0 18 24" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M17.9499 14.0685C17.9491 14.0572 17.9483 14.0459 17.9474 14.0346C17.6685 10.4652 16.005 8.22618 14.5371 6.24998C13.1758 4.41754 12 2.83514 12 0.500856C12 0.313356 11.895 0.141981 11.7285 0.0560595C11.5615 -0.030378 11.3608 -0.0162217 11.209 0.094122C9.00098 1.67409 7.1587 4.33701 6.51516 6.87783C6.47138 7.05117 6.43132 7.22663 6.39466 7.40335C5.91577 9.7124 3.83739 10.7632 3.49514 8.42995C3.47217 8.27175 3.37552 8.13408 3.23489 8.05889C3.09281 7.98468 2.9258 7.97929 2.78175 8.05059C2.67483 8.10234 0.157219 9.38156 0.0107342 14.4891C0.000468576 14.659 0 14.8295 0 14.9999C0 19.9619 4.03758 23.9992 9 23.9992C9.00456 23.9996 9.00933 24.0001 9.01375 24C9.01795 23.9999 9.02214 23.9992 9.02634 23.9992C13.9766 23.985 18 19.9531 18 14.9999C18 14.7733 17.9577 14.1763 17.9499 14.0685ZM9 22.9993C7.34569 22.9993 6 21.5658 6 19.8037C6 19.7437 5.99953 19.6831 6.00389 19.6089C6.02118 18.967 6.83723 18.932 7.44655 19.1346C7.60537 19.1874 7.77972 19.2169 7.97072 19.2169C8.24709 19.2169 8.47073 18.9932 8.47073 18.7169C8.47073 18.0051 8.48541 17.1838 8.66264 16.4426C8.66436 16.4355 8.6661 16.4283 8.66787 16.4211C8.89063 15.519 10.171 15.6567 10.7056 16.4167C11.2843 17.2389 11.8824 18.089 11.9874 19.5386C11.9937 19.6245 12.0001 19.711 12.0001 19.8037C12 21.5658 10.6543 22.9993 9 22.9993Z" fill="#DD516B" fill-opacity="0.9"/>
					</svg>
				</div>
				<div class="side-last-news-list">
                    <?php
                        $latest_posts = get_latest_author_posts($top_category, $curauth, 6);

                        if($latest_posts->have_posts()){
                    ?>
                        <ul>
                            <?php while ( $latest_posts->have_posts() ) { $latest_posts->the_post(); ?>
                                <li class="side-last-news-list__item">
                                    <div class="side-last-news-list__img-box">
                                        <?php
                                            if(has_post_thumbnail()){
                                                echo get_the_post_thumbnail(null,'icon');
                                            }else{
                                                echo wp_get_attachment_image($placeholder,'icon');
                                            }
                                        ?>
                                    </div>
                                    <a href="<?=get_the_permalink()?>"><?=get_the_title()?></a>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } wp_reset_postdata(); ?>
				</div>
            </div>
            <?php
                $popular = get_most_popular_author_posts($popular_category, $curauth, 4);
            ?>
			<div class="top-articles-slider-box">
                <?php if($popular->have_posts()){ ?>
                    <ul id="topArticlesSlider" class="top-articles-slider">
                        <?php while ( $latest_posts->have_posts() ) { $latest_posts->the_post(); ?>
                            <li class="slide top-article-card">
                                <a href="<?=get_the_permalink()?>">
                                    <div class="top-article-card__img-box">
                                        <?php
                                            if(has_post_thumbnail()){
                                                echo get_the_post_thumbnail(null,'large');
                                            }else{
                                                echo wp_get_attachment_image($placeholder,'large');
                                            }
                                        ?>
    								</div>
    								<?php

    									$posted = human_time_diff(get_the_time('U'));
    								?>
    								<div class="top-article-card__tag"><?=__('Trending','panda')?></div>
                                    <h2 class="top-article-card__title"><?=get_the_title()?></h2>
                                    <span class="top-article-card__view-all-link">
                                        <svg width="56" height="56" viewBox="0 0 56 56" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <circle cx="28" cy="28" r="27.5" stroke="white"/>
                                            <path d="M18 27.4C17.6686 27.4 17.4 27.6686 17.4 28C17.4 28.3314 17.6686 28.6 18 28.6V27.4ZM38.4243 28.4243C38.6586 28.1899 38.6586 27.8101 38.4243 27.5757L34.6059 23.7574C34.3716 23.523 33.9917 23.523 33.7574 23.7574C33.523 23.9917 33.523 24.3716 33.7574 24.6059L37.1515 28L33.7574 31.3941C33.523 31.6284 33.523 32.0083 33.7574 32.2426C33.9917 32.477 34.3716 32.477 34.6059 32.2426L38.4243 28.4243ZM18 28.6H38V27.4H18V28.6Z" fill="white"/>
                                        </svg>
                                    </span>
    								<div class="top-article-card__date"><?=get_the_date('F d, Y')?></div>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                    <div id="topArticlesSliderControls" class="slider-controls top-articles-slider__controls"></div>
                <?php } wp_reset_postdata(); ?>
			</div>
		</section>
		<section class="main-blog-content">
			<section class="filtered-articles-box">
                <div class="post-author-card">
					<div class="post-author-card__avatar-box">
						<?=get_avatar( get_the_author_email())?>
					</div>
					<div class="post-author-card__text-box">
						<h4><?=get_the_author()?></h4>
                        <p><?php the_author_meta('description');?></p>
                        <?php
                                    $twitter = get_the_author_meta( 'twitter' );
                                    $facebook = get_the_author_meta( 'facebook' );
                                    $linkedin = get_the_author_meta( 'linkedin' );
                                    $instagram = get_the_author_meta( 'inst_url' );
                                    $reddit = get_the_author_meta( 'reddit_url' );
                                ?>
                                <ul class="dense-social-list post-author-card__social-list">
                                    <?php if($linkedin):?>
                                    <li class="dense-social-list__item dense-social-list__item-linked-in">
                                        <a href="<?=$linkedin;?>">
                                            <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M3.17811 14V4.55422H0.177061V14H3.17811ZM1.67797 3.26379C2.72449 3.26379 3.3759 2.53846 3.3759 1.63205C3.3564 0.705193 2.72453 0 1.69783 0C0.671294 0 0 0.705207 0 1.63205C0 2.53851 0.651247 3.26379 1.65838 3.26379H1.67788H1.67797ZM4.83918 14H7.84023V8.72502C7.84023 8.44271 7.85973 8.16069 7.93898 7.95888C8.15593 7.39483 8.64972 6.81064 9.47874 6.81064C10.5647 6.81064 10.9991 7.67685 10.9991 8.94665V13.9999H14V8.58381C14 5.68244 12.5194 4.33246 10.5449 4.33246C8.92599 4.33246 8.21516 5.27917 7.82027 5.92397H7.8403V4.55403H4.83925C4.87863 5.44037 4.83925 13.9998 4.83925 13.9998L4.83918 14Z" fill="#000"></path>
                                            </svg>
                                        </a>
                                    </li>
                                    <?php endif;?>
                                    <?php if($facebook):?>
                                    <li class="dense-social-list__item dense-social-list__item-facebook">
                                        <a href="<?=$facebook;?>" rel="nofollow" target="_blank" >
                                            <svg width="9" height="16" viewBox="0 0 9 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M8.0075 8.9995L8.452 6.104H5.6735V4.225C5.6735 3.433 6.0615 2.6605 7.306 2.6605H8.569V0.1955C8.569 0.1955 7.423 0 6.327 0C4.039 0 2.5435 1.387 2.5435 3.8975V6.1045H0V9H2.5435V16H5.6735V9L8.0075 8.9995Z" fill="#000"></path>
                                            </svg>
                                        </a>
                                    </li>
                                    <?php endif;?>
                                    <?php if($twitter):?>
                                    <li class="dense-social-list__item dense-social-list__item-twitter">
                                        <a href="<?=$twitter;?>" rel="nofollow" target="_blank" >
                                            <svg width="16" height="13" viewBox="0 0 16 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M16 1.54405C15.4116 1.79787 14.7713 1.98054 14.1118 2.05168C14.7965 1.64488 15.3093 1.00219 15.5539 0.244209C14.9113 0.62645 14.2074 0.894484 13.4734 1.03642C13.1666 0.708435 12.7955 0.447146 12.3833 0.268836C11.9711 0.090526 11.5266 -0.000984081 11.0775 7.98058e-06C9.26043 7.98058e-06 7.79906 1.47291 7.79906 3.28038C7.79906 3.5342 7.82983 3.78801 7.87982 4.03222C5.159 3.88993 2.73236 2.59008 1.1191 0.599936C0.825142 1.10202 0.671099 1.6737 0.672996 2.25551C0.672996 3.39383 1.25177 4.39756 2.13436 4.98787C1.61424 4.96739 1.1063 4.82442 0.651845 4.57061V4.61099C0.651845 6.20503 1.77863 7.52603 3.28038 7.82984C2.99841 7.90308 2.70834 7.94055 2.41702 7.94136C2.20358 7.94136 2.00168 7.92021 1.79786 7.89137C2.2132 9.19121 3.42267 10.1353 4.86288 10.1661C3.73609 11.0487 2.32472 11.5678 0.792213 11.5678C0.517246 11.5678 0.26343 11.5582 0 11.5275C1.45367 12.46 3.17846 12.9984 5.03593 12.9984C11.066 12.9984 14.3656 8.00289 14.3656 3.66688C14.3656 3.52458 14.3656 3.38229 14.356 3.24C14.9944 2.77275 15.5539 2.19397 16 1.54405Z" fill="#000"></path>
                                            </svg>
                                        </a>
                                    </li>
                                    <?php endif;?>
                                    <?php if($instagram):?>
                                    <li class="dense-social-list__item dense-social-list__item-instagram">
                                        <a href="<?=$instagram;?>" rel="nofollow" target="_blank" >
                                            <svg id="Bold" enable-background="new 0 0 24 24" viewBox="0 0 24 24" width="17" xmlns="http://www.w3.org/2000/svg"><path d="m12.004 5.838c-3.403 0-6.158 2.758-6.158 6.158 0 3.403 2.758 6.158 6.158 6.158 3.403 0 6.158-2.758 6.158-6.158 0-3.403-2.758-6.158-6.158-6.158zm0 10.155c-2.209 0-3.997-1.789-3.997-3.997s1.789-3.997 3.997-3.997 3.997 1.789 3.997 3.997c.001 2.208-1.788 3.997-3.997 3.997z"/><path d="m16.948.076c-2.208-.103-7.677-.098-9.887 0-1.942.091-3.655.56-5.036 1.941-2.308 2.308-2.013 5.418-2.013 9.979 0 4.668-.26 7.706 2.013 9.979 2.317 2.316 5.472 2.013 9.979 2.013 4.624 0 6.22.003 7.855-.63 2.223-.863 3.901-2.85 4.065-6.419.104-2.209.098-7.677 0-9.887-.198-4.213-2.459-6.768-6.976-6.976zm3.495 20.372c-1.513 1.513-3.612 1.378-8.468 1.378-5 0-7.005.074-8.468-1.393-1.685-1.677-1.38-4.37-1.38-8.453 0-5.525-.567-9.504 4.978-9.788 1.274-.045 1.649-.06 4.856-.06l.045.03c5.329 0 9.51-.558 9.761 4.986.057 1.265.07 1.645.07 4.847-.001 4.942.093 6.959-1.394 8.453z"/><circle cx="18.406" cy="5.595" r="1.439"/></svg>
                                        </a>
                                    </li>
                                    <?php endif;?>
                                    <?php if($reddit):?>
                                        <li class="dense-social-list__item dense-social-list__item-reddit">
                                        <a href="<?=$reddit;?>" rel="nofollow" target="_blank" >
                                            <svg height="16" viewBox="0 0 24 24" width="16" xmlns="http://www.w3.org/2000/svg"><path d="m17.631 2.782c.034 1.32.952 2.631 2.685 2.631 1.621 0 2.691-1.216 2.691-2.706 0-1.345-.925-2.707-2.692-2.707-.958 0-1.836.428-2.335 1.321l-4.297-1.289c-.414-.123-.835.123-.941.527l-1.923 7.31c-2.236.16-4.278.777-5.951 1.709-2.77-2.595-6.557 1.633-4.091 4.562-1.774 5.542 4.594 9.86 11.223 9.86 6.681 0 12.974-4.39 11.223-9.86 2.458-2.919-1.301-7.154-4.095-4.564-1.87-1.04-4.206-1.678-6.75-1.736l1.617-6.148zm2.684-1.282c.879 0 1.191.65 1.191 1.208 0 .667-.409 1.206-1.191 1.206-.88 0-1.191-.65-1.191-1.208 0-.674.412-1.206 1.191-1.206zm-16.784 8.955c-.793.607-1.455 1.297-1.957 2.052-.38-1.412.839-2.632 1.957-2.052zm8.469 12.045c-8.943 0-13.396-7.182-6.982-11.298.422-.194 2.664-1.876 6.982-1.876 3.756 0 5.474 1.184 6.98 1.875 6.39 4.099 2.018 11.299-6.98 11.299zm10.426-9.992c-.502-.756-1.164-1.446-1.958-2.053 1.141-.589 2.335.654 1.958 2.053z"/><path d="m10.305 14.935c0-1.321-.998-2.206-2.217-2.206-1.076 0-2.219.773-2.219 2.206 0 1.32.996 2.206 2.217 2.206 1.076 0 2.219-.773 2.219-2.206zm-2.936 0c0-.655.55-.706.719-.706.403 0 .717.229.717.706 0 .655-.55.706-.719.706-.168 0-.717-.051-.717-.706z"/><path d="m15.914 12.729c-1.076 0-2.219.773-2.219 2.206 0 1.32.996 2.206 2.217 2.206 1.076 0 2.219-.773 2.219-2.206 0-1.321-.998-2.206-2.217-2.206zm-.002 2.912c-.168 0-.717-.051-.717-.706s.55-.706.719-.706c.403 0 .717.229.717.706 0 .655-.55.706-.719.706z"/><path d="m15.297 18.42c-1.106 1.517-5.454 1.536-6.593.001-.56-.812-1.786.023-1.241.842 1.591 2.402 7.483 2.403 9.074 0 .536-.803-.667-1.64-1.24-.843z"/></svg>
                                        </a>
                                    </li>
                                    <?php endif;?>
                                </ul>
					</div>
				</div>
				<section class="articles-filters-box">
                    <?php
                        $quoteText = '';
                        if(!is_front_page() && is_home()):
                            $quoteText = apply_filters('the_content',carbon_get_post_meta(get_option( 'page_for_posts' ),'blog_quote'));
                        else:
                            $quoteText = category_description();
                        endif;

                        if($quoteText){
                    ?>
                        <div class="blog-blockquote">
                            <div class="blog-blockquote__img-box">
                                <img src="<?php echo get_stylesheet_directory_uri();?>/img/blog-main-page/blog-blockquote.png" alt="">
                            </div>
                            <?=$quoteText?>
                        </div>
                    <?php } ?>
				</section>
				<div class="horizontal-articles-list">
                    <?php
                        $i = 0;
                        while ( have_posts() ) : the_post();

                            if($i%3 == 2 ){
                                get_template_part( 'template-parts/list_article','nodescription' );
                            }else{
                                get_template_part( 'template-parts/list_article','description' );
                            }
                            $i++;

                        endwhile;
                    ?>
                </div>
                <?php if($published_posts > $default_posts_per_page){ ?>
                    <div class="view-all-link-box" id="view-all-author-link-box" data-page="1" data-auth="<?=($curauth)?$curauth:0?>" >
                        <a href="#">
                            <span class="view-all-link-box__arrow"></span>
                            <span class="view-all-link-box__text"><?=__('More Articles','panda')?></span>
                        </a>
                    </div>
                <?php } ?>
			</section>
			<section class="credit-cards-sidebar">
                <?php if($ccitems){ ?>
                    <div class="credit-cards-sidebar__header">
                        <h4><?=$cctitle?></h4>
                        <?php if(isset($cards_page[0]['id'])){ ?>
                            <a href="<?=get_the_permalink($cards_page[0]['id'])?>"><?=__('See all','panda')?></a>
                        <?php } ?>
                    </div>
                    <ul class="credit-card-box-vertical-list">
                        <?php
                            foreach($ccitems as $i => $item){
                                $clink = carbon_get_post_meta($item['id'], 'card_url');
								if ( empty($clink) ) {
									$clink = '#';
								}
                            ?>
                            <li class="credit-card-box__item">
                                <a class="credit-card-box__card-link" href="<?=$clink?>" rel="noopener noreferrer">
                                    <div class="credit-card-box__img-box">
                                        <?php
                                            if(has_post_thumbnail($item['id'])){
                                                echo get_the_post_thumbnail($item['id'],'small_card');
                                            }else{
                                                echo wp_get_attachment_image($placeholder,'small_card');
                                            }
                                        ?>
                                    </div>
                                    <div class="credit-card-box__card-info">
                                        <?php if($i == 0){ ?>
											<div class="credit-card-box__subtitle"><?=__('Best Total Value','panda')?></div>
										<?php } ?>
                                        <div class="credit-card-box__title"><?=get_the_title($item['id'])?></div>
                                        <div class="credit-card-box__info">
                                        <?php
											$current = carbon_get_post_meta($item['id'],'current_offer');
											if($current){
												?>
												<span class="credit-card-box__mark-text"><?=__('Current offer:','panda')?></span> <?=$current?>
												<?php
											}else{
												$in_bonus = carbon_get_post_meta($item['id'],'text_offer');
												echo $in_bonus;
											}
										?>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                    <div id="creditCardsSlider" class="credit-card-box__header">
                        <h2><?=$cctitle?></h2>
                        <div class="credit-cards-slider">
                            <?php
                                foreach($ccitems as $i => $item){
                                    $clink = carbon_get_post_meta($item['id'], 'card_url');
                                    if ( empty($clink) ) {
                                        $clink = '#';
                                    }
                                ?>
                                <div class="slide credit-cards-slider__item credit-card-box__item">
                                    <a class="credit-card-box__card-link" href="<?=$clink?>" rel="noopener noreferrer">
                                        <div class="credit-card-box__img-box">
                                        <?php
                                            if(has_post_thumbnail($item['id'])){
                                                echo get_the_post_thumbnail($item['id'],'small_card');
                                            }else{
                                                echo wp_get_attachment_image($placeholder,'small_card');
                                            }
                                        ?>
                                        </div>
                                        <div class="credit-card-box__card-info">
                                            <?php if($i == 0){ ?>
                                                <div class="credit-card-box__subtitle"><?=__('Best Total Value','panda')?></div>
                                            <?php } ?>
                                            <div class="credit-card-box__title"><?=get_the_title($item['id'])?></div>
                                            <div class="credit-card-box__info">
                                                <?php
                                                    $current = carbon_get_post_meta($item['id'],'current_offer');
                                                    if($current){
                                                        ?>
                                                        <span class="credit-card-box__mark-text"><?=__('Current offer:','panda')?></span> <?=$current?>
                                                        <?php
                                                    }else{
                                                        $in_bonus = carbon_get_post_meta($item['id'],'text_offer');
                                                        echo $in_bonus;
                                                    }
                                                ?>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            <?php } ?>
                            <div id="creditCardsSliderControls" class="credit-cards-slider__controls"></div>
                        </div>
                        <?php if(isset($cards_page[0]['id'])){ ?>
                            <a class="credit-card-box__link--transparent" href="<?=get_the_permalink($cards_page[0]['id'])?>">
                                <span><?=__('See all','panda')?></span>
                                <svg width="42" height="10" viewBox="0 0 42 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 4.4C0.668629 4.4 0.4 4.66863 0.4 5C0.4 5.33137 0.668629 5.6 1 5.6V4.4ZM41.4243 5.42426C41.6586 5.18995 41.6586 4.81005 41.4243 4.57574L37.6059 0.757359C37.3716 0.523045 36.9917 0.523045 36.7574 0.757359C36.523 0.991674 36.523 1.37157 36.7574 1.60589L40.1515 5L36.7574 8.39411C36.523 8.62843 36.523 9.00833 36.7574 9.24264C36.9917 9.47696 37.3716 9.47696 37.6059 9.24264L41.4243 5.42426ZM1 5.6H41V4.4H1V5.6Z" fill="#146AFF"/>
                                </svg>
                            </a>
                        <?php } ?>
                    </div>
                <?php } ?>

                <div class="credit-cards-sidebar__ad-box">
					<iframe
						src="https://o1.qnsr.com/sjsc/o1/ff2.html?;p=src%3D664331;n=203;c=1670329/485404/223057;s=7273;d=40;w=300;h=240" frameborder=0 marginheight=0 marginwidth=0 scrolling="no" allowTransparency="true" width=300 height=240>
                    </iframe>
                </div>
			</section>
        </section>
    </div>
</main>
<script>
  jQuery(document).ready( function () {
    // I only have one form on the page but you can be more specific if need be.
    var form = jQuery('#mc-embedded-subscribe-form');
    var email = jQuery('#mce-EMAIL');
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    if ( form.length > 0 ) {
      jQuery('form input[type="submit"]').bind('click', function ( event ) {
        if ( event ) event.preventDefault();
        // validate_input() is a validation function I wrote, you'll have to substitute this with your own.
        if ( jQuery('#mce-EMAIL').val().match(mailformat) )
        {
          register(form);
        }else{
          jQuery('#mce-success-response').css('display','none');
          jQuery('#mce-error-response').css('display','block');
          jQuery('#mce-error-response').html('<p>Enter a valid value</p>');
        }
      });
    }
  });

  function register($form) {
    jQuery.ajax({
      type: $form.attr('method'),
      url: $form.attr('action'),
      data: $form.serialize(),
      cache       : false,
      dataType    : 'json',
      contentType: "application/json; charset=utf-8",
      error       : function(err) { alert("Could not connect to the registration server. Please try again later."); },
      success     : function(data) {
        if (data.result != "success") {
          jQuery('#mce-success-response').css('display','none')
          jQuery('#mce-error-response').css('display','block')
          jQuery('#mce-error-response').html('<p>' + data.msg.substring(4) + '</p>')
        } else {
          jQuery('#mce-error-response').css('display','none')
          jQuery('#mce-success-response').css('display','block')
          jQuery('#mce-success-response').html('<p>Thank you for subscribing!</p>')
          jQuery('#mce-EMAIL').val('')
          jQuery('#mce-FNAME').val('')
        }
      }
    });
  }
</script>
<?php get_footer();?>
