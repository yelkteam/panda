<?php
if ( ! function_exists( 'ppanda_setup' ) ) :
    function ppanda_setup(){

        add_theme_support( 'title-tag' );
        add_theme_support( 'post-thumbnails' );

        register_nav_menus( array( 'main_menu' => __( 'Main Navigation','panda') ) );
        register_nav_menus( array( 'footer_menu' => __( 'Footer Navigation','panda') ) );

        add_image_size('big_thumbnail',606,340, true);
        add_image_size('icon',60,60, array('center','center'));
        add_image_size('big_icon',100,100, array('center','center'));
        add_image_size('small_card',120,76);
        add_image_size('teacher',440,440, array('center','center'));
        add_image_size('blog_top',1235,440, array('center','center'));
        update_option( 'large_crop', 0 );

        load_theme_textdomain( 'panda', get_template_directory() . '/lang' );
    }
endif;
add_action( 'after_setup_theme', 'ppanda_setup' );


function panda_scripts() {

    wp_enqueue_script('jquery');

    $template_dir = get_template_directory_uri();
    $template_type = '';

    if ( is_front_page() ) {
        $template_type = 'home';
    }elseif ( is_page_template('template-cards.php') || is_tax('card-category') ) {
        $template_type = 'cards';
    }elseif ( is_home() || is_archive() ) { // !is_front_page && is_home
        $template_type = 'blog';
    }elseif(is_page_template('template-service.php')){
      $template_type = 'service';
    }elseif(is_page_template('template-course.php')){
      $template_type = 'courses';
    }elseif(is_page_template('template-about.php')){
      $template_type = 'about';
    }elseif(is_page_template('template-work.php')){
      $template_type = 'work';
    }


    if ( $template_type == 'blog' )
    {
        wp_enqueue_style( 'panda-blog-style', $template_dir  . '/css/blog-main-page.css',false, null);
    }

    if(is_single()){

      wp_enqueue_style( 'panda-swiper-style', "https://unpkg.com/swiper/swiper-bundle.min.css",false, null);
      wp_enqueue_style( 'panda-main-style', $template_dir  . '/css/main.css',false, null);
      wp_enqueue_style( 'panda-blog-style', $template_dir  . '/css/blog-post-page.css',false, null);

    }else{
      wp_enqueue_style( 'panda-main-style', $template_dir  . '/css/main.css',false, null);
    }
    if(is_singular('cards')){
        wp_enqueue_style( 'panda-credit-card-style', $template_dir  . '/css/show-credit-card-page.css',false, null);
    }

    if($template_type == 'service'){
      wp_enqueue_style( 'panda-swiper-style', "https://unpkg.com/swiper/swiper-bundle.min.css",false, null);
      wp_enqueue_style( 'panda-concierge-style', $template_dir  . '/css/concierge-page.css',false, null);
    }

    if($template_type == 'courses'){
      wp_enqueue_style( 'panda-courses-style', $template_dir  . '/css/courses-page.css',false, null);
    }

    if($template_type == 'about'){
      wp_enqueue_style( 'panda-courses-style', $template_dir  . '/css/about-page.css',false, null);
    }

    if($template_type == 'work'){
      wp_enqueue_style( 'panda-work-style', $template_dir  . '/css/work-with-us-page.css',false, null);
    }

    if(is_page_template('template-contacts.php')){
        wp_enqueue_style( 'panda-contact-style', $template_dir  . '/css/contact-page.css',false, null);
    }

    if(is_page_template('template-sitemap.php')){
        wp_enqueue_style( 'panda-sitemap-style', $template_dir  . '/css/sitemap-page.css',false, null);
    }

    if( $template_type == 'cards' ){
        wp_enqueue_style( 'panda-cards-style', $template_dir  . '/css/credit-cards-page.css',false, null);
    }

    if(is_404()){
        wp_enqueue_style( 'panda-err-style', $template_dir  . '/css/404-page.css',false, null);
    }

    if(is_search()){
        wp_enqueue_style( 'panda-search-style', $template_dir  . '/css/search-result-page.css',false, null);
    }

    wp_enqueue_style( 'panda-style', get_stylesheet_uri() );

    wp_enqueue_script( 'panda-menu-script', $template_dir . '/js/menu.js', array(), null, true );
    wp_enqueue_script( 'panda-slider-script', $template_dir . '/js/slider.js', array(), null, true );
    wp_enqueue_script( 'panda-search-script', $template_dir . '/js/search.js', array(), null, true );
    wp_enqueue_script( 'panda-scroll-script', $template_dir . '/js/scroll.js', array(), null, true );

    if ( $template_type == 'blog' || $template_type == 'cards' )
    {
        wp_enqueue_script( 'panda-dropdown-script', $template_dir . '/js/dropdown.js', array(), null, true );
    }

    if(is_page_template('template-contacts.php') || is_single()){
        wp_enqueue_script( 'panda-form1-script', $template_dir . '/js/form.js', array(), null, true );
    }
//    if(is_singular('cards')){
//        wp_enqueue_script( 'panda-modal-script', $template_dir . '/js/modal.js', array(), null, true );
//    }
    /*
    if(in_category('reviews')){
        wp_enqueue_script( 'panda-modal-script', $template_dir . '/js/modal.js', array(), null, true );
        wp_enqueue_script( 'panda-single-script', $template_dir . '/js/cards-single.js', array('panda-swiper-script'), null, true );
    }*/

    wp_enqueue_script( 'panda-modal-script', $template_dir . '/js/modal.js', array(), null, true );
    wp_enqueue_script( 'panda-main-script', $template_dir . '/js/main.js', array('panda-modal-script'), null, true );

    if ( $template_type == 'blog' )
    {
        wp_enqueue_script( 'panda-blog-script', $template_dir . '/js/blog.js', array(), null, true );
        wp_enqueue_script( 'mailerlite-script', 'https://static.mailerlite.com/js/w/webforms.min.js?v28bf44f740701752bfc6767bc7e171d4', array(), null, true );
    }elseif($template_type == 'home'){
        wp_enqueue_script( 'panda-home-script', $template_dir . '/js/home.js', array('panda-slider-script'), null, true );
    }

    if($template_type == 'service' || $template_type == 'courses' || is_single()){
      wp_enqueue_script( 'panda-video-script', $template_dir . '/js/video.js', array(), null, true );
    }

    if($template_type == 'service'){
      wp_enqueue_script( 'panda-accordion-script', $template_dir . '/js/accordion.js', array(), null, true );
      wp_enqueue_script( 'panda-swiper-script', "https://unpkg.com/swiper/swiper-bundle.min.js", array(), null, true );
      wp_enqueue_script( 'panda-service-script', $template_dir . '/js/service.js', array('panda-accordion-script','panda-swiper-script'), null, true );
    }

    if(is_page_template('template-travel.php')){
        wp_enqueue_style( 'panda-swiper-style', "https://unpkg.com/swiper/swiper-bundle.min.css",false, null);
        wp_enqueue_style( 'panda-concierge-style', $template_dir  . '/css/concierge-page.css',false, null);

        wp_enqueue_script( 'panda-accordion-script', $template_dir . '/js/accordion.js', array(), null, true );
        wp_enqueue_script( 'panda-swiper-script', "https://unpkg.com/swiper/swiper-bundle.min.js", array(), null, true );
        wp_enqueue_script( 'panda-service-script', $template_dir . '/js/service.js', array('panda-accordion-script','panda-swiper-script'), null, true );
        wp_enqueue_script( 'panda-video-script', $template_dir . '/js/video.js', array(), null, true );
    }

    if(is_page_template('template-frontpage.php')){
        wp_enqueue_script( 'panda-video-script', $template_dir . '/js/video.js', array(), null, true );
        wp_enqueue_script( 'mailerlite-script', 'https://static.mailerlite.com/js/w/webforms.min.js?v28bf44f740701752bfc6767bc7e171d4', array(), null, true );
    }

    if(is_single()){
      wp_enqueue_script( 'panda-swiper-script', "https://unpkg.com/swiper/swiper-bundle.min.js", array(), null, true );
      wp_enqueue_script( 'mailerlite-script', 'https://static.mailerlite.com/js/w/webforms.min.js?v28bf44f740701752bfc6767bc7e171d4', array(), null, true );

      if(is_singular('cards')){
        wp_enqueue_script( 'panda-single-script', $template_dir . '/js/cards-single.js', array('panda-swiper-script'), null, true );
      }else{
        wp_enqueue_script( 'panda-single-script', $template_dir . '/js/blog-single.js', array('panda-swiper-script'), null, true );
      }
    }

    if(is_page_template('template-contacts.php')){
        wp_enqueue_script( 'panda-contacts-script', $template_dir . '/js/contacts.js', array(), null, true );
    }

    if( $template_type == 'cards' ){
        wp_enqueue_script( 'panda-cards-script', $template_dir . '/js/page-cards.js', array('panda-dropdown-script'), null, true );
    }

    if ( is_singular() && get_option( 'thread_comments' ) )
      wp_enqueue_script( 'comment-reply' );

    wp_enqueue_script( 'panda-custom-script', $template_dir . '/js/custom.js', array(), null, true );

}
add_action( 'wp_enqueue_scripts', 'panda_scripts' );

function max_entries_per_sitemap() {
return 5000;
}
add_filter( 'wpseo_sitemap_entries_per_page', 'max_entries_per_sitemap' );


function js_variables(){
    $variables = array (
        'ajax_url' => admin_url('admin-ajax.php'),
    );
    echo '<script type="text/javascript">vars_ajax = '.json_encode($variables).';</script>';
}
add_action('wp_head','js_variables');

require_once('inc/post_types.php');
require_once('inc/fields.php');
require_once('inc/shortcodes.php');

/*
    MENU
*/

add_filter( 'nav_menu_css_class', 'panda_menu_item_classes', 10, 4 );
function panda_menu_item_classes( $classes, $item, $args, $depth ){

    if($args->theme_location == 'main_menu'){

        if( in_array('menu-item-has-children', $classes ) ){
            $classes[] = 'menu-box__nested-item';
        }

    }elseif($args->theme_location == 'footer_menu'){

        return array('footer-menu__item');

    }

    return $classes;
}

add_filter( 'nav_menu_submenu_css_class', 'panda_submenu_class', 10, 3 );
function panda_submenu_class( $classes, $args, $depth ){

    if($args->theme_location == 'main_menu'){

        $new_classes = array( 'menu-box__dropdown-content' );
        return $new_classes;
    }

    return $classes;
}

/*
    END MENU
*/

function get_latest_posts( $latest_cat = 0, $number_posts = 2){
    $args =  array(
        'numberposts' => $number_posts,
        'post_type'   => 'post',
    );
    if($latest_cat){
        $c = '';
        if(is_array($latest_cat)){
            foreach($latest_cat as $i => $category){
                if($i > 0){
                    $c .= ',';
                }
                $c .= $category['id'];
            }
        }else{
            $c = $latest_cat;
        }
        $args['category'] = $c ;
    }

    $latest_posts = get_posts( $args );

    return $latest_posts;
}

function get_most_popular_posts( $category = 0, $number_posts = 3){

    $start_date = date('Y-m-d H:m:s', strtotime("-60 days"));
    $args =  array(
        'numberposts' => $number_posts,
        'post_type'   => 'post',
        'meta_key' => 'views',
        'orderby' => 'meta_value_num',
        'order' => 'DESC',
        'date_query' => array(
            array(
                'after' => $start_date,
            ),
        ),
    );
    if($latest_cat){
        $c = '';
        foreach($latest_cat as $i => $category){
            if($i > 0){
                $c .= ',';
            }
            $c .= $category['id'];
        }
        $args['category'] = $c ;
    }

    $posts = get_posts( $args );

    return $posts;
}

function get_most_popular_post_in_category( $category = 0, $number_posts = 1){

    $start_date = date('Y-m-d H:m:s', strtotime("-60 days"));
    $args =  array(
        'numberposts' => $number_posts,
        'post_type'   => 'post',
        'meta_key' => 'views',
        'orderby' => 'meta_value_num',
        'order' => 'DESC',
        'cat' => $category
    );

    $posts = get_posts( $args );

    return $posts;
}

function get_latest_author_posts( $latest_cat = 0, $author, $number_posts = 2){

  $args =  array(
                  'posts_per_page' => $number_posts,
                  'post_type'   => 'post',
                  'post_status'=>'publish',
                  'author_name' => $author,
                  //'fields' => 'ids',
                );

  if($latest_cat){
      $c = '';
      if(is_array($latest_cat)){
          foreach($latest_cat as $i => $category){
              if($i > 0){
                  $c .= ',';
              }
              $c .= $category['id'];
          }
      }else{
          $c = $latest_cat;
      }
      $args['cat'] = $c ;
  }

  $latest_posts = new WP_Query( $args );

  return $latest_posts;
}

function get_most_popular_author_posts( $category = 0, $author, $number_posts = 3){

  $start_date = date('Y-m-d H:m:s', strtotime("-60 days"));
  $args =  array(
      'posts_per_page' => $number_posts,
      'post_type'   => 'post',
      'meta_key' => 'views',
      'orderby' => 'meta_value_num',
      'order' => 'DESC',
      'date_query' => array(
          array(
              'after' => $start_date,
          ),
      ),
  );
  if($latest_cat){
      $c = '';
      foreach($latest_cat as $i => $category){
          if($i > 0){
              $c .= ',';
          }
          $c .= $category['id'];
      }
      $args['cat'] = $c ;
  }

  $posts = new WP_Query( $args );

  return $posts;
}

function get_tags_list($post_id, $hash = 0){

  $r = '';
  $tags = wp_get_post_terms($post_id,'category');
    if($tags){
        foreach($tags as $i => $tag){
            if($i > 0){
                $r .= ', ';
            }
        $r .= $tag->name;
          break;
    }
  }
    return $r;
}

add_action( 'wp_ajax_nopriv_live_search', 'live_search_function' );
add_action( 'wp_ajax_live_search', 'live_search_function' );
function live_search_function(){

    echo '<ul>';

    if(isset($_POST['search']) && strlen(trim($_POST['search'])) > 3){

        $s = trim($_POST['search']);

        $query = new WP_Query( 's='.$s.'&posts_per_page=4&post_status=publish' );

        if ( $query->have_posts() ) {

            while ( $query->have_posts() ) {
                $query->the_post();
                get_template_part( 'template-parts/live_search','item' );
            }

        } else {
            get_template_part( 'template-parts/live_search', 'nothing' );
        }
        wp_reset_postdata();

    }else{
        get_template_part( 'template-parts/live_search', 'nothing' );
    }

    echo '</ul>';

    wp_die();
}

add_action( 'wp_ajax_nopriv_load_more_posts', 'load_more_posts_function' );
add_action( 'wp_ajax_load_more_posts', 'load_more_posts_function' );
function load_more_posts_function()
{
    $result = array(
                        'html' => '',
                        'has_more' => 0,
                    );

    $page = 1;

    if(isset($_POST['page']) && intval($_POST['page']) > 0 ){
        $page = $_POST['page'];
        $page ++;

        $result['page'] = $page;

        $args = array(
            'post_type' => 'post',
            'post_status' => 'publish',
            'paged'     => $page,
        );

        if(isset($_POST['cat']) && intval($_POST['cat']) > 0 ){
            $args['cat'] = intval($_POST['cat']);
        }

        if(isset($_POST['auth']) ){
          $args['author_name'] = $_POST['auth'];
        }

        $query = new WP_Query( $args );

        if ( $query->have_posts() ) {

            ob_start();

            $i = 0;
            while ( $query->have_posts() ) {
                $query->the_post();

                if($i%3 == 2 ){
                    get_template_part( 'template-parts/list_article','nodescription' );
                }else{
                    get_template_part( 'template-parts/list_article','description' );
                }
                $i++;
            }
            $result['html'] = ob_get_contents();
            ob_end_clean();

            if($query->max_num_pages > $result['page'] ){
                $result['has_more'] = 1;
            }
        } else {

        }
        wp_reset_postdata();
    }

    echo json_encode( $result );
    wp_die();
}

add_action( 'wp_ajax_nopriv_show_email', 'get_site_email_function' );
add_action( 'wp_ajax_show_email', 'get_site_email_function' );
function get_site_email_function(){

    echo carbon_get_theme_option('pemail');

    wp_die();
}

add_action( 'wp_ajax_nopriv_show_phone', 'get_site_phone_function' );
add_action( 'wp_ajax_show_phone', 'get_site_phone_function' );
function get_site_phone_function(){

    echo carbon_get_theme_option('pphone');

    wp_die();
}

function get_post_type_title($type){
    $title = '';
    switch($type){
        case 'cards':
            $title = 'credit cards';
            break;
        case 'post':
            $title = 'blog';
            break;

        default:
            $title = 'page';
            break;
    }

    return $title;
}

function get_post_terms_string( $post ){

    $post_taxonomy = array(
                            'post' => 'category',
                            'cards' => 'card-category',
                        );
    $terms_string = '';

    if($post && isset($post->post_type) && isset($post_taxonomy[$post->post_type]) ){
        $terms = wp_get_post_terms( $post->ID, $post_taxonomy[$post->post_type], array('fields' => 'names') );
        if($terms){
            foreach($terms as $i => $term){
                if($i > 0){
                    $terms_string .= ', ';
                }
                $terms_string .= $term;
            }
        }
    }


    return $terms_string;
}

/*
function panda_breadcrumbs() {

    $text['home'] = __('Home','panda');
    $text['blog'] = __('Blog','panda');
    $text['category'] =  __('%s','panda');
    $text['search'] = __('You searched for "%s"','panda');
    $text['tag'] = __('Tag "%s"','panda');
    $text['author'] = __('Author %s','panda');
    $text['404'] = __('Error 404: Page not found','panda');
    $text['page'] = __('Page %','panda');
    $text['cpage'] = __('Comments page %s','panda');

    $back = '<li class="breadcrumbs__item breadcrumbs__item--back">
              <a href="#">
                <svg width="16" height="10" viewBox="0 0 16 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M15 5.6C15.3314 5.6 15.6 5.33137 15.6 5C15.6 4.66863 15.3314 4.4 15 4.4L15 5.6ZM0.575736 4.57573C0.341421 4.81005 0.341421 5.18995 0.575736 5.42426L4.39411 9.24264C4.62843 9.47695 5.00833 9.47695 5.24264 9.24264C5.47696 9.00833 5.47696 8.62843 5.24264 8.39411L1.84853 5L5.24264 1.60589C5.47696 1.37157 5.47696 0.991673 5.24264 0.757358C5.00833 0.523044 4.62843 0.523044 4.39411 0.757358L0.575736 4.57573ZM15 4.4L1 4.4L1 5.6L15 5.6L15 4.4Z" fill="#6B6B79"/>
                </svg>
                <span>'.__('Back','panda').'</span>
              </a>
            </li>';

    $wrap_before = '<ul class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">'.$back;
    $wrap_after = '</ul><!-- .breadcrumbs -->';
    $sep = '';
    $sep_before = '';
    $sep_after = '';
    $show_home_link = 1;
    $show_on_home = 0;
    $show_current = 1;
    $before = '<li class="breadcrumbs__item breadcrumbs__item--disable">';
    $after = '</li>';

    global $post;
    $home_url = home_url('/');
    $link_before = '<li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">';
    $link_after = '</li>';
    $link_attr = ' itemprop="item"';
    $link_in_before = '<span itemprop="name">';
    $link_in_after = '</span>';
    $link = $link_before . '<a href="%1$s"' . $link_attr . '>' . $link_in_before . '%2$s' . $link_in_after . '</a>' . $link_after;
    $before_home_link = '<svg width="16" height="10" viewBox="0 0 16 10" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M15 5.6C15.3314 5.6 15.6 5.33137 15.6 5C15.6 4.66863 15.3314 4.4 15 4.4L15 5.6ZM0.575736 4.57573C0.341421 4.81005 0.341421 5.18995 0.575736 5.42426L4.39411 9.24264C4.62843 9.47695 5.00833 9.47695 5.24264 9.24264C5.47696 9.00833 5.47696 8.62843 5.24264 8.39411L1.84853 5L5.24264 1.60589C5.47696 1.37157 5.47696 0.991673 5.24264 0.757358C5.00833 0.523044 4.62843 0.523044 4.39411 0.757358L0.575736 4.57573ZM15 4.4L1 4.4L1 5.6L15 5.6L15 4.4Z" fill="#6B6B79"/></svg>';
    $frontpage_id = get_option('page_on_front');
    $blogpage_id = get_option('page_for_posts');
    $parent_id = ($post) ? $post->post_parent : '';
    $sep = ' ' . $sep_before . $sep . $sep_after . ' ';
    $home_link = $link_before . '<a href="' . $home_url . '"' . $link_attr . ' class="home">' . $before_home_link. $link_in_before . $text['home'] . $link_in_after . '</a>' . $link_after;

    if (is_home() || is_front_page()) {

      if (is_front_page() && $show_on_home) echo $wrap_before . $home_link . $wrap_after;
      elseif(!is_front_page() && is_home() ){

        echo $wrap_before . $home_link ;

        if ( $show_current && $blogpage_id ) echo $sep . $before . $text['blog'] . $after;

        echo $wrap_after;
      }

    } else {
      echo $wrap_before;
      if ($show_home_link) echo $home_link;

      if ( is_category() ) {
        $cat = get_category(get_query_var('cat'), false);
        if ($cat->parent != 0) {
          $cats = get_category_parents($cat->parent, TRUE, $sep);
          $cats = preg_replace("#^(.+)$sep$#", "$1", $cats);
          $cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr .'>' . $link_in_before . '$2' . $link_in_after .'</a>' . $link_after, $cats);
          if ($show_home_link) echo $sep;
          echo $cats;
        }
        if ( get_query_var('paged') ) {
          $cat = $cat->cat_ID;
          echo $sep . sprintf($link, get_category_link($cat), get_cat_name($cat)) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
        } else {
          if ($show_current) echo $sep . $before . sprintf($text['category'], single_cat_title('', false)) . $after;
        }

      } elseif ( is_search() ) {
        if (have_posts()) {
          if ($show_home_link && $show_current) echo $sep;
          if ($show_current) echo $before . sprintf($text['search'], get_search_query()) . $after;
        } else {
          if ($show_home_link) echo $sep;
          echo $before . sprintf($text['search'], get_search_query()) . $after;
        }

      } elseif ( is_day() ) {
        if ($show_home_link) echo $sep;
        echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $sep;
        echo sprintf($link, get_month_link(get_the_time('Y'), get_the_time('m')), get_the_time('F'));
        if ($show_current) echo $sep . $before . get_the_time('d') . $after;

      } elseif ( is_month() ) {
        if ($show_home_link) echo $sep;
        echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y'));
        if ($show_current) echo $sep . $before . get_the_time('F') . $after;

      } elseif ( is_year() ) {
        if ($show_home_link && $show_current) echo $sep;
        if ($show_current) echo $before . get_the_time('Y') . $after;

      } elseif ( is_single() && !is_attachment() ) {
        if ($show_home_link) echo $sep;
        if ( get_post_type() != 'post' ) {
          $post_type = get_post_type_object(get_post_type());
          $slug = $post_type->rewrite;
          printf($link, $home_url . $slug['slug'] . '/', $post_type->labels->singular_name);
          if ($show_current) echo $sep . $before . get_the_title() . $after;
        } else {
          $cat = get_the_category();
          $cat = $cat[0];
          $cats = get_category_parents($cat, TRUE, $sep);
          if (!$show_current || get_query_var('cpage')) $cats = preg_replace("#^(.+)$sep$#", "$1", $cats);
          $cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr .'>' . $link_in_before . '$2' . $link_in_after .'</a>' . $link_after, $cats);
          echo $cats;
          if ( get_query_var('cpage') ) {
            echo $sep . sprintf($link, get_permalink(), get_the_title()) . $sep . $before . sprintf($text['cpage'], get_query_var('cpage')) . $after;
          } else {
            if ($show_current) echo $before . get_the_title() . $after;
          }
        }

      // custom post type
      } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
        $post_type = get_post_type_object(get_post_type());
        if ( get_query_var('paged') ) {
          echo $sep . sprintf($link, get_post_type_archive_link($post_type->name), $post_type->singular_name) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
        } else {
            if( is_tax('card-category')  ){
                $cards_page = carbon_get_theme_option( 'ccpage' );
                echo $sep . sprintf($link, get_permalink($cards_page[0]['id']), get_the_title($cards_page[0]['id']));

                if ($show_current){
                    $currentTax = get_queried_object();
                    if($currentTax){
                        echo  $sep . $before . $currentTax->name . $after;
                    }
                }

            }else{
                if ($show_current) echo $sep . $before . $post_type->label . $after;
            }
        }

      } elseif ( is_attachment() ) {
        if ($show_home_link) echo $sep;
        $parent = get_post($parent_id);
        $cat = get_the_category($parent->ID); $cat = $cat[0];
        if ($cat) {
          $cats = get_category_parents($cat, TRUE, $sep);
          $cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr .'>' . $link_in_before . '$2' . $link_in_after .'</a>' . $link_after, $cats);
          echo $cats;
        }
        printf($link, get_permalink($parent), $parent->post_title);
        if ($show_current) echo $sep . $before . get_the_title() . $after;

      } elseif ( is_page() && !$parent_id ) {
        if ($show_current) echo $sep . $before . get_the_title() . $after;

      } elseif ( is_page() && $parent_id ) {
        if ($show_home_link) echo $sep;
        if ($parent_id != $frontpage_id) {
          $breadcrumbs = array();
          while ($parent_id) {
            $page = get_page($parent_id);
            if ($parent_id != $frontpage_id) {
              $breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
            }
            $parent_id = $page->post_parent;
          }
          $breadcrumbs = array_reverse($breadcrumbs);
          for ($i = 0; $i < count($breadcrumbs); $i++) {
            echo $breadcrumbs[$i];
            if ($i != count($breadcrumbs)-1) echo $sep;
          }
        }
        if ($show_current) echo $sep . $before . get_the_title() . $after;

      } elseif ( is_tag() ) {
        if ( get_query_var('paged') ) {
          $tag_id = get_queried_object_id();
          $tag = get_tag($tag_id);
          echo $sep . sprintf($link, get_tag_link($tag_id), $tag->name) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
        } else {
          if ($show_current) echo $sep . $before . sprintf($text['tag'], single_tag_title('', false)) . $after;
        }

      } elseif ( is_author() ) {
        global $author;
        $author = get_userdata($author);
        if ( get_query_var('paged') ) {
          if ($show_home_link) echo $sep;
          echo sprintf($link, get_author_posts_url($author->ID), $author->display_name) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
        } else {
          if ($show_home_link && $show_current) echo $sep;
          if ($show_current) echo $before . sprintf($text['author'], $author->display_name) . $after;
        }

      } elseif ( is_404() ) {
        if ($show_home_link && $show_current) echo $sep;
        if ($show_current) echo $before . $text['404'] . $after;

      } elseif ( has_post_format() && !is_singular() ) {
        if ($show_home_link) echo $sep;
        echo get_post_format_string( get_post_format() );
      }

      echo $wrap_after;

    }
  }
*/


function panda_breadcrumbs()
{

    $text['home'] = __('Home', 'panda');
    $text['blog'] = __('Blog', 'panda');
    $text['category'] = __('%s', 'panda');
    $text['search'] = __('You searched for "%s"', 'panda');
    $text['tag'] = __('Tag "%s"', 'panda');
    $text['author'] = __('Author %s', 'panda');
    $text['404'] = __('Error 404: Page not found', 'panda');
    $text['page'] = __('Page %', 'panda');
    $text['cpage'] = __('Comments page %s', 'panda');


    $wrap_before = '<div class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">';
    $wrap_after = '</div><!-- .breadcrumbs -->';
    $sep = '';
    $before = '<li class="breadcrumbs__item breadcrumbs__item--disable">';
    $after = '</li>';

    $show_on_home = 0;
    $show_home_link = 1;
    $show_current = 1;
    $show_last_sep = 1;

    global $post;
    $home_url = home_url('/');
    $link = '<li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">';
    $link .= '<a class="breadcrumbs__link" href="%1$s" itemprop="item"><span itemprop="name">%2$s</span></a>';
    $link .= '<meta itemprop="position" content="%3$s" />';
    $link .= '</li>';

    $before_hlink = '<svg width="16" height="10" viewBox="0 0 16 10" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M15 5.6C15.3314 5.6 15.6 5.33137 15.6 5C15.6 4.66863 15.3314 4.4 15 4.4L15 5.6ZM0.575736 4.57573C0.341421 4.81005 0.341421 5.18995 0.575736 5.42426L4.39411 9.24264C4.62843 9.47695 5.00833 9.47695 5.24264 9.24264C5.47696 9.00833 5.47696 8.62843 5.24264 8.39411L1.84853 5L5.24264 1.60589C5.47696 1.37157 5.47696 0.991673 5.24264 0.757358C5.00833 0.523044 4.62843 0.523044 4.39411 0.757358L0.575736 4.57573ZM15 4.4L1 4.4L1 5.6L15 5.6L15 4.4Z" fill="#6B6B79"/></svg>';
    $hlink = '<li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">';
    $hlink .= '<a class="breadcrumbs__link" href="%1$s" itemprop="item">'.$before_hlink.'<span itemprop="name">%2$s</span></a>';
    $hlink .= '<meta itemprop="position" content="%3$s" />';
    $hlink .= '</li>';


    $parent_id = ($post) ? $post->post_parent : '';
    $home_link = sprintf($hlink, $home_url, $text['home'], 1);

    if (is_home() || is_front_page()) {

        if ($show_on_home) echo $wrap_before . $home_link . $wrap_after;

    } else {

        $position = 0;

        echo $wrap_before;

        if ($show_home_link) {
            $position += 1;
            echo $home_link;
        }

        if (is_category()) {
            $parents = get_ancestors(get_query_var('cat'), 'category');
            foreach (array_reverse($parents) as $cat) {
                $position += 1;
                if ($position > 1) echo $sep;
                echo sprintf($link, get_category_link($cat), get_cat_name($cat), $position);
            }
            if (get_query_var('paged')) {
                $position += 1;
                $cat = get_query_var('cat');
                echo $sep . sprintf($link, get_category_link($cat), get_cat_name($cat), $position);
                echo $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
            } else {
                if ($show_current) {
                    if ($position >= 1) echo $sep;
                    echo $before . sprintf($text['category'], single_cat_title('', false)) . $after;
                } elseif ($show_last_sep) echo $sep;
            }

        } elseif (is_search()) {
            if (get_query_var('paged')) {
                $position += 1;
                if ($show_home_link) echo $sep;
                echo sprintf($link, $home_url . '?s=' . get_search_query(), sprintf($text['search'], get_search_query()), $position);
                echo $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
            } else {
                if ($show_current) {
                    if ($position >= 1) echo $sep;
                    echo $before . sprintf($text['search'], get_search_query()) . $after;
                } elseif ($show_last_sep) echo $sep;
            }

        } elseif (is_year()) {
            if ($show_home_link && $show_current) echo $sep;
            if ($show_current) echo $before . get_the_time('Y') . $after;
            elseif ($show_home_link && $show_last_sep) echo $sep;

        } elseif (is_month()) {
            if ($show_home_link) echo $sep;
            $position += 1;
            echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y'), $position);
            if ($show_current) echo $sep . $before . get_the_time('F') . $after;
            elseif ($show_last_sep) echo $sep;

        } elseif (is_day()) {
            if ($show_home_link) echo $sep;
            $position += 1;
            echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y'), $position) . $sep;
            $position += 1;
            echo sprintf($link, get_month_link(get_the_time('Y'), get_the_time('m')), get_the_time('F'), $position);
            if ($show_current) echo $sep . $before . get_the_time('d') . $after;
            elseif ($show_last_sep) echo $sep;

        } elseif (is_single() && !is_attachment()) {
            if (get_post_type() != 'post') {
                $position += 1;
                $post_type = get_post_type_object(get_post_type());
                if ($position > 1) echo $sep;

                $archive_link = '';
                if ($post_type->name == 'cards') {
                    $cards_page = carbon_get_theme_option( 'ccpage' );
                    if ( isset($cards_page[0]['id']) ) {
                        $archive_link = get_the_permalink($cards_page[0]['id']);
                    }
                } else {
                    $archive_link = get_post_type_archive_link($post_type->name);
                }

                echo sprintf($link, $archive_link, $post_type->labels->name, $position);
                if ($show_current) echo $sep . $before . get_the_title() . $after;
                elseif ($show_last_sep) echo $sep;
            } else {
                $cat = get_the_category();
                $catID = $cat[0]->cat_ID;
                $parents = get_ancestors($catID, 'category');
                $parents = array_reverse($parents);
                $parents[] = $catID;
                foreach ($parents as $cat) {
                    $position += 1;
                    if ($position > 1) echo $sep;
                    echo sprintf($link, get_category_link($cat), get_cat_name($cat), $position);
                }
                if (get_query_var('cpage')) {
                    $position += 1;
                    echo $sep . sprintf($link, get_permalink(), get_the_title(), $position);
                    echo $sep . $before . sprintf($text['cpage'], get_query_var('cpage')) . $after;
                } else {
                    if ($show_current) echo $sep . $before . get_the_title() . $after;
                    elseif ($show_last_sep) echo $sep;
                }
            }

        } elseif (is_post_type_archive()) {
            $post_type = get_post_type_object(get_post_type());
            if (get_query_var('paged')) {
                $position += 1;
                if ($position > 1) echo $sep;
                echo sprintf($link, get_post_type_archive_link($post_type->name), $post_type->label, $position);
                echo $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
            } else {
                if ($show_home_link && $show_current) echo $sep;
                if ($show_current) echo $before . $post_type->label . $after;
                elseif ($show_home_link && $show_last_sep) echo $sep;
            }

        } elseif (is_attachment()) {
            $parent = get_post($parent_id);
            $cat = get_the_category($parent->ID);
            $catID = $cat[0]->cat_ID;
            $parents = get_ancestors($catID, 'category');
            $parents = array_reverse($parents);
            $parents[] = $catID;
            foreach ($parents as $cat) {
                $position += 1;
                if ($position > 1) echo $sep;
                echo sprintf($link, get_category_link($cat), get_cat_name($cat), $position);
            }
            $position += 1;
            echo $sep . sprintf($link, get_permalink($parent), $parent->post_title, $position);
            if ($show_current) echo $sep . $before . get_the_title() . $after;
            elseif ($show_last_sep) echo $sep;

        } elseif (is_page() && !$parent_id) {
            if ($show_home_link && $show_current) echo $sep;
            if ($show_current) echo $before . get_the_title() . $after;
            elseif ($show_home_link && $show_last_sep) echo $sep;

        } elseif (is_page() && $parent_id) {
            $parents = get_post_ancestors(get_the_ID());
            foreach (array_reverse($parents) as $pageID) {
                $position += 1;
                if ($position > 1) echo $sep;
                echo sprintf($link, get_page_link($pageID), get_the_title($pageID), $position);
            }
            if ($show_current) echo $sep . $before . get_the_title() . $after;
            elseif ($show_last_sep) echo $sep;

        } elseif (is_tag()) {
            if (get_query_var('paged')) {
                $position += 1;
                $tagID = get_query_var('tag_id');
                echo $sep . sprintf($link, get_tag_link($tagID), single_tag_title('', false), $position);
                echo $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
            } else {
                if ($show_home_link && $show_current) echo $sep;
                if ($show_current) echo $before . sprintf($text['tag'], single_tag_title('', false)) . $after;
                elseif ($show_home_link && $show_last_sep) echo $sep;
            }

        } elseif (is_author()) {
            $author = get_userdata(get_query_var('author'));
            if (get_query_var('paged')) {
                $position += 1;
                echo $sep . sprintf($link, get_author_posts_url($author->ID), sprintf($text['author'], $author->display_name), $position);
                echo $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
            } else {
                if ($show_home_link && $show_current) echo $sep;
                if ($show_current) echo $before . sprintf($text['author'], $author->display_name) . $after;
                elseif ($show_home_link && $show_last_sep) echo $sep;
            }

        } elseif (is_404()) {
            if ($show_home_link && $show_current) echo $sep;
            if ($show_current) echo $before . $text['404'] . $after;
            elseif ($show_last_sep) echo $sep;

        } elseif (has_post_format() && !is_singular()) {
            if ($show_home_link && $show_current) echo $sep;
            echo get_post_format_string(get_post_format());
        }

        echo $wrap_after;

    }
}

// Changing excerpt more
function panda_excerpt_more($more) {
    return '…';
}
add_filter('excerpt_more', 'panda_excerpt_more');

function get_seo_post_image($postID){

    if(has_post_thumbnail($postID)){
      return get_the_post_thumbnail_url($postID);
    }
  return get_home_url().'wp-content/uploads/2020/03/Screenshot_2020-03-13-Save-Money-by-Buying-Cheap-Spirit-PointsPanda.png';
}

add_filter('crp_custom_template', 'crp_panda_template',10,4);
function crp_panda_template($t, $results, $args){

  if(!$results) return;

  $placeholder = carbon_get_theme_option( 'defimage' );

  ?>
    <div class="blog-cards-arrows-slider-box base-arrows-slider-box">
      <div class="base-arrows-slider-box__header">
        <h2><?=__('You might like','panda')?></h2>
        <div class="base-arrows-slider-box__arrows">
        <button class="base-arrows-slider-box__prev-arrow">
                        <svg width="8" height="16" viewBox="0 0 10 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M8.29346 1.25473C8.6843 0.864517 9.31746 0.865026 9.70767 1.25586C10.0979 1.6467 10.0974 2.27987 9.70654 2.67008L8.29346 1.25473ZM1 9.94956L0.293461 10.6572C0.105577 10.4697 0 10.2151 0 9.94956C0 9.68407 0.105577 9.42947 0.293461 9.24189L1 9.94956ZM9.70654 17.229C10.0974 17.6193 10.0979 18.2524 9.70767 18.6433C9.31746 19.0341 8.6843 19.0346 8.29346 18.6444L9.70654 17.229ZM9.70654 2.67008L1.70654 10.6572L0.293461 9.24189L8.29346 1.25473L9.70654 2.67008ZM1.70654 9.24189L9.70654 17.229L8.29346 18.6444L0.293461 10.6572L1.70654 9.24189Z" fill="#146AFF"/>
                        </svg>
                    </button>
                    <button class="base-arrows-slider-box__next-arrow">
                        <svg width="8" height="16" viewBox="0 0 10 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M1.70654 1.25473C1.3157 0.864517 0.682536 0.865026 0.292326 1.25586C-0.097885 1.6467 -0.0973765 2.27987 0.293461 2.67008L1.70654 1.25473ZM9 9.94956L9.70654 10.6572C9.89442 10.4697 10 10.2151 10 9.94956C10 9.68407 9.89442 9.42947 9.70654 9.24189L9 9.94956ZM0.293461 17.229C-0.0973765 17.6193 -0.097885 18.2524 0.292326 18.6433C0.682536 19.0341 1.3157 19.0346 1.70654 18.6444L0.293461 17.229ZM0.293461 2.67008L8.29346 10.6572L9.70654 9.24189L1.70654 1.25473L0.293461 2.67008ZM8.29346 9.24189L0.293461 17.229L1.70654 18.6444L9.70654 10.6572L8.29346 9.24189Z" fill="#146AFF"/>
                        </svg>
                    </button>
        </div>
      </div>
      <div class="swiper-container blog-cards-arrows-slider" id="blogCardsArrowsSlider">
        <div class="swiper-wrapper">
          <?php foreach($results as $item){ ?>
                  <div class="swiper-slide">
                    <div class="blog-card">
                        <a class="blog-card__link" href="<?=get_the_permalink($item->ID)?>">
                          <div class="blog-card__img-box">
                            <?php
                              if(has_post_thumbnail($item->ID)){
                                echo get_the_post_thumbnail($item->ID,'thumbnail');
                              }else{
                                echo wp_get_attachment_image($placeholder,'thumbnail');
                              }
                            ?>
                            <?php
                              $tags = wp_get_post_terms($item->ID,'category');
                              if($tags){
                                foreach($tags as $tag){
                            ?>
                                    <object>
                                        <a href="<?=get_category_link($tag->term_id)?>" class="blog-card__img-tag"><?=$tag->name?></a>
                                    </object>
                                <?php break; } ?>
                            <?php } ?>
                          </div>
                          <div class="blog-card__info">
                            <h4 class="blog-card__title"><?=get_the_title($item->ID)?></h4>
                            <div class="blog-card__content">
                                <?php
                                    $excerpt = get_the_excerpt($item->ID);
                                    echo wp_trim_words($excerpt, 13, '...');
                                ?>
                            </div>
                            <div class="blog-card__date"><?=get_the_date('F d, Y',$item->ID)?></div>
                          </div>
                        </a>
                    </div>
                  </div>
          <?php } ?>
        </div>
        <div class="blog-cards-arrows-slider-box__dots base-arrows-slider-box__dots"></div>
      </div>
    </div>
  <?php

}

function panda_excerpt_length( $length ) {

  return 25;
}
add_filter( 'excerpt_length', 'panda_excerpt_length', 999 );

function get_compare_block_data( $pageID ){

  $result = array();
  $fields = array(
                    'compare_%stop',
                    'compare_%stitle',
                    'compare_%sprice',
                    'compare_%spriceperiod',
                    'compare_%sitems',
                    'compare_%sokitems',
                    'compare_%spaypal',
                    'compare_%sstripe',
                    'compare_%sbottom',
                );

  $sides = array('l','r');
  $is_empty = 1;

  foreach($fields as $field){
    foreach($sides as $side ){
      $meta_key = sprintf($field, $side);
      $arr_key = str_replace('compare_%s','', $field);
      $result[$side][ $arr_key ] = carbon_get_post_meta($pageID, $meta_key);

      if( $is_empty && $result[$side][ $arr_key ] ){
        $is_empty = 0;
      }

    }
  }

  if( $is_empty ){
    return false;
  }

  return $result;
}


add_action( 'pre_get_posts', 'panda_get_all_search_resulta' );
function panda_get_all_search_resulta( $query ) {

    if( ! is_admin() && $query->is_main_query() ){
        if($query->is_search){
            $query->set( 'posts_per_page', -1 );
        }
    }

    return $query;
}

function disable_wp_responsive_images() {
    return 1;
}
add_filter('max_srcset_image_width', 'disable_wp_responsive_images');

add_filter("wpseo_robots", function($robots) {
        if (is_paged()) {
        return 'noindex,follow';
    } else {
        return $robots;
    }
});


add_filter( 'wp_postratings_ratings_images', 'panda_postratings_ratings_images',10,4 );
function panda_postratings_ratings_images( $get_ratings_images, $post_id, $post_ratings, $ratings_max ){

    $html = '';
    $template_dir = get_template_directory_uri();

    $html .= '<div class="card-statistics__stars-box">';

    for ($i=1; $i <= $ratings_max ; $i++) {
        if($post_ratings >= $i){
            $html .= '<img src="'.$template_dir.'/img/star-full.png" alt="average: '.$post_ratings.' out of '.$ratings_max.'" title="average: '.$post_ratings.' out of '.$ratings_max.'" class="post-ratings-image" />';
        }else{
            $html .= '<img src="'.$template_dir.'/img/star-empty.png" alt="average: '.$post_ratings.' out of '.$ratings_max.'" title="average: '.$post_ratings.' out of '.$ratings_max.'" class="post-ratings-image" />';
        }

    }

    $html .= '</div>';

    return $html;
}

function panda_blog_redirect() {

    if( is_404() ) {

        $url = strtok($_SERVER["REQUEST_URI"], '?');
        $url = str_replace('/', '', $url);

        $args=array(
            'name' => $url,
            'post_type' => 'post',
            'post_status' => 'publish',
            'posts_per_page' => 1
        );
        $posts = get_posts( $args );
        if( $posts ) {
            foreach ($posts as $post) {
                wp_redirect( get_the_permalink($post->ID) );
                exit();
            }
        }

    }
}
add_action( 'template_redirect', 'panda_blog_redirect' );

function wpb_move_comment_field_to_bottom( $fields ) {
  $comment_field = $fields['comment'];
  unset( $fields['comment'] );
  $fields['comment'] = $comment_field;

  $new_fields = array(
                      'before_all' => '<div class="comment-form-box__icon-box">
                      <svg width="166" height="180" viewBox="0 0 166 180" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path d="M60.9009 69.4123L60.9009 39.0796C60.9009 37.1663 59.3498 35.6152 57.4365 35.6152L14.4643 35.6152C12.551 35.6152 11 37.1663 11 39.0796L11 84.532C11 86.1983 13.1226 86.9036 14.1199 85.5686L23.6012 72.8766L57.4365 72.8766C59.3498 72.8766 60.9009 71.3256 60.9009 69.4123Z" fill="url(#paint0_linear)"/>
                          <g filter="url(#filter0_d)">
                              <path d="M51.2179 47.2307L70.2121 14.3319C72.6037 10.1894 77.9006 8.77016 82.043 11.1618L131.499 39.7152C135.641 42.1068 137.061 47.4037 134.669 51.5461L101.421 109.132C100.589 110.574 98.4014 110.127 98.2017 108.474L95.0674 82.5478L54.388 59.0616C50.2456 56.6699 48.8263 51.3731 51.2179 47.2307Z" fill="url(#paint1_linear)"/>
                          </g>
                          <path fill-rule="evenodd" clip-rule="evenodd" d="M82.6147 32.0143C83.093 31.1858 84.1524 30.902 84.9809 31.3803L113.487 47.8386C114.316 48.3169 114.6 49.3763 114.121 50.2048C113.643 51.0333 112.584 51.3171 111.755 50.8388L83.2487 34.3805C82.4202 33.9022 82.1363 32.8428 82.6147 32.0143ZM77.7502 40.4358C78.2285 39.6073 79.2879 39.3234 80.1164 39.8018L108.623 56.2601C109.451 56.7384 109.735 57.7978 109.257 58.6262C108.779 59.4547 107.719 59.7386 106.891 59.2603L78.3842 42.802C77.5557 42.3236 77.2719 41.2643 77.7502 40.4358ZM72.8876 48.8582C73.366 48.0297 74.4253 47.7459 75.2538 48.2242L103.76 64.6825C104.589 65.1608 104.873 66.2202 104.394 67.0487C103.916 67.8771 102.857 68.161 102.028 67.6827L73.5216 51.2244C72.6932 50.7461 72.4093 49.6867 72.8876 48.8582Z" fill="white"/>
                          <defs>
                              <filter id="filter0_d" x="0.0561523" y="0" width="165.775" height="180" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                                  <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                                  <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
                                  <feOffset dx="-10" dy="30"/>
                                  <feGaussianBlur stdDeviation="20"/>
                                  <feColorMatrix type="matrix" values="0 0 0 0 0.827451 0 0 0 0 0.34902 0 0 0 0 0.45098 0 0 0 0.2 0"/>
                                  <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
                                  <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
                              </filter>
                              <linearGradient id="paint0_linear" x1="31.7215" y1="68.6005" x2="31.7215" y2="38.9983" gradientUnits="userSpaceOnUse">
                                  <stop stop-color="#146AFF"/>
                                  <stop offset="0.97883" stop-color="#149CFF"/>
                              </linearGradient>
                              <linearGradient id="paint1_linear" x1="113.53" y1="45.3073" x2="120.63" y2="33.0087" gradientUnits="userSpaceOnUse">
                                  <stop stop-color="#D35973"/>
                                  <stop offset="1" stop-color="#F5748F"/>
                              </linearGradient>
                          </defs>
                      </svg>
                  </div>',
                      'ftitle' => '<div class="comment-form__header"><h4>Leave a comment</h4></div>',
                      'fbefore' => '<div class="comment-form__fields">',
                  );
  $new_fields = array_merge($new_fields, $fields);
  $new_fields['fafter'] = '</div>';

return $new_fields;
}

add_filter( 'comment_form_fields', 'wpb_move_comment_field_to_bottom' );

class Panda_Walker_Comment extends Walker_Comment {

  public function start_lvl( &$output, $depth = 0, $args = array() ) {
      $GLOBALS['comment_depth'] = $depth + 1;

      switch ( $args['style'] ) {
          case 'div':
              break;
          case 'ol':
              $output .= '<ol class="children">' . "\n";
              break;
          case 'ul':
          default:
              $output .= '<ul class="comments-list">' . "\n";
              break;
      }
  }

  public function end_lvl( &$output, $depth = 0, $args = array() ) {
      $GLOBALS['comment_depth'] = $depth + 1;

      switch ( $args['style'] ) {
          case 'div':
              break;
          case 'ol':
              $output .= "</ol><!-- .children -->\n";
              break;
          case 'ul':
          default:
              $output .= "</ul><!-- .children -->\n";
              break;
      }
  }

  public function start_el( &$output, $comment, $depth = 0, $args = array(), $id = 0 ) {
      $depth++;
      $GLOBALS['comment_depth'] = $depth;
      $GLOBALS['comment']       = $comment;

      if ( ! empty( $args['callback'] ) ) {
          ob_start();
          call_user_func( $args['callback'], $comment, $args, $depth );
          $output .= ob_get_clean();
          return;
      }

      if ( ( 'pingback' == $comment->comment_type || 'trackback' == $comment->comment_type ) && $args['short_ping'] ) {
          ob_start();
          $this->ping( $comment, $depth, $args );
          $output .= ob_get_clean();
      } elseif ( 'html5' === $args['format'] ) {
          ob_start();
          $this->html5_comment( $comment, $depth, $args );
          $output .= ob_get_clean();
      } else {
          ob_start();
          $this->comment( $comment, $depth, $args );
          $output .= ob_get_clean();
      }
  }

  /**
   * Outputs a pingback comment.
   *
   * @since 3.6.0
   *
   * @see wp_list_comments()
   *
   * @param WP_Comment $comment The comment object.
   * @param int        $depth   Depth of the current comment.
   * @param array      $args    An array of arguments.
   */
  protected function ping( $comment, $depth, $args ) {
      $tag = ( 'div' == $args['style'] ) ? 'div' : 'li';
      ?>
      <<?php echo $tag; ?> id="comment-<?php comment_ID(); ?>" <?php comment_class( '', $comment ); ?>>
          <div class="comment-body">
              <?php _e( 'Pingback:' ); ?> <?php comment_author_link( $comment ); ?> <?php edit_comment_link( __( 'Edit' ), '<span class="edit-link">', '</span>' ); ?>
          </div>
      <?php
  }

  protected function html5_comment( $comment, $depth, $args ) {
      $tag = ( 'div' === $args['style'] ) ? 'div' : 'li';

      $commenter = wp_get_current_commenter();
      if ( $commenter['comment_author_email'] ) {
          $moderation_note = __( 'Your comment is awaiting moderation.' );
      } else {
          $moderation_note = __( 'Your comment is awaiting moderation. This is a preview, your comment will be visible after it has been approved.' );
      }

      $classes = get_comment_class('comments-list__item ', $comment);
    /*
      $classes[] = 'comment-instance';

      if($depth > 0){
          $classes[] = 'comment-instance--nested';
      }
*/
      $classes_list = '';
      if($classes){
          foreach ($classes as $value) {
              $classes_list .= $value.' ';
          }
      }

      ?>
      <<?php echo $tag; ?> id="div-comment-<?php comment_ID(); ?>" class="<?php echo $classes_list; ?>">
          <div class="comment-body comment-instance">
              <div class="comment-instance__header">
                  <div class="comment-instance__avatar-box">
                      <?php
                          if ( 0 != $args['avatar_size'] ) {
                              echo get_avatar( $comment, $args['avatar_size'] );
                          }
                      ?>
                  </div>
                  <div class="comment-instance__info-box">
                      <h4 class="fn"><?=get_comment_author_link( $comment )?></h4>
                      <span class="comment-instance__date"><?=get_comment_date( 'F d, Y', $comment )?></span>
                  </div>
              </div>
              <?php comment_text(); ?>

              <?php
                      comment_reply_link(
                          array_merge(
                              $args,
                              array(
                                  'add_below' => 'div-comment',
                                  'depth'     => $depth,
                                  'max_depth' => $args['max_depth'],
                                  'before'    => '<div class="reply">',
                                  'after'     => '</div>',
                              )
                          )
                      );
                  ?>
          </div>

      <?php
  }
}

add_filter( 'img_caption_shortcode_width', '__return_false' );

//clear caches after post published
function post_status( $new_status, $old_status, $post )
{
    if ( $new_status === "publish" || $old_status === "publish" )
    {
        if ( defined( 'W3TC' ) )
        {
            w3tc_flush_all();
        }
    }
}
add_action(  'transition_post_status',  'post_status', 10, 3 );

//get primary category
function get_primary_category($category){
  $useCatLink = true;
  // If post has a category assigned.
  if ($category){
    $category_display = '';
    $category_link = '';
    if ( class_exists('WPSEO_Primary_Term') )
    {
      // Show the post's 'Primary' category, if this Yoast feature is available, & one is set
      $wpseo_primary_term = new WPSEO_Primary_Term( 'category', get_the_id() );
      $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
      $term = get_term( $wpseo_primary_term );
      if (is_wp_error($term)) {
        // Default to first category (not Yoast) if an error is returned
        $category_display = $category[0]->term_id;
        $category_link = get_category_link( $category[0]->term_id );
      } else {
        // Yoast Primary category
        $category_display = $term->term_id;
        $category_link = get_category_link( $term->term_id );
      }
    }
    else {
      // Default, display the first category in WP's list of assigned categories
      $category_display = $category[0]->term_id;
      $category_link = get_category_link( $category[0]->term_id );
    }
    // Display category
    if ( !empty($category_display) ){
      if ( $useCatLink == true && !empty($category_link) ){
      return ''.htmlspecialchars($category_display).'';
      } else {
      return ''.htmlspecialchars($category_display).'';
      }
    }
  }
}

// Remove Tags
add_action('init', 'myprefix_remove_tax');
function myprefix_remove_tax() {
    register_taxonomy('post_tag', array());
}

add_action('admin_menu', 'my_remove_sub_menus');
function my_remove_sub_menus() {
    remove_submenu_page('edit.php', 'edit-tags.php?    taxonomy=post_tag&post_type=post');
}

add_filter('user_contactmethods', 'panda_update_user_contactmethods',100);

function panda_update_user_contactmethods( $contactmethods ) {

  $contactmethods['inst_url'] = 'Instagram profile URL';
  $contactmethods['reddit_url'] = 'Reddit profile URL';

  return $contactmethods;
}

/**
 * Remove built in jQuery and replace with hosted version to improve speed, prevent render blocking, and defer
 */

function my_enqueued_assets() {
    wp_deregister_script( 'jquery' );
    wp_enqueue_script( 'script-name', '//code.jquery.com/jquery-1.12.4.min.js', array(), '1.12.4' );
}
add_action( 'wp_enqueue_scripts', 'my_enqueued_assets' );